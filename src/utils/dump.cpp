/*
    This file is part of WhiskEras 2.0.

    WhiskEras 2.0 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WhiskEras 2.0 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WhiskEras 2.0.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "dump.h"
#include "../pipeline/pipeline.h"
#include <fstream>
#include "../evaluation/evaluation.h"

void loadTracks(bool &start, std::string tracksFileLocation, std::vector<whisker> &lineGatherer_tracker,
                std::vector<int> &order, track_opts &trackOpts, train_opts &trainOpts, fwptd_opts &fwptdOpts) {
    start = false;
    std::ifstream tracks_r_FILE;
    tracks_r_FILE.open(tracksFileLocation);
    whisker line;
    float xf, angle, length, shape, x_max, shiftDown;
    while (tracks_r_FILE >> xf >> angle >> length >> shape >> x_max >> shiftDown) {
        line.position = xf;
        line.angle = angle;
        line.length = length;
        line.shape = shape;
        line.x_max = x_max;
        line.shiftDown = shiftDown;
        lineGatherer_tracker.push_back(line);
    }
    tracks_r_FILE.close();
    loadPreviousTracks(order, lineGatherer_tracker, trackOpts, trainOpts, fwptdOpts);
}

void saveTracks(std::vector<whisker> &lineGatherer_tracker, std::string tracksFileLocation) {
    std::ofstream tracks_FILE;
    tracks_FILE.open(tracksFileLocation);
    for (size_t j = 0; j < lineGatherer_tracker.size(); j++) {
        tracks_FILE << lineGatherer_tracker[j].position << " " << lineGatherer_tracker[j].angle << " " <<
                    lineGatherer_tracker[j].length << " " << lineGatherer_tracker[j].shape << " "
                    << lineGatherer_tracker[j].x_max << " " << lineGatherer_tracker[j].shiftDown << std::endl;
    }
    tracks_FILE.close();
}

void saveTimingProfile(timers timers, int i, std::ofstream &execTimeFile) {
    execTimeFile << "Frame " << i << std::endl;
    execTimeFile << "Whole = " << timers.loop_time << std::endl;
    execTimeFile << "Preprocessing = " << timers.preprocessing_time << std::endl;
    execTimeFile << "Whisker Points Detection = " << timers.detection_time << std::endl;
    execTimeFile << "Clustering I = " << timers.clustering_I_time << std::endl;
    execTimeFile << "Clustering II = " << timers.clustering_II_time << std::endl;
    execTimeFile << "Stitching_II = " << timers.loop_time << std::endl;
    execTimeFile << "Parameter fitting = " << timers.parametrization_time << std::endl;
    execTimeFile << "Whisker Fitting = " << timers.tracking_time << std::endl;
    execTimeFile << "Training = " << timers.training_time << std::endl;
    execTimeFile << "Transfer to GPU = " << timers.transfer_to_gpu_time << std::endl;
    execTimeFile << "Transfer from GPU = " << timers.transfer_from_gpu_time << std::endl;
    execTimeFile << "----------------------------------------------" << std::endl << std::endl;
}

void saveTotalTimingProfile(timers timers, std::string timeProfileLocation) {
    std::ofstream execTimeFile;
    execTimeFile.open(timeProfileLocation, std::ios::app);
    execTimeFile << std::endl << "TOTAL:" << std::endl;
    execTimeFile << "Whole processing took " << timers.pipeline_timer.seconds() << " seconds" << std::endl;
    execTimeFile << "Preprocessing took " << std::setprecision(4) << timers.preprocessing_time << " seconds"
              << std::endl;
    execTimeFile << "Transfering image to device took " << std::setprecision(4) << timers.transfer_to_gpu_time
              << " seconds" << std::endl;
    execTimeFile << "Steger Whisker detection took " << std::setprecision(4) << timers.detection_time << " seconds"
              << std::endl;
    execTimeFile << "Transfering results from device took " << std::setprecision(4) << timers.transfer_from_gpu_time
              << " seconds" << std::endl;
    execTimeFile << "Clustering took " << std::setprecision(4) << timers.clustering_time << " seconds" << std::endl;
    execTimeFile << "\tClustering I took " << std::setprecision(4) << timers.clustering_I_time << " seconds"
              << std::endl;
    execTimeFile << "\tClustering II took " << std::setprecision(4) << timers.clustering_II_time << " seconds"
              << std::endl;
    execTimeFile << "\t\t Stitching I took " << std::setprecision(4) << timers.stitching_I_time << " seconds"
              << std::endl;
    execTimeFile << "\t\t Stitching II took " << std::setprecision(4) << timers.stitching_II_time << " seconds"
              << std::endl;
    execTimeFile << "Parametrization took " << std::setprecision(4) << timers.parametrization_time << " seconds"
              << std::endl;
    execTimeFile << "Tracking and recognition took " << std::setprecision(4) << timers.tracking_time << " seconds"
              << std::endl;
    execTimeFile << "Training took " << std::setprecision(4) << timers.training_time << " seconds" << std::endl;
    execTimeFile << "Loop took " << std::setprecision(4) << timers.loop_time << " seconds" << std::endl;
    execTimeFile << "OpenCV overhead took " << std::setprecision(4) << timers.CV_time << " seconds" << std::endl;
    execTimeFile << std::endl << std::endl;
}

void getQualityResults(std::vector<int> &whiskers_detected_Nr, std::vector<std::vector<float>> &whisker_angles,
                       std::vector<std::vector<whisker>> &whiskers, std::vector<whisker> &lineGatherer_tracker, track_opts trackOpts,
                       general_opts generalOpts, plotting_opts plottingOpts, dump_opts dumpOpts) {
    // detection ratio per whisker
//    writeDetectionRates(trackOpts, dumpOpts.qualityOutputDir);
    // detected whisker per frame
    if (plottingOpts.startFrame == -1) plottingOpts.startFrame = generalOpts.startFrame;
    if (plottingOpts.endFrame == -1) plottingOpts.endFrame = generalOpts.endFrame;
//    writeDetectedWhiskersNr(whiskers_detected_Nr, plottingOpts.startFrame, plottingOpts.endFrame,
//                           generalOpts.startFrame - generalOpts.loadTracks, dumpOpts.qualityOutputDir);
    // tracking quality - angles
    writeWhiskerAnglesToCSV(whisker_angles, lineGatherer_tracker.size(), plottingOpts.startFrame, plottingOpts.endFrame,
                      generalOpts.startFrame - generalOpts.loadTracks, dumpOpts.qualityOutputDir + '/' + dumpOpts.outputFilename);
    clearAngles(whisker_angles, lineGatherer_tracker.size());
    // all whisker characteristis
//    writeWhiskersToCSV(whiskers, lineGatherer_tracker.size(), plottingOpts.startFrame, plottingOpts.endFrame,
//                       generalOpts.startFrame, dumpOpts.qualityOutputDir + '/' + dumpOpts.outputFilename);
}
