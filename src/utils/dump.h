/*
    This file is part of WhiskEras 2.0.

    WhiskEras 2.0 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WhiskEras 2.0 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WhiskEras 2.0.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef FWTS_C_DUMP_H
#define FWTS_C_DUMP_H

#include "../stages/whisker_forming/whisker_forming_options.h"
#include "../stages/tracking_over_time/tracking.h"
#include "profile.h"
#include "../options/options.h"

void loadTracks(bool &start, std::string tracksFileLocation, std::vector<whisker> &lineGatherer_tracker,
                std::vector<int> &order, track_opts &trackOpts, train_opts &trainOpts, fwptd_opts &fwptdOpts);

void saveTracks(std::vector<whisker> &lineGatherer_tracker, std::string tracksFileLocation);

void saveTimingProfile(timers timers, int i, std::ofstream &execTimeFile);

void saveTotalTimingProfile(timers timers, std::string timeProfileLocation);

void getQualityResults(std::vector<int> &whiskers_detected_Nr, std::vector<std::vector<float>> &whisker_angles,
                       std::vector<std::vector<whisker>> &whiskers, std::vector<whisker> &lineGatherer_tracker, track_opts trackOpts,
                       general_opts generalOpts, plotting_opts plottingOpts, dump_opts dumpOpts);

#endif //FWTS_C_DUMP_H
