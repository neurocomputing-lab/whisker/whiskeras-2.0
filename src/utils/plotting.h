/*
    This file is part of WhiskEras 2.0.

    WhiskEras 2.0 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WhiskEras 2.0 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WhiskEras 2.0.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef FWTS_C_PLOTTING_H
#define FWTS_C_PLOTTING_H

#pragma once
#include <opencv2/opencv.hpp>
#include <numeric>
#include "../stages/whisker_forming/whisker_forming_options.h"

struct plotting_opts {
    int width;
    int height;
    float origin[2];
    float sbase[2][2];
    int cutoff = 10;
    int startFrame = 0;
    int endFrame = 1000;
    int colormap = cv::COLORMAP_INFERNO;
    bool plot_unprocessed_frame = false;
    bool plot_unprocessed_frame_with_snout_line = false;
    int framing = 7; // add framing to output image (pixels)
    bool writeVideoOutput = false;
    bool dontShow = false;
    bool saveIntermediateResults = false;
    std::string outputDir;
    int current_frame_no = 0;
    cv::VideoWriter videoOutput;
};

void rotateBackClusters(std::vector<float> rotatedPositions, std::vector<float> &plotPositions, plotting_opts plottingOpts);

void plotPairOfClusters(std::vector<int> clusters, std::vector<float> rotatedPositions,
                        int whisker1, int whisker2, plotting_opts plottingOpts, std::string label);

void rotateAndPlotClusters(std::vector<int> clusters, std::vector<float> rotatedPositions, int numOfClusters,
                           int whisker_num, plotting_opts plottingOpts, bool withColors, std::string label);

// create framing for plotted images
void createFraming(cv::Mat &src, int framing_width);

void createFramingGrayscale(cv::Mat &src, int framing_width);

/// This function takes as input lines parameters in [xf a b L] notation and returns a series of coordinates which can be plotted
/// \param whiskers         [in] whiskers parameters
/// \param numOfLines       [in] number of lines
void plotLines(std::vector<struct whisker> whiskers, int numOfLines, int whisker_num, std::vector<int> order, plotting_opts plottingOpts, std::string label);

cv::Mat plotLinesOverlaid(cv::Mat unprocessed_frame, int upscaling, std::vector<struct whisker> whiskers, int numOfLines,
                        int whisker_num, std::vector<int> order, plotting_opts &plottingOpts, std::string label);

void showImage(cv::Mat image, cv::String label, int framing, bool gray, plotting_opts plottingOpts);

void showImageWithSnout(cv::Mat image, int tip[], int middle[], float upscaling, int framing, plotting_opts plottingOpts, cv::String label);

#endif //FWTS_C_PLOTTING_H
