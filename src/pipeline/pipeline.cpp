/*
    This file is part of WhiskEras 2.0.

    WhiskEras 2.0 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WhiskEras 2.0 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WhiskEras 2.0.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "pipeline.h"
#include "../utils/video.h"
#include <iostream>
#include <fstream>
#include "../utils/dump.h"
#include "../evaluation/evaluation.h"
#include <QThread>

template<typename T>
void convertMatToVector(cv::Mat mat, std::vector<T> &vector) {
    if (mat.isContinuous()) {
        // array.assign((float*)mat.datastart, (float*)mat.dataend); // <- has problems for sub-matrix like mat = big_mat.row(i)
        vector.assign((T*)mat.data, (T*)mat.data + mat.total());
    } else {
        for (int i = 0; i < mat.rows; ++i) {
            vector.insert(vector.end(), mat.ptr<T>(i), mat.ptr<T>(i)+mat.cols);
        }
    }
}

void run_pipeline(timers &timers, cv::Mat frame, cv::Mat bgImage, cv::Mat bwShape, std::vector<float> &rotatedPositions,
                  std::vector<int> &clusters, std::vector<whisker> &parametrizations, preprocessing_opts preprocessingOpts,
                  c_extraction_opts cExtractionOpts, clustering_opts clusteringOpts, stitch_I_opts stitchIOpts,
                  stitch_II_opts stitchIIOpts, param_opts &paramOpts, plotting_opts plottingOpts, general_opts generalOpts) {
    // preprocessing
    timers.tt.start();
    cv::Mat preprocessedImage;
    preprocessedImage = preprocessing(frame, bgImage, bwShape, preprocessingOpts, plottingOpts);
    timers.tt.stop();
    timers.preprocessing_time += timers.tt.seconds();
    if (generalOpts.verbose) std::cout << "Preprocessing took " << timers.tt.seconds() << " seconds" << std::endl;                                                    // measure FPS

    // whisker point detection
    timers.tt.start();
    cv::Mat whiskerPoints, egv, whiskerAngles;
    std::vector<std::pair<int16_t,int16_t>> startPoints;
    steger_point_detection(preprocessedImage, whiskerPoints, startPoints, whiskerAngles, cExtractionOpts, plottingOpts);

    timers.tt.stop();
    timers.detection_time += timers.tt.seconds();
    if (generalOpts.verbose) std::cout << "Steger whisker detection took " << timers.tt.seconds() << " seconds" << std::endl;                                                    // measure FPS

    // clustering
    timers.tt.start();
    timers.transfer_timer.start();
    // load whiskerAngles and preprocessedImage as C arrays
    std::vector<float> whiskerAnglesVec;
    convertMatToVector(whiskerAngles, whiskerAnglesVec);
    cExtractionOpts.whiskerAngles = whiskerAnglesVec.data();
    std::vector<uchar> preprocessedImageVec;
    convertMatToVector(preprocessedImage, preprocessedImageVec);
    uchar *preprocessedImageArr = preprocessedImageVec.data();

    int numOfPoints;
    std::vector<std::pair<uint16_t, uint16_t>> whiskerPositions;
    clustering_I(startPoints, cExtractionOpts.whiskerAngles, numOfPoints, whiskerPositions, clusters, clusteringOpts);
    timers.transfer_timer.stop();
    timers.clustering_I_time += timers.transfer_timer.seconds();

    timers.transfer_timer.start();
    int numOfClusters = clustering_II(clusters, whiskerPositions, rotatedPositions, numOfPoints, preprocessedImageArr,
                  plottingOpts, clusteringOpts, stitchIOpts, stitchIIOpts, timers);
    if (numOfClusters == 0) {
        std::cout << "No clusters were found. Returning." << std::endl;
        return;
    }
    timers.transfer_timer.stop();
    timers.tt.stop();
    timers.clustering_II_time += timers.transfer_timer.seconds();
    timers.clustering_time += timers.tt.seconds();
    if (generalOpts.verbose) std::cout << "Clustering took " << timers.tt.seconds() << " seconds" << std::endl;                                                    // measure FPS

    // parametrization
    timers.tt.start();
    clusters2whiskers(parametrizations, clusters, rotatedPositions, paramOpts, plottingOpts);

    timers.tt.stop();
    timers.parametrization_time += timers.tt.seconds();
    if (generalOpts.verbose) std::cout << "Parametrization took " << timers.tt.seconds() << " seconds" << std::endl;                                                    // measure FPS


    timers.FPS_timer.stop();
    if (generalOpts.verbose) std::cout << "Frame processing took " << timers.FPS_timer.seconds() << " seconds" << std::endl;                                                    // measure FPS
}

void getBackground(cv::Mat &bgImage, cv::Mat &bwShape, cv::VideoCapture video, dump_opts dumpOpts,
                   general_opts generalOpts, preprocessing_opts preprocessingOpts) {
    Timer bg_shape_timer;
    std::ofstream timeProfileFile;
    if (dumpOpts.getTimingProfile) {
        // open file to dump timing profile
        timeProfileFile.open(dumpOpts.timeProfileLocation, std::ios::out);
        bg_shape_timer.start();
    }
    // get background and shape of snout
    if (dumpOpts.getTimingProfile) bg_shape_timer.start();
    // extract image background a number of frames linearly distributed in the video
    bgImage = getBackGroundImageAndShape(video, bwShape, &preprocessingOpts, generalOpts.startFrame, generalOpts.endFrame);
    if (dumpOpts.getTimingProfile) {
        bg_shape_timer.stop();
        timeProfileFile << std::endl << "Extracted background and mouse silhouette in " << bg_shape_timer.seconds() << " seconds"
                        << std::endl << std::endl;                                                    // measure FPS
    }
    if (dumpOpts.getTimingProfile) timeProfileFile.close();
}

void run_sequential(cv::VideoCapture video, cv::Mat &bgImage, cv::Mat &bwShape, timers &timers,
                    preprocessing_opts preprocessingOpts, c_extraction_opts cExtractionOpts, clustering_opts clusteringOpts,
                    stitch_I_opts stitchIOpts, stitch_II_opts stitchIIOpts, param_opts &paramOpts, track_opts &trackOpts,
                    train_opts &trainOpts, fwptd_opts fwptdOpts, recog_opts &recogOpts, N_expert_opts NExpertOpts,
                    plotting_opts plottingOpts, general_opts generalOpts, dump_opts dumpOpts, cv::Mat &overlaid_whiskers) {
    int numFrames = video.get(cv::CAP_PROP_FRAME_COUNT);
    std::ofstream timeProfileFile;
    if (dumpOpts.getTimingProfile) {
        // open file to dump timing profile
        timeProfileFile.open(dumpOpts.timeProfileLocation, std::ios::app);
        if (!timeProfileFile) perror("Time profile file could not be created.\n");
    }

    bool start = true;
    std::vector<whisker> lineGatherer_tracker;
    std::vector<int> order;
    std::vector<classSVMData> svmFeatures;
    std::vector<std::vector<float>> whisker_angles;
    std::vector<std::vector<whisker>> whiskers;
    std::vector<int> whiskers_detected_Nr;

    // Load existing tracks from file
    if (generalOpts.loadTracks) {
        loadTracks(start, generalOpts.loadTracksLocation,lineGatherer_tracker, order, trackOpts, trainOpts, fwptdOpts);
        if (dumpOpts.getQualityResults) {
            addAngles(whisker_angles, lineGatherer_tracker, trackOpts.couldNotFit);
            addWhiskers(whiskers, lineGatherer_tracker, trackOpts.couldNotFit);
            whiskers_detected_Nr.push_back(lineGatherer_tracker.size());
        }
    }

    video.set(cv::CAP_PROP_POS_FRAMES, generalOpts.startFrame);
    // run pipeline sequentially
    for (int i = generalOpts.startFrame; i < generalOpts.endFrame; i++) {
        // check for user interruption
        if (i % 10 == 0 && QThread::currentThread()->isInterruptionRequested()) {
            generalOpts.endFrame = i;
            // and exit
            break;
        }
        if (dumpOpts.getTimingProfile) {
            timers.FPS_timer.start();
            timers.CV_timer.start();
        }
        cv::Mat frame;

        // Capture frame-by-frame
        video >> frame;

        // If the frame is empty, break immediately
        if (frame.empty())
            break;

        // plot unprocessed frame with and without snout line if specified
        if (plottingOpts.plot_unprocessed_frame)
            showImage(frame, "Frame before any processing", plottingOpts.framing/2, false, plottingOpts);
        if (plottingOpts.plot_unprocessed_frame_with_snout_line)
            showImageWithSnout(frame, generalOpts.tip, generalOpts.middle, preprocessingOpts.upscaling,
                           plottingOpts.framing/2, plottingOpts, "Frame before any processing, with snout line");

        std::cout << "~~~~~~~~~~~~\n" << "Frame " << i + 1 << " of " << numFrames << std::endl;
        plottingOpts.current_frame_no = i+1;

        if (dumpOpts.getTimingProfile) {
            timers.CV_timer.stop();
            timers.CV_time += timers.CV_timer.seconds();
            std::cout << "OpenCV overhead took " << timers.CV_timer.seconds() << " seconds." << std::endl;
        }

        std::vector<int> clusters;
        std::vector<float> rotatedPositions;
        std::vector<whisker> lineGatherer_detector;
        run_pipeline(timers, frame, bgImage, bwShape, rotatedPositions, clusters, lineGatherer_detector,
                     preprocessingOpts, cExtractionOpts, clusteringOpts, stitchIOpts, stitchIIOpts, paramOpts,
                     plottingOpts, generalOpts);

        // plot whiskers in preview mode
        if (generalOpts.preview) {
                order = std::vector<int>(lineGatherer_detector.size()); // for colors
                for (int j = 0; j < lineGatherer_detector.size(); j++) {
                    order[j] = j;
                }
                overlaid_whiskers = plotLinesOverlaid(frame, preprocessingOpts.upscaling, lineGatherer_detector, lineGatherer_detector.size(),
                                   -1, order, plottingOpts, "Tracked Whiskers");
        }

        if (!trackOpts.dontTrack) {
            run_tracking(lineGatherer_detector, lineGatherer_tracker, rotatedPositions, clusters, order,
                         svmFeatures, start, i, generalOpts, trackOpts, trainOpts, fwptdOpts, recogOpts,
                         NExpertOpts, plottingOpts, timers);
            // plot whiskers or write video with whiskers
            if (trackOpts.plot || plottingOpts.writeVideoOutput) {
                overlaid_whiskers = plotLinesOverlaid(frame, preprocessingOpts.upscaling, lineGatherer_tracker, lineGatherer_tracker.size(),
                                   -1, order, plottingOpts, "Tracked Whiskers");
            }
            if (dumpOpts.getQualityResults) {
                // save whisker angles and number of whiskers detected
                addAngles(whisker_angles, lineGatherer_tracker, trackOpts.couldNotFit);
                addWhiskers(whiskers, lineGatherer_tracker, trackOpts.couldNotFit);
                whiskers_detected_Nr.push_back(lineGatherer_detector.size());
            }
        }
        // Save tracks
        if (start && dumpOpts.saveTracks) {
            saveTracks(lineGatherer_tracker, dumpOpts.saveTracksLocation);
        }

        start = false;
        if (dumpOpts.getTimingProfile) {
            timers.FPS_timer.stop();
            timers.loop_time += timers.FPS_timer.seconds();
            // Dump timing profile
            saveTimingProfile(timers, i, timeProfileFile);
            std::cout << "Frame processing took " << timers.FPS_timer.seconds() << " seconds" << std::endl;
        }
    }
    if (!trackOpts.dontTrack) {
        if (dumpOpts.getQualityResults) {
            getQualityResults(whiskers_detected_Nr, whisker_angles, whiskers, lineGatherer_tracker, trackOpts, generalOpts, plottingOpts, dumpOpts);
        }
        freeLinearProblem(svmFeatures, trackOpts, recogOpts);
    }

    if (dumpOpts.getTimingProfile) timeProfileFile.close();
}

void run_tracking(std::vector<whisker> &lineGatherer_detector, std::vector<whisker> &lineGatherer_tracker,
                  std::vector<float> &rotatedPositions, std::vector<int> &clusters, std::vector<int> &order,
                  std::vector<classSVMData> &svmFeatures, bool start, int i, general_opts generalOpts,
                  track_opts &trackOpts, train_opts &trainOpts, fwptd_opts fwptdOpts, recog_opts &recogOpts,
                  N_expert_opts NExpertOpts, plotting_opts plottingOpts, timers &timers) {

    timers.tt.start();
    // tracking and recognition
    tracking_and_recognition(lineGatherer_detector, lineGatherer_tracker, rotatedPositions, clusters, order,
                             start, i, generalOpts.startFrame,trackOpts, trainOpts, fwptdOpts, recogOpts, NExpertOpts, plottingOpts);
    timers.tt.stop();
    timers.tracking_time += timers.tt.seconds();
    if (trainOpts.verbose) std::cout << "Tracking and recognition took " << timers.tt.seconds() << " seconds" << std::endl;

    // training
    timers.tt.start();
    svmFeatures.resize(lineGatherer_tracker.size());
    if (i == generalOpts.startFrame) {
        allocateSVMFeatures(svmFeatures, trainOpts.maxSizeTrainingData);
        // ONE-VS-ONE strategy using libLinear
        for (size_t j = 0; j < lineGatherer_tracker.size(); j++) {
            for (size_t k = j + 1; k < lineGatherer_tracker.size(); k++) {
                recogOpts.ks.push_back(k);
                recogOpts.js.push_back(j);
            }
        }
        allocateLinearProblemOnevsOne(trainOpts.maxSizeTrainingData, lineGatherer_tracker.size(), trainOpts.numFeatures,
                                      recogOpts);
        configLinearEps(trainOpts.linear_param);
    }
    training(lineGatherer_tracker, order, svmFeatures, i, generalOpts.startFrame,
             trackOpts, fwptdOpts, trainOpts, recogOpts, plottingOpts);
    timers.tt.stop();
    timers.training_time += timers.tt.seconds();
    if (trainOpts.verbose) std::cout << "Training took " << timers.tt.seconds() << " seconds" << std::endl;
}

void loadPreviousTracks(std::vector<int> &order, std::vector<whisker> &lineGatherer_tracker, track_opts &trackOpts,
                        train_opts &trainOpts, fwptd_opts &fwptdOpts) {
    // initialize several tracking vectors
    order.resize(lineGatherer_tracker.size());
    for (size_t i = 0; i < lineGatherer_tracker.size(); i++) {
        order[i] = i;
    }
    // whiskers are already sorted by xf (snout position)
//    std::sort(lineGatherer_tracker.begin(), lineGatherer_tracker.end(), sort_position);
    trackOpts.couldNotFit.assign(lineGatherer_tracker.size(), false); // ... so no missed whiskers yet.
    // The average of the whisker parameters is calculated, this is useful as training data.
    trackOpts.oldAverages = whiskerAverages(lineGatherer_tracker, order, true);
    trackOpts.prevAverages = whiskerAverages(lineGatherer_tracker, order, false);
    trackOpts.whiskerDb.resize(lineGatherer_tracker.size());

    whisker currentMean = whiskerAverages(lineGatherer_tracker, order, false);

    // configure whisker database (add new detected whiskers' features)
    configureWhiskerDB(lineGatherer_tracker, trackOpts, fwptdOpts, trainOpts);
    trackOpts.prevMean = currentMean;
    trainOpts.newDataSize++; // increment size of new data to train in next training
}
