/*
 * Copyright 1993-2015 NVIDIA Corporation.  All rights reserved.
 *
 * Please refer to the NVIDIA end user license agreement (EULA) associated
 * with this source code for terms and conditions that govern your use of
 * this software. Any use, reproduction, disclosure, or distribution of
 * this software and related documentation outside the terms of the EULA
 * is strictly prohibited.
 *
 */

/* This file was edited by P. Arvanitis to generalize the acceptable image dimensions and the kernel size */

#include <assert.h>
#include <helper_cuda.h>
#include <cooperative_groups.h>

namespace cg = cooperative_groups;
#include "convolutionSeparable_common.h"

#define CHECK(err) if (err != cudaSuccess) { \
    printf("%s in %s at line %d\n", cudaGetErrorString(err), __FILE__, __LINE__); \
    exit(EXIT_FAILURE); \
}

////////////////////////////////////////////////////////////////////////////////
// Convolution Kernel's Constants
////////////////////////////////////////////////////////////////////////////////
void validKernelConstants(int &BLOCKDIM1, int &BLOCKDIM2, int &RESULT_STEPS, int &HALO_STEPS,
                        int imageDim1, int imageDim2, int KERNEL_RADIUS_MAX) {
    int STEPS[8] = {8, 10, 12, 14, 6, 4};
    bool success = false;
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 6; j++) {
            RESULT_STEPS = STEPS[j];
            if (imageDim1 % (RESULT_STEPS * BLOCKDIM1) == 0) {
                success = true;
                break;
            }
        }
        if (success) break;
        BLOCKDIM1 /= 2;
    }
    while (!(BLOCKDIM1 * HALO_STEPS >= KERNEL_RADIUS_MAX)) {
        HALO_STEPS += 1;
    }
    // ensure that a block consists of at least 32 threads (warp size)
    if (BLOCKDIM1 * BLOCKDIM2 < 32) BLOCKDIM2 = 32 / BLOCKDIM1;
    while (imageDim2 % BLOCKDIM2 != 0) {
        BLOCKDIM2 /= 2;
    }
}


////////////////////////////////////////////////////////////////////////////////
// Convolution kernel storage
////////////////////////////////////////////////////////////////////////////////
__constant__ float c_Kernel[KERNEL_LENGTH];

extern "C" void setConvolutionKernel(float *h_Kernel)
{
    cudaMemcpyToSymbol(c_Kernel, h_Kernel, KERNEL_LENGTH * sizeof(float));
}

extern "C" void setConvolutionKernelV2(float *h_Kernel, int kernel_size)
{
    cudaMemcpyToSymbol(c_Kernel, h_Kernel, kernel_size * sizeof(float));
}

extern "C" void setConvolutionKernelInGlobalMem(float *h_Kernel, int kernel_size, int offset)
{
    cudaMemcpyToSymbol(c_Kernel, h_Kernel, kernel_size * sizeof(float), offset*sizeof(float));
}

__global__ void printKernel(float *d_Kernel, int offset) {
    printf("kernel(%d,%d) = %f\n", threadIdx.x, threadIdx.y, c_Kernel[offset + threadIdx.x]);
}

////////////////////////////////////////////////////////////////////////////////
// Row convolution filter
////////////////////////////////////////////////////////////////////////////////
//#define   ROWS_BLOCKDIM_X 16
//#define   ROWS_BLOCKDIM_Y 4
//#define ROWS_RESULT_STEPS 8
//#define   ROWS_HALO_STEPS 1

__global__ void convolutionRowsKernel(
    float *d_Dst,
    float *d_Src,
    int imageW,
    int imageH,
    int pitch,
    const float *d_Kernel, int kernelOffset,
    int ROWS_BLOCKDIM_X, int ROWS_BLOCKDIM_Y, int ROWS_RESULT_STEPS, int ROWS_HALO_STEPS,
    int KERNEL_CENTRE, int KERNEL_RADIUS_LEFT, int KERNEL_RADIUS_RIGHT
)
{
    // Handle to thread block group
    cg::thread_block cta = cg::this_thread_block();
//    __shared__ float s_Data[ROWS_BLOCKDIM_Y][(ROWS_RESULT_STEPS + 2 * ROWS_HALO_STEPS) * ROWS_BLOCKDIM_X];
    extern __shared__ float s_Data[];
    int dsW = (ROWS_RESULT_STEPS + 2 * ROWS_HALO_STEPS) * ROWS_BLOCKDIM_X; // shared memory width

    //Offset to the left halo edge
    const int baseX = (blockIdx.x * ROWS_RESULT_STEPS - ROWS_HALO_STEPS) * ROWS_BLOCKDIM_X + threadIdx.x;
    const int baseY = blockIdx.y * ROWS_BLOCKDIM_Y + threadIdx.y;

    d_Src += baseY * pitch + baseX;
    d_Dst += baseY * pitch + baseX;

    //Load main data
#pragma unroll

    for (int i = ROWS_HALO_STEPS; i < ROWS_HALO_STEPS + ROWS_RESULT_STEPS; i++)
    {
//        s_Data[threadIdx.y][threadIdx.x + i * ROWS_BLOCKDIM_X] = d_Src[i * ROWS_BLOCKDIM_X];
        s_Data[threadIdx.y*dsW + threadIdx.x + i * ROWS_BLOCKDIM_X] = d_Src[i * ROWS_BLOCKDIM_X];
    }

    //Load left halo
#pragma unroll

    for (int i = 0; i < ROWS_HALO_STEPS; i++)
    {
//        s_Data[threadIdx.y][threadIdx.x + i * ROWS_BLOCKDIM_X] = (baseX >= -i * ROWS_BLOCKDIM_X) ? d_Src[i * ROWS_BLOCKDIM_X] : 0;
        s_Data[threadIdx.y*dsW + threadIdx.x + i * ROWS_BLOCKDIM_X] = (baseX >= -i * ROWS_BLOCKDIM_X) ? d_Src[i * ROWS_BLOCKDIM_X] : 0;
    }

    //Load right halo
#pragma unroll

    for (int i = ROWS_HALO_STEPS + ROWS_RESULT_STEPS; i < ROWS_HALO_STEPS + ROWS_RESULT_STEPS + ROWS_HALO_STEPS; i++)
    {
//        s_Data[threadIdx.y][threadIdx.x + i * ROWS_BLOCKDIM_X] = (imageW - baseX > i * ROWS_BLOCKDIM_X) ? d_Src[i * ROWS_BLOCKDIM_X] : 0;
        s_Data[threadIdx.y*dsW + threadIdx.x + i * ROWS_BLOCKDIM_X] = (imageW - baseX > i * ROWS_BLOCKDIM_X) ? d_Src[i * ROWS_BLOCKDIM_X] : 0;
    }

    //Compute and store results
    cg::sync(cta);
#pragma unroll

    for (int i = ROWS_HALO_STEPS; i < ROWS_HALO_STEPS + ROWS_RESULT_STEPS; i++)
    {
        float sum = 0;

#pragma unroll

        for (int j = -KERNEL_RADIUS_RIGHT; j <= KERNEL_RADIUS_LEFT; j++)
        {
//            sum += c_Kernel[KERNEL_RADIUS - j] * s_Data[threadIdx.y][threadIdx.x + i * ROWS_BLOCKDIM_X + j];
//            sum += c_Kernel[KERNEL_CENTRE - j] * s_Data[threadIdx.y*dsW + threadIdx.x + i * ROWS_BLOCKDIM_X + j];
            sum += c_Kernel[kernelOffset + KERNEL_CENTRE - j] * s_Data[threadIdx.y*dsW + threadIdx.x + i * ROWS_BLOCKDIM_X + j];
//            sum += d_Kernel[KERNEL_RADIUS - j] * s_Data[threadIdx.y*dsW + threadIdx.x + i * ROWS_BLOCKDIM_X + j];
        }

        d_Dst[i * ROWS_BLOCKDIM_X] = sum;
    }
}

extern "C" void convolutionRowsGPU(
    float *d_Dst,
    float *d_Src,
    int imageW,
    int imageH,
    float *d_Kernel,
    int kernel_size,
    int pitch,
    int kernelOffset
)
{
    int ROWS_BLOCKDIM_X = 16;
    int ROWS_BLOCKDIM_Y = 4;
    int ROWS_RESULT_STEPS = 8;
    int ROWS_HALO_STEPS = 1;
    int KERNEL_CENTRE = kernel_size / 2;
    int KERNEL_RADIUS_LEFT = kernel_size / 2;        // for even kernels, there is one more element on the left
    int KERNEL_RADIUS_RIGHT = (kernel_size - 1) / 2; // than on the right by convention

    // find valid values for the GPU kernel constants that will abide by the assumptions
    validKernelConstants(ROWS_BLOCKDIM_X, ROWS_BLOCKDIM_Y, ROWS_RESULT_STEPS, ROWS_HALO_STEPS, imageW, imageH, KERNEL_RADIUS_LEFT);
    assert(ROWS_BLOCKDIM_X * ROWS_HALO_STEPS >= KERNEL_RADIUS_LEFT);
    assert(imageW % (ROWS_RESULT_STEPS * ROWS_BLOCKDIM_X) == 0);
    assert(imageH % ROWS_BLOCKDIM_Y == 0);

//    printKernel<<<1,kernel_size>>>(d_Kernel, kernelOffset);
//    CHECK(cudaDeviceSynchronize());

    dim3 blocks(imageW / (ROWS_RESULT_STEPS * ROWS_BLOCKDIM_X), imageH / ROWS_BLOCKDIM_Y);
    dim3 threads(ROWS_BLOCKDIM_X, ROWS_BLOCKDIM_Y);

    size_t shared_size = (ROWS_BLOCKDIM_Y)*((ROWS_RESULT_STEPS + 2 * ROWS_HALO_STEPS)*ROWS_BLOCKDIM_X)*sizeof(float);
    convolutionRowsKernel<<<blocks, threads, shared_size>>>(
        d_Dst,
        d_Src,
        imageW,
        imageH,
        pitch,
        d_Kernel, kernelOffset,
        ROWS_BLOCKDIM_X, ROWS_BLOCKDIM_Y, ROWS_RESULT_STEPS, ROWS_HALO_STEPS,
        KERNEL_CENTRE, KERNEL_RADIUS_LEFT, KERNEL_RADIUS_RIGHT
    );
    getLastCudaError("convolutionRowsKernel() execution failed\n");
}



////////////////////////////////////////////////////////////////////////////////
// Column convolution filter
////////////////////////////////////////////////////////////////////////////////
//#define   COLUMNS_BLOCKDIM_X 16
//#define   COLUMNS_BLOCKDIM_Y 8
//#define COLUMNS_RESULT_STEPS 8
//#define   COLUMNS_HALO_STEPS 1

__global__ void convolutionColumnsKernel(
    float *d_Dst,
    float *d_Src,
    int imageW,
    int imageH,
    int pitch,
    const float *d_Kernel, int kernelOffset,
    int COLUMNS_BLOCKDIM_X, int COLUMNS_BLOCKDIM_Y, int COLUMNS_RESULT_STEPS, int COLUMNS_HALO_STEPS,
    int KERNEL_CENTRE, int KERNEL_RADIUS_UP, int KERNEL_RADIUS_DOWN
)
{
    // Handle to thread block group
    cg::thread_block cta = cg::this_thread_block();
//    __shared__ float s_Data[COLUMNS_BLOCKDIM_X][(COLUMNS_RESULT_STEPS + 2 * COLUMNS_HALO_STEPS) * COLUMNS_BLOCKDIM_Y + 1];
    extern __shared__ float s_Data[];
    int dsW = (COLUMNS_RESULT_STEPS + 2 * COLUMNS_HALO_STEPS) * COLUMNS_BLOCKDIM_Y + 1; // shared memory width

    //Offset to the upper halo edge
    const int baseX = blockIdx.x * COLUMNS_BLOCKDIM_X + threadIdx.x;
    const int baseY = (blockIdx.y * COLUMNS_RESULT_STEPS - COLUMNS_HALO_STEPS) * COLUMNS_BLOCKDIM_Y + threadIdx.y;
    d_Src += baseY * pitch + baseX;
    d_Dst += baseY * pitch + baseX;

    //Main data
#pragma unroll

    for (int i = COLUMNS_HALO_STEPS; i < COLUMNS_HALO_STEPS + COLUMNS_RESULT_STEPS; i++)
    {
//        s_Data[threadIdx.x][threadIdx.y + i * COLUMNS_BLOCKDIM_Y] = d_Src[i * COLUMNS_BLOCKDIM_Y * pitch];
        s_Data[threadIdx.x*dsW + threadIdx.y + i * COLUMNS_BLOCKDIM_Y] = d_Src[i * COLUMNS_BLOCKDIM_Y * pitch];
    }

    //Upper halo
#pragma unroll

    for (int i = 0; i < COLUMNS_HALO_STEPS; i++)
    {
//        s_Data[threadIdx.x][threadIdx.y + i * COLUMNS_BLOCKDIM_Y] = (baseY >= -i * COLUMNS_BLOCKDIM_Y) ? d_Src[i * COLUMNS_BLOCKDIM_Y * pitch] : 0;
        s_Data[threadIdx.x*dsW + threadIdx.y + i * COLUMNS_BLOCKDIM_Y] = (baseY >= -i * COLUMNS_BLOCKDIM_Y) ? d_Src[i * COLUMNS_BLOCKDIM_Y * pitch] : 0;
    }

    //Lower halo
#pragma unroll

    for (int i = COLUMNS_HALO_STEPS + COLUMNS_RESULT_STEPS; i < COLUMNS_HALO_STEPS + COLUMNS_RESULT_STEPS + COLUMNS_HALO_STEPS; i++)
    {
//        s_Data[threadIdx.x][threadIdx.y + i * COLUMNS_BLOCKDIM_Y]= (imageH - baseY > i * COLUMNS_BLOCKDIM_Y) ? d_Src[i * COLUMNS_BLOCKDIM_Y * pitch] : 0;
        s_Data[threadIdx.x*dsW + threadIdx.y + i * COLUMNS_BLOCKDIM_Y]= (imageH - baseY > i * COLUMNS_BLOCKDIM_Y) ? d_Src[i * COLUMNS_BLOCKDIM_Y * pitch] : 0;
    }

    //Compute and store results
    cg::sync(cta);
#pragma unroll

    for (int i = COLUMNS_HALO_STEPS; i < COLUMNS_HALO_STEPS + COLUMNS_RESULT_STEPS; i++)
    {
        float sum = 0;
#pragma unroll

        for (int j = -KERNEL_RADIUS_DOWN; j <= KERNEL_RADIUS_UP; j++)
        {
//            sum += c_Kernel[KERNEL_RADIUS - j] * s_Data[threadIdx.x][threadIdx.y + i * COLUMNS_BLOCKDIM_Y + j];
//            sum += c_Kernel[KERNEL_CENTRE - j] * s_Data[threadIdx.x*dsW + threadIdx.y + i * COLUMNS_BLOCKDIM_Y + j];
            sum += c_Kernel[kernelOffset + KERNEL_CENTRE - j] * s_Data[threadIdx.x*dsW + threadIdx.y + i * COLUMNS_BLOCKDIM_Y + j];
//            sum += d_Kernel[KERNEL_RADIUS - j] * s_Data[threadIdx.x*dsW + threadIdx.y + i * COLUMNS_BLOCKDIM_Y + j];
        }

        d_Dst[i * COLUMNS_BLOCKDIM_Y * pitch] = sum;
    }
}

extern "C" void convolutionColumnsGPU(
    float *d_Dst,
    float *d_Src,
    int imageW,
    int imageH,
    float *d_Kernel,
    int kernel_size,
    int pitch,
    int kernelOffset
)
{
    int COLUMNS_BLOCKDIM_X = 16;
    int COLUMNS_BLOCKDIM_Y = 8;
    int COLUMNS_RESULT_STEPS = 8;
    int COLUMNS_HALO_STEPS = 1;
    int KERNEL_CENTRE = kernel_size / 2;
    int KERNEL_RADIUS_UP = kernel_size / 2;
    int KERNEL_RADIUS_DOWN = (kernel_size - 1) / 2;
    // find valid values for the GPU kernel constants that will abide by the assumptions
    validKernelConstants(COLUMNS_BLOCKDIM_Y, COLUMNS_BLOCKDIM_X, COLUMNS_RESULT_STEPS, COLUMNS_HALO_STEPS, imageH, imageW, KERNEL_RADIUS_UP);
    assert(COLUMNS_BLOCKDIM_Y * COLUMNS_HALO_STEPS >= KERNEL_RADIUS_UP);
    assert(imageW % COLUMNS_BLOCKDIM_X == 0);
    assert(imageH % (COLUMNS_RESULT_STEPS * COLUMNS_BLOCKDIM_Y) == 0);

//    printKernel<<<1,KERNEL_LENGTH>>>(d_Kernel);
//    CHECK(cudaDeviceSynchronize());

    dim3 blocks(imageW / COLUMNS_BLOCKDIM_X, imageH / (COLUMNS_RESULT_STEPS * COLUMNS_BLOCKDIM_Y));
    dim3 threads(COLUMNS_BLOCKDIM_X, COLUMNS_BLOCKDIM_Y);

    int shared_size = COLUMNS_BLOCKDIM_X*((COLUMNS_RESULT_STEPS + 2 * COLUMNS_HALO_STEPS) * COLUMNS_BLOCKDIM_Y + 1)*sizeof(float);
    convolutionColumnsKernel<<<blocks, threads, shared_size>>>(
        d_Dst,
        d_Src,
        imageW,
        imageH,
        pitch,
        d_Kernel, kernelOffset,
        COLUMNS_BLOCKDIM_X, COLUMNS_BLOCKDIM_Y, COLUMNS_RESULT_STEPS, COLUMNS_HALO_STEPS,
        KERNEL_CENTRE, KERNEL_RADIUS_UP, KERNEL_RADIUS_DOWN
    );
    getLastCudaError("convolutionColumnsKernel() execution failed\n");
}

