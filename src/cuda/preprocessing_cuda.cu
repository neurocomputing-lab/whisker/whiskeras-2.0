/*
	this file is part of whiskeras 2.0.
	
	whiskeras 2.0 is free software: you can redistribute it and/or modify
	it under the terms of the gnu general public license as published by
	the free software foundation, either version 3 of the license, or
	(at your option) any later version.
	
	whiskeras 2.0 is distributed in the hope that it will be useful,
	but without any warranty; without even the implied warranty of
	merchantability or fitness for a particular purpose.  see the
	gnu general public license for more details.
	
	you should have received a copy of the gnu general public license
	along with whiskeras 2.0.  if not, see <https://www.gnu.org/licenses/>.
*/

#include "preprocessing_cuda.h"
#include "../stages/whisker_point_detection/imgproc.h"
#include "cuda_kernels.h"
#include "../utils/Timer.hpp"

#define CHECK(err) if (err != cudaSuccess) { \
    printf("%s in %s at line %d\n", cudaGetErrorString(err), __FILE__, __LINE__); \
    exit(EXIT_FAILURE); \
}

void preprocessing_cuda(cv::cuda::GpuMat src, cv::cuda::GpuMat bgImage, cv::cuda::GpuMat bwShape,
        cv::cuda::GpuMat& upscaledImage, const preprocessing_opts pre_opts, plotting_opts plottingOpts) {

    int BLOCK_WIDTH = 32;
    Timer tt;

    // perform deinterlacing if specified
    if (pre_opts.deinterlacingFlag) {
        dim3 dimGrid(src.cols/BLOCK_WIDTH + 1, (src.rows/2)/BLOCK_WIDTH + 1, 1);
        dim3 dimBlock(BLOCK_WIDTH, BLOCK_WIDTH, 1);
        // using line repetition
        deinterlaceLineRepImage_cuda<<<dimGrid, dimBlock>>>(src);
    }

    // remove background and snout silhouette
    if (pre_opts.verbose > 0) tt.start();
    cv::cuda::subtract(bgImage, src, src);
    cv::cuda::multiply(src, bwShape, src);
    if (pre_opts.verbose > 0) cudaDeviceSynchronize();
    if (pre_opts.verbose > 0) tt.stop();
    if (pre_opts.verbose > 0) std::cout << "\tsubtract/multiply took " << tt.seconds() << " seconds" << std::endl;
    // perform gaussian blur if specified
    if (pre_opts.initialGaussianSigma > 0) {
        cv::Ptr<cv::cuda::Filter> gaussianFilter = cv::cuda::createGaussianFilter(CV_8UC1,
                CV_8UC1, cv::Size(2*ceil(2*pre_opts.initialGaussianSigma)+1,
                        2*ceil(2*pre_opts.initialGaussianSigma)+1),
                        pre_opts.initialGaussianSigma, pre_opts.initialGaussianSigma);
        gaussianFilter->apply(src, src);
        if (pre_opts.verbose > 0) cudaDeviceSynchronize();
    }

    // remove edge (framing) of image and enhance contrast
    if (pre_opts.verbose > 0)tt.start();
    cv::cuda::GpuMat preprocessed(src.rows, src.cols, CV_32FC1);
    dim3 dimGrid(src.cols/BLOCK_WIDTH + 1, src.rows/BLOCK_WIDTH + 1, 1);
    dim3 dimBlock(BLOCK_WIDTH, BLOCK_WIDTH, 1);
    imadjust_removeEdge_cuda<<<dimGrid, dimBlock>>>(src, preprocessed, pre_opts.adjustRangeIn[0], pre_opts.adjustRangeIn[1],
            pre_opts.adjustRangeOut[0], pre_opts.adjustRangeOut[1], pre_opts.histEqGamma, pre_opts.framing);
    if (pre_opts.verbose > 0) tt.stop();
    if (pre_opts.verbose > 0) cudaDeviceSynchronize();
    if (pre_opts.verbose > 0) std::cout << "\timadjust took " << tt.seconds() << " seconds" << std::endl;

    // remove low levels of intensity from image
    if (pre_opts.verbose > 0) tt.start();
    cv::cuda::GpuMat lowlevel_img;
    cv::cuda::threshold(preprocessed, lowlevel_img, pre_opts.remove_lowlevels, 1, cv::THRESH_BINARY);
    cv::cuda::multiply(preprocessed, lowlevel_img, preprocessed);
    if (pre_opts.verbose > 0) cudaDeviceSynchronize();
    if (pre_opts.verbose > 0) tt.stop();
    if (pre_opts.verbose > 0) std::cout << "\tthreshold/multiply took " << tt.seconds() << " seconds" << std::endl;

    if (pre_opts.verbose > 0) tt.start();
    // very thin whiskers are difficult to detect by the Steger algorithm. Therefore, upscaling is necessary
    if (pre_opts.upscaling != 1) cv::cuda::resize(preprocessed, upscaledImage,
               cv::Size(lround(pre_opts.upscaling*src.cols),
                        lround(pre_opts.upscaling*src.rows)), 0, 0, cv::INTER_CUBIC);
    else upscaledImage = preprocessed;
    if (pre_opts.verbose > 0) cudaDeviceSynchronize();
    if (pre_opts.verbose > 0) tt.stop();
    if (pre_opts.verbose > 0) std::cout << "\tresizing took " << tt.seconds() << " seconds" << std::endl;

    if (pre_opts.plot) {
        cv::cuda::GpuMat upscaledImage_UCHAR;
        upscaledImage.convertTo(upscaledImage_UCHAR, CV_8U);
        dim3 dimGrid(upscaledImage_UCHAR.cols/BLOCK_WIDTH + 1, upscaledImage_UCHAR.rows/BLOCK_WIDTH + 1, 1);
        dim3 dimBlock(BLOCK_WIDTH, BLOCK_WIDTH, 1);
        invertImgColors<<<dimGrid, dimBlock>>>(upscaledImage_UCHAR);
        showImage_cuda(upscaledImage_UCHAR, "Frame after preprocessing", true, plottingOpts);
    }
}

cv::cuda::GpuMat getBackGroundImageAndShape_cuda(cv::VideoCapture video, cv::cuda::GpuMat &bwShape_d, float *bgImageData,
                                                 float *bwShapeData, preprocessing_opts pre_opts, int startFrame, int endFrame) {
    int videoWidth = video.get(cv::CAP_PROP_FRAME_WIDTH);
    int videoHeight = video.get(cv::CAP_PROP_FRAME_HEIGHT);
    // initialize background image as a black image
    cv::Mat bgImage(cv::Size(videoWidth, videoHeight), CV_8UC1, cv::Scalar(0));

    // step over frames to perform background extraction
    int framesStep = (endFrame - startFrame) / 60;
    if (framesStep < 1) framesStep = 1;

    cv::Mat frame;
    for (int i=startFrame; i < endFrame; i += framesStep) {

        video.set(cv::CAP_PROP_POS_FRAMES, i);
        video >> frame;

        // If the frame is empty, break immediately
        if (frame.empty())
            break;

        // convert to grayscale
        cv::cvtColor(frame, frame, cv::COLOR_BGR2GRAY);

        cv::Mat whiteImage(frame.rows, frame.cols, frame.type(), cv::Scalar(255));
        // invert colors if background is black and whiskers are white
        if (pre_opts.invertColors) {
            cv::subtract(whiteImage, frame, frame);
        }

        if (pre_opts.deinterlacingFlag) {
            // deinterlacing using line repetition
            deinterlaceLineRepImage(frame);
        }

        // since the background is light, extract the brightest pixels
        bgImage = cv::max(bgImage, frame);

        if (i == startFrame) {
            cv::cuda::GpuMat frame_d(frame);  
            cudaMalloc(&bwShapeData, videoHeight*videoWidth*sizeof(uint8_t));
            bwShape_d = cv::cuda::GpuMat(videoHeight, videoWidth, CV_8UC1, bwShapeData, cv::Mat::CONTINUOUS_FLAG);
            bwShape_d = extractWhiskersGrayValuesWithMorphology_cuda(frame_d, pre_opts.bwThreshold, pre_opts.dilate_value);
        }
    }
    // go to starting frame for the rest of the processing
    video.set(cv::CAP_PROP_POS_FRAMES , 0);

//    cv::cuda::GpuMat bgImage_d(bgImage);
    cudaMalloc(&bgImageData, videoHeight*videoWidth*sizeof(uint8_t));
    cudaMemcpy(bgImageData, bgImage.data, videoHeight*videoWidth*sizeof(uint8_t), cudaMemcpyHostToDevice);
    cv::cuda::GpuMat bgImage_d(videoHeight, videoWidth, CV_8UC1, bgImageData);

    return bgImage_d;
}

cv::cuda::GpuMat extractWhiskersGrayValuesWithMorphology_cuda(cv::cuda::GpuMat frame, float bwThreshold, int razorSize) {
    cv::cuda::GpuMat BWtemp, bwShape;
    // perform thresholding to the image
    cv::cuda::threshold(frame, BWtemp, bwThreshold, 255, cv::THRESH_BINARY);

    BWtemp = dilation_cuda(BWtemp, 2, 2);
    bwShape = erosion_cuda(BWtemp, razorSize, 2);

    // assign all its non-black pixels to 1 to multiply this later with every frame
    cv::cuda::threshold(bwShape, bwShape, 0, 1, cv::THRESH_BINARY);

    return bwShape;
}

void allocImageData(float *image_data, int height, int weight) {
    cudaMalloc(&image_data, height*weight*sizeof(float));
}

void freeImageData(float *image_data, int height, int weight) {
    cudaFree(image_data);
}
