/*
    This file is part of WhiskEras 2.0.

    WhiskEras 2.0 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WhiskEras 2.0 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WhiskEras 2.0.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef FWTS_C_PREPROCESSING_CUDA_H
#define FWTS_C_PREPROCESSING_CUDA_H

#pragma once

#include "../stages/whisker_point_detection/preprocessing.h"
// cuda
#include <opencv2/core/cuda.hpp>
#include <opencv2/cudaarithm.hpp>
#include <opencv2/cudaimgproc.hpp>
#include <opencv2/cudafilters.hpp>

void preprocessing_cuda(cv::cuda::GpuMat src, cv::cuda::GpuMat bgImage, cv::cuda::GpuMat bwShape,
                        cv::cuda::GpuMat& upscaledImage, const preprocessing_opts pre_opts, plotting_opts plottingOpts);

cv::cuda::GpuMat getBackGroundImageAndShape_cuda(cv::VideoCapture video, cv::cuda::GpuMat &bwShape_d, float *bgImageData,
                                                 float *bwShapeData, preprocessing_opts pre_opts, int startFrame, int endFrame);

cv::cuda::GpuMat extractWhiskersGrayValuesWithMorphology_cuda(cv::cuda::GpuMat frame, float bwThreshold, int razorSize);

void allocImageData(float *image_data, int height, int weight);

void freeImageData(float *image_data, int height, int weight);

#endif //FWTS_C_PREPROCESSING_CUDA_H
