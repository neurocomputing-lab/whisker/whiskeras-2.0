/*
    This file is part of WhiskEras 2.0.

    WhiskEras 2.0 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WhiskEras 2.0 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WhiskEras 2.0.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "c_extraction_cuda.h"
#include "cuda_kernels.h"
#include "../stages/whisker_point_detection/imgproc.h"
#include "../stages/whisker_point_detection/c_extraction.h"
#include "../utils/Timer.hpp"
#include "samples/convolutionSeparable_common.h"
#define CUB_IGNORE_DEPRECATED_CPP_DIALECT
#include <cub/cub.cuh>



#define CHECK(err) if (err != cudaSuccess) { \
    printf("%s in %s at line %d\n", cudaGetErrorString(err), __FILE__, __LINE__); \
    exit(EXIT_FAILURE); \
}

void steger_point_detection_cuda(std::vector<std::pair<int16_t,int16_t>> &startPoints, const c_extraction_opts &cExtractionOpts,
                                 c_extraction_cuda cExtractionCuda) {
    Timer tt;
    if (cExtractionOpts.verbose > 0) tt.start();

    // Perform convolutions to obtain Hessian Matrix using separable filters
    int width = cExtractionCuda.width;
    int height = cExtractionCuda.height;
    int pitch = cExtractionCuda.width;

    int kernel_size = cExtractionOpts.gaussianKernelSize;
    int k = 0;
    performSeparableConvolution(cExtractionCuda.d_preprocessedImage, cExtractionCuda.gxRow, cExtractionCuda.gxCol, cExtractionCuda.rx,
                                cExtractionCuda, kernel_size-1, kernel_size, pitch, k*kernel_size, kernel_size);
    k += 2;
    performSeparableConvolution(cExtractionCuda.d_preprocessedImage, cExtractionCuda.gxxRow, cExtractionCuda.gxxCol, cExtractionCuda.rxx,
                                cExtractionCuda, kernel_size-2, kernel_size, pitch, k*kernel_size, kernel_size);
    k += 2;
    performSeparableConvolution(cExtractionCuda.d_preprocessedImage, cExtractionCuda.gxyRow, cExtractionCuda.gxyCol, cExtractionCuda.rxy,
                                cExtractionCuda, kernel_size-1, kernel_size-1, pitch, k*kernel_size, kernel_size);
    k += 2;
    performSeparableConvolution(cExtractionCuda.d_preprocessedImage, cExtractionCuda.gyRow, cExtractionCuda.gyCol, cExtractionCuda.ry,
                                cExtractionCuda, kernel_size, kernel_size-1, pitch, k*kernel_size, kernel_size);
    k += 2;
    performSeparableConvolution(cExtractionCuda.d_preprocessedImage, cExtractionCuda.gyyRow, cExtractionCuda.gyyCol, cExtractionCuda.ryy,
                                cExtractionCuda, kernel_size, kernel_size-2, pitch, k*kernel_size, kernel_size);

    if (cExtractionOpts.verbose > 0) CHECK(cudaDeviceSynchronize());
    if (cExtractionOpts.verbose > 0) tt.stop();
    if (cExtractionOpts.verbose > 0) std::cout << "\tConvolutions took " << tt.seconds() << " seconds" << std::endl;

    // Steger's algorithm
    if (cExtractionOpts.verbose > 0) tt.start();
    // using a CUDA kernel
    int BLOCK_WIDTH = 32;
    dim3 dimGrid(width/BLOCK_WIDTH + 1, height/BLOCK_WIDTH + 1, 1);
    dim3 dimBlock(BLOCK_WIDTH, BLOCK_WIDTH, 1);
    stegerDetection_cuda<<<dimGrid, dimBlock>>>(cExtractionCuda.startPoints, cExtractionCuda.flags_startPoints, cExtractionCuda.egv,
                                                cExtractionCuda.whiskerAngles, cExtractionCuda.rx, cExtractionCuda.ry,
                                                cExtractionCuda.rxx, cExtractionCuda.rxy, cExtractionCuda.ryy, cExtractionOpts.a_value,
                                                cExtractionOpts.b_value, cExtractionCuda.height, cExtractionCuda.width, cExtractionOpts.tr2,
                                                cExtractionOpts.upside_whiskers);
    if (cExtractionOpts.verbose > 0) CHECK(cudaDeviceSynchronize());
    if (cExtractionOpts.verbose > 0) tt.stop();
    if (cExtractionOpts.verbose > 0) std::cout << "\tSteger algorithm took " << tt.seconds() << " seconds" << std::endl;

    // Keep only valid whisker-point values
    if (cExtractionOpts.verbose > 1) tt.start();
    CubDebugExit(cub::DeviceSelect::Flagged(cExtractionCuda.flagged_storage, cExtractionCuda.flagged_storage_bytes, (int32_t*) cExtractionCuda.startPoints, (uint8_t*) cExtractionCuda.flags_startPoints,
                                            (int32_t*) cExtractionCuda.startPoints_compact, cExtractionCuda.numStartPoints, cExtractionCuda.height * cExtractionCuda.width));
    CubDebugExit(cub::DeviceSelect::Flagged(cExtractionCuda.flagged_storage, cExtractionCuda.flagged_storage_bytes, (float*) cExtractionCuda.egv, (uint8_t*) cExtractionCuda.flags_startPoints,
                                            (float*) cExtractionCuda.egv_compact, cExtractionCuda.numStartPoints, cExtractionCuda.height * cExtractionCuda.width));

    CHECK(cudaMemset(cExtractionCuda.flags_startPoints, 0, cExtractionCuda.width*cExtractionCuda.height*sizeof(uint8_t)));
    int numStartPoints;
    CHECK(cudaMemcpy(&numStartPoints, cExtractionCuda.numStartPoints, sizeof(int), cudaMemcpyDeviceToHost));
    if (cExtractionOpts.verbose > 1) std::cout << "\tNumber of starting starting points found: " << numStartPoints << std::endl;
    if (cExtractionOpts.verbose > 1) CHECK(cudaDeviceSynchronize());
    if (cExtractionOpts.verbose > 1) tt.stop();
    if (cExtractionOpts.verbose > 1) std::cout << "\tFlagged operation took " << tt.seconds() << " seconds." << std::endl;

    // Radix-Sort starting whisker points according to second directional derivative value
    if (cExtractionOpts.verbose > 1) tt.start();
    // allocate enough array storage
    void *radix_storage = NULL;
    size_t radix_storage_bytes = 0;
    CubDebugExit(cub::DeviceRadixSort::SortPairsDescending(radix_storage, radix_storage_bytes, cExtractionCuda.egv_compact, cExtractionCuda.egv_sorted,
                                                 cExtractionCuda.startPoints_compact, cExtractionCuda.startPoints_sorted, numStartPoints));
    if (cExtractionOpts.verbose > 1) std::cout << "\tCUB allocated " << radix_storage_bytes << " bytes." << std::endl;

    CHECK(cudaMalloc(&radix_storage, radix_storage_bytes));
    // perform actual sorting
    CubDebugExit(cub::DeviceRadixSort::SortPairsDescending(radix_storage, radix_storage_bytes, cExtractionCuda.egv_compact, cExtractionCuda.egv_sorted,
                                                 cExtractionCuda.startPoints_compact, cExtractionCuda.startPoints_sorted, numStartPoints));
    CHECK(cudaFree(radix_storage));
    if (cExtractionOpts.verbose > 1) CHECK(cudaDeviceSynchronize());
    if (cExtractionOpts.verbose > 1) tt.stop();
    if (cExtractionOpts.verbose > 1) std::cout << "\tRadix sort took " << tt.seconds() << " seconds." << std::endl;
    // copy sorted starting points to host
    if (cExtractionOpts.verbose > 1) tt.start();
    startPoints.resize(numStartPoints);
    CHECK(cudaMemcpy(startPoints.data(), cExtractionCuda.startPoints_sorted, numStartPoints*sizeof(int32_t), cudaMemcpyDeviceToHost));
    if (cExtractionOpts.verbose > 1) tt.stop();
    if (cExtractionOpts.verbose > 1) std::cout << "\tCopying results to host took " << tt.seconds() << " seconds." << std::endl;
}

void performSeparableConvolution(float *d_Input, float *d_filterRow, float *d_filterCol, float *result,
                                 c_extraction_cuda cExtractionCuda, int rowSize, int colSize, int pitch, int offset,
                                 int maxKernelSize) {
    convolutionRowsGPU(cExtractionCuda.gaussianBuffer, d_Input, cExtractionCuda.width, cExtractionCuda.height,
                       d_filterRow, rowSize, pitch, offset);
    convolutionColumnsGPU(result, cExtractionCuda.gaussianBuffer, cExtractionCuda.width, cExtractionCuda.height,
                          d_filterCol, colSize, pitch, offset + maxKernelSize);
}

void allocateCudaStructs(c_extraction_cuda &cExtractionCuda, c_extraction_opts &cExtractionOpts) {
    int width = cExtractionCuda.width;
    int height = cExtractionCuda.height;
    CHECK(cudaMalloc((void **)&(cExtractionCuda.rx),  width * height * sizeof(float)));
    CHECK(cudaMalloc((void **)&(cExtractionCuda.rxx),  width * height * sizeof(float)));
    CHECK(cudaMalloc((void **)&(cExtractionCuda.rxy),  width * height * sizeof(float)));
    CHECK(cudaMalloc((void **)&(cExtractionCuda.ry),  width * height * sizeof(float)));
    CHECK(cudaMalloc((void **)&(cExtractionCuda.ryy),  width * height * sizeof(float)));
    CHECK(cudaMalloc((void **)&(cExtractionCuda.gaussianBuffer),  width * height * sizeof(float)));
    CHECK(cudaMalloc((void **)&(cExtractionCuda.egv),  width * height * sizeof(float)));
    CHECK(cudaMalloc((void **)&(cExtractionCuda.whiskerAngles),  width * height * sizeof(float)));
    CHECK(cudaMalloc((void **)&(cExtractionCuda.d_preprocessedImage), width * height * sizeof(float)));
    int k = 0;
    int offset = cExtractionOpts.gaussianKernelSize;
    setConvolutionKernelInGlobalMem(cExtractionOpts.gxRow.ptr<float>(), offset-1, (k++)*offset);
    setConvolutionKernelInGlobalMem(cExtractionOpts.gxCol.ptr<float>(), offset, (k++)*offset);
    setConvolutionKernelInGlobalMem(cExtractionOpts.gxxRow.ptr<float>(), offset-2, (k++)*offset);
    setConvolutionKernelInGlobalMem(cExtractionOpts.gxxCol.ptr<float>(), offset, (k++)*offset);
    setConvolutionKernelInGlobalMem(cExtractionOpts.gxyRow.ptr<float>(), offset-1, (k++)*offset);
    setConvolutionKernelInGlobalMem(cExtractionOpts.gxyCol.ptr<float>(), offset-1, (k++)*offset);
    setConvolutionKernelInGlobalMem(cExtractionOpts.gyRow.ptr<float>(), offset, (k++)*offset);
    setConvolutionKernelInGlobalMem(cExtractionOpts.gyCol.ptr<float>(), offset-1, (k++)*offset);
    setConvolutionKernelInGlobalMem(cExtractionOpts.gyyRow.ptr<float>(), offset, (k++)*offset);
    setConvolutionKernelInGlobalMem(cExtractionOpts.gyyCol.ptr<float>(), offset-2, k*offset);
    // allocate necessary CUB structs
    // CUB Flag Allocations
    CHECK(cudaMalloc(&cExtractionCuda.egv_compact, cExtractionCuda.width*cExtractionCuda.height*sizeof(float)));
    CHECK(cudaMalloc(&cExtractionCuda.startPoints, cExtractionCuda.width*cExtractionCuda.height*sizeof(int32_t)));
    CHECK(cudaMalloc(&cExtractionCuda.startPoints_compact, cExtractionCuda.width*cExtractionCuda.height*sizeof(int32_t)));
    CHECK(cudaMalloc(&cExtractionCuda.flags_startPoints, cExtractionCuda.width*cExtractionCuda.height*sizeof(uint8_t)));
    CHECK(cudaMemset(cExtractionCuda.flags_startPoints, 0, cExtractionCuda.width*cExtractionCuda.height*sizeof(uint8_t)));
    CHECK(cudaMalloc(&cExtractionCuda.numStartPoints, sizeof(int)));
    // allocate bytes
    cExtractionCuda.flagged_storage = NULL;
    cExtractionCuda.flagged_storage_bytes = 0;
    CubDebugExit(cub::DeviceSelect::Flagged(cExtractionCuda.flagged_storage, cExtractionCuda.flagged_storage_bytes, (int32_t*) cExtractionCuda.startPoints,
                                            (uint8_t*) cExtractionCuda.flags_startPoints, (int32_t*) cExtractionCuda.startPoints_compact,
                                            cExtractionCuda.numStartPoints, cExtractionCuda.height * cExtractionCuda.width));
//    std::cout << "CUB allocated " << cExtractionCuda.flagged_storage_bytes << " bytes for FLAG operations." << std::endl;
    CHECK(cudaMalloc(&cExtractionCuda.flagged_storage, cExtractionCuda.flagged_storage_bytes));
    // CUB Radix Allocations
    CHECK(cudaMalloc(&cExtractionCuda.egv_sorted, cExtractionCuda.width*cExtractionCuda.height*sizeof(float)));
    CHECK(cudaMalloc(&cExtractionCuda.startPoints_sorted, cExtractionCuda.width*cExtractionCuda.height*sizeof(int32_t)));
    CHECK(cudaMallocHost(&cExtractionOpts.whiskerAngles, cExtractionCuda.width * cExtractionCuda.height * sizeof(float)));
    CHECK(cudaMallocHost(&cExtractionOpts.preprocessedImage, cExtractionCuda.width * cExtractionCuda.height * sizeof(float)));
}

void freeCudaStructs(c_extraction_cuda &cExtractionCuda, c_extraction_opts &cExtractionOpts) {
    CHECK(cudaFree(cExtractionCuda.rx));
    CHECK(cudaFree(cExtractionCuda.rxx));
    CHECK(cudaFree(cExtractionCuda.rxy));
    CHECK(cudaFree(cExtractionCuda.ry));
    CHECK(cudaFree(cExtractionCuda.ryy));
    CHECK(cudaFree(cExtractionCuda.gaussianBuffer));
    CHECK(cudaFree(cExtractionCuda.egv));
    CHECK(cudaFree(cExtractionCuda.whiskerAngles));
    CHECK(cudaFree(cExtractionCuda.d_preprocessedImage));
    // free CUB structs
    CHECK(cudaFree(cExtractionCuda.egv_compact));
    CHECK(cudaFree(cExtractionCuda.egv_sorted));
    CHECK(cudaFree(cExtractionCuda.startPoints));
    CHECK(cudaFree(cExtractionCuda.startPoints_compact));
    CHECK(cudaFree(cExtractionCuda.startPoints_sorted));
    CHECK(cudaFree(cExtractionCuda.flags_startPoints));
    CHECK(cudaFree(cExtractionCuda.flagged_storage));
    CHECK(cudaFree(cExtractionCuda.numStartPoints));
    CHECK(cudaFreeHost(cExtractionOpts.whiskerAngles));
    CHECK(cudaFreeHost(cExtractionOpts.preprocessedImage));
}

void transferResults(c_extraction_cuda cExtractionCuda, float *whiskerAngles, float *preprocessedImage) {
    CHECK(cudaMemcpy(whiskerAngles, cExtractionCuda.whiskerAngles, cExtractionCuda.width * cExtractionCuda.height * sizeof(float), cudaMemcpyDeviceToHost));
    CHECK(cudaMemcpy(preprocessedImage, cExtractionCuda.d_preprocessedImage, cExtractionCuda.width * cExtractionCuda.height * sizeof(float), cudaMemcpyDeviceToHost));
}

void convertCudaMatToArr(cv::cuda::GpuMat src, c_extraction_cuda &cExtractionCuda) {
    if (!src.isContinuous()) {
        int block_width = 32;
        dim3 gridDim(cExtractionCuda.width / block_width, cExtractionCuda.height / block_width, 1);
        dim3 blockDim(block_width, block_width, 1);
        matToArr<<<gridDim, blockDim>>>(src, cExtractionCuda.d_preprocessedImage, cExtractionCuda.width, cExtractionCuda.height);
    }
    else cExtractionCuda.d_preprocessedImage = src.ptr<float>();
}

void debugging(cv::cuda::GpuMat src, c_extraction_cuda &cExtractionCuda, plotting_opts plottingOpts) {
    cv::Mat preprocessedImage(src);
    std::cout << preprocessedImage << std::endl;
    std::cout << "preprocessedImage channels = " << src.channels() << std::endl;
    showImage(preprocessedImage, "preprocessedImage", 0, true, plottingOpts);
//    cv::Mat preprocessedArray(cExtractionCuda.height, cExtractionCuda.width, )

    // debugging
    float *cpu_arr = (float*) malloc(cExtractionCuda.width*cExtractionCuda.height*sizeof(float));
    cudaMemcpy(cpu_arr, cExtractionCuda.d_preprocessedImage, cExtractionCuda.height*cExtractionCuda.width*sizeof(float), cudaMemcpyDeviceToHost);
    cv::Mat cpuMat(cExtractionCuda.height, cExtractionCuda.width, CV_32FC1, cpu_arr);
    std::cout << cpuMat << std::endl;
    showImage(cpuMat, "preprocessedMat", 0 , true, plottingOpts);
}
