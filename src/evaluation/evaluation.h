/*
    This file is part of WhiskEras 2.0.

    WhiskEras 2.0 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WhiskEras 2.0 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WhiskEras 2.0.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef FWTS_C_EVALUATION_H
#define FWTS_C_EVALUATION_H

#include "../utils/plotting.h"
#include "../../libs/eigen/Eigen/Dense"
#include "../options/options.h"
#include "../stages/whisker_forming/whisker_forming_options.h"
#include "../cuda/preprocessing_cuda.h"
#include "../cuda/c_extraction_cuda.h"
#include "../utils/profile.h"

void writeDetectedWhiskersNr(std::vector<int> &whiskers_detected_Nr, int startFrame, int endFrame, int videoStart, std::string outputDir);

void writeDetectionRates(track_opts trackOpts, std::string outputDir);

void writeWhiskerAnglesToCSV(std::vector<std::vector<float>> &whisker_angles, int numTrackedWhiskers, int startFrame,
                             int endFrame, int videoStart, std::string outputPath);

void addAngles(std::vector<std::vector<float>> &whisker_angles, std::vector<whisker> &lineGatherer_tracker, std::vector<bool> &couldNotFit);

void clearAngles(std::vector<std::vector<float>> &whisker_angles, int numTrackedWhiskers);

void addWhiskers(std::vector<std::vector<whisker>> &whiskers, std::vector<whisker> &lineGatherer_tracker, std::vector<bool> &couldNotFit);

void writeWhiskersToCSV(std::vector<std::vector<whisker>> &whiskers, int numTrackedWhiskers, int startFrame,
                        int endFrame, int videoStart, std::string outputPath);

#endif //FWTS_C_EVALUATION_H
