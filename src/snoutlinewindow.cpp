/*
    This file is part of WhiskEras 2.0.

    WhiskEras 2.0 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WhiskEras 2.0 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WhiskEras 2.0.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "snoutlinewindow.h"
#include "ui_snoutlinewindow.h"
#include "mainwindow.h"
#include <QSettings>

SnoutLineWindow::SnoutLineWindow(cv::VideoCapture video, int startFrame, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SnoutLineWindow)
{
    ui->setupUi(this);

    // restore geometry to last session's
    QSettings settings("Erasmus MC Neuroscience Lab", "WhiskEras 2.0");
    this->restoreGeometry(settings.value("SnoutLineWindow/geometry").toByteArray());

    ui->ConfirmButtons->setVisible(false);
    ui->confirmLabel->setVisible(false);
    frame_nr = startFrame;
    whisker_video = video;
    showFrame();
    connect(ui->framePreview, SIGNAL(lineDrawn()), this, SLOT(on_LineDrawn()));
    connect(ui->framePreview, SIGNAL(lineCleared()), this, SLOT(on_LineCleared()));
    connect(this, SIGNAL(frameChanged()), ui->framePreview, SLOT(drawSnoutLine()));

    // configure frame indexing
    ui->frameSpinBox->setValue(startFrame+1);
    ui->frameSpinBox->setMaximum(video.get(cv::CAP_PROP_FRAME_COUNT)-1);
    ui->frameSlider->setMaximum(video.get(cv::CAP_PROP_FRAME_COUNT)-1);
    ui->firstFrame_bg_spinBox->setMaximum(video.get(cv::CAP_PROP_FRAME_COUNT)-1);
    ui->firstFrame_bg_slider->setMaximum(video.get(cv::CAP_PROP_FRAME_COUNT)-1);
    ui->lastFrame_bg_spinBox->setMaximum(video.get(cv::CAP_PROP_FRAME_COUNT)-1);
    ui->lastFrame_bg_slider->setMaximum(video.get(cv::CAP_PROP_FRAME_COUNT)-1);
    ui->lastFrame_bg_slider->setValue(video.get(cv::CAP_PROP_FRAME_COUNT)-1);
}

SnoutLineWindow::~SnoutLineWindow()
{
    delete ui;
}

void SnoutLineWindow::resizeEvent(QResizeEvent *event)
{
    QSettings settings("Erasmus MC Neuroscience Lab", "WhiskEras 2.0");
    settings.setValue("SnoutLineWindow/geometry", saveGeometry());
}

void SnoutLineWindow::closeEvent(QCloseEvent *event)
{
    QSettings settings("Erasmus MC Neuroscience Lab", "WhiskEras 2.0");
    settings.setValue("SnoutLineWindow/geometry", saveGeometry());
    QDialog::closeEvent(event);
}

void SnoutLineWindow::showFrame()
{
    // get frame
    cv::Mat frame, dest;
    whisker_video.set(cv::CAP_PROP_POS_FRAMES, frame_nr);
    whisker_video >> frame;
    cv::cvtColor(frame, dest, cv::COLOR_BGR2RGB);
    QImage image = QImage((uchar*) dest.data, dest.cols, dest.rows, dest.step, QImage::Format_RGB888);
    QPixmap pixmap = QPixmap::fromImage(image);
    ui->framePreview->frame_pixmap = pixmap;

    ui->framePreview->setBackgroundRole(QPalette::Base);
    ui->framePreview->setPixmap(pixmap);
    ui->framePreview->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    QSize image_size(image.width(), image.height());
}

void SnoutLineWindow::on_ConfirmButtons_accepted()
{
    tip[0] = ui->framePreview->tip[0];
    tip[1] = ui->framePreview->tip[1];
    middle[0] = ui->framePreview->middle[0];
    middle[1] = ui->framePreview->middle[1];
    this->accept();
}

void SnoutLineWindow::on_ConfirmButtons_rejected()
{
    ui->framePreview->clearSnoutLine();
}

void SnoutLineWindow::on_LineDrawn()
{
    ui->ConfirmButtons->setVisible(true);
    ui->confirmLabel->setVisible(true);
    ui->instructionsLabel->setVisible(false);
}

void SnoutLineWindow::on_LineCleared()
{
    ui->ConfirmButtons->setVisible(false);
    ui->confirmLabel->setVisible(false);
    ui->instructionsLabel->setVisible(true);
}

void SnoutLineWindow::on_frameSlider_valueChanged(int value)
{
    frame_nr = value;
    showFrame();
    emit frameChanged();
}

void SnoutLineWindow::on_firstFrame_bg_slider_valueChanged(int value)
{
    if (value >= endFrame - 60) {
        ui->firstFrame_bg_slider->setValue(endFrame - 61);
    }
    else {
        startFrame = value;
        ui->firstFrame_bg_spinBox->setValue(value);
        emit bgFramesChanged(startFrame, endFrame);
    }
}

void SnoutLineWindow::on_firstFrame_bg_spinBox_valueChanged(int arg1)
{
    ui->firstFrame_bg_slider->setValue(arg1);
}

void SnoutLineWindow::on_lastFrame_bg_slider_valueChanged(int value)
{
    if (value <= startFrame + 60) {
        ui->lastFrame_bg_slider->setValue(startFrame + 61);
    }
    else {
        endFrame = value;
        ui->lastFrame_bg_spinBox->setValue(value);
        emit bgFramesChanged(startFrame, endFrame);
    }
}

void SnoutLineWindow::on_lastFrame_bg_spinBox_valueChanged(int arg1)
{
    ui->lastFrame_bg_slider->setValue(arg1);
}

void SnoutLineWindow::on_whiskersUpside_checkBox_clicked(bool checked)
{
    upside_whiskers = checked;
}

void SnoutLineWindow::on_InvertColors_checkBox_clicked(bool checked)
{
    invertColors = checked;
}
