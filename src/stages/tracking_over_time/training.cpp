/*
    This file is part of WhiskEras 2.0.

    WhiskEras 2.0 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WhiskEras 2.0 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WhiskEras 2.0.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "training.h"
#include "tracking.h"
#include "recognition.h"
#include "../../utils/error.h"
#include "../../utils/Timer.hpp"

void training(const std::vector<whisker> &lineGatherer_tracker, std::vector<int> order, std::vector<classSVMData> &svmFeatures,
              int currFrame, int startFrame, track_opts &trackOpts, fwptd_opts fwptdOpts,
              train_opts &trainOpts, recog_opts &recogOpts, plotting_opts plottingOpts) {
    whisker currentMean = whiskerAverages(lineGatherer_tracker, order, false);

    // configure whisker database (add new detected whiskers' features)
    configureWhiskerDB(lineGatherer_tracker, trackOpts, fwptdOpts, trainOpts);
    trackOpts.prevMean = currentMean;
    trainOpts.newDataSize++; // increment size of new data to train in next training

    // train
    if (((currFrame - startFrame > trainOpts.bootstrap) &&
    !(currFrame % std::min(trainOpts.retrainFreqInit, trainOpts.retrainAtLeast))) ||
    currFrame - startFrame == trainOpts.bootstrap) {
        Timer prepareTimer;
        prepareTimer.start();
        prepareTrainingData(svmFeatures, trackOpts.whiskerDb, trainOpts, trackOpts, true);
        prepareTimer.stop();
        if (trainOpts.verbose) std::cout << "Preparing training data took " << prepareTimer.seconds() << std::endl;
        // libLinear one-vs-one multiclass
        trainOnevsOneLinear(svmFeatures, lineGatherer_tracker, trackOpts, trainOpts, recogOpts);
        // reset size of new data to train in next training
        trainOpts.newDataSize = 0;
    }

    // decrease retrain frequency
    if (!(currFrame % trainOpts.increaseEvery) && (currFrame != 0)) trainOpts.retrainFreqInit += trainOpts.increaseWith;
}

void configureWhiskerDB(const std::vector<whisker> &lineGatherer_tracker, track_opts &trackOpts, fwptd_opts fwptdOpts,
                        const train_opts &trainOpts) {
    for (size_t i = 0; i < lineGatherer_tracker.size(); i++) {
        double sinx2_p, cosx2_p, sinx2_a, cosx2_a;
        sincos(lineGatherer_tracker[i].angle, &sinx2_p, &cosx2_p);
        float cotx2_p = cosx2_p / sinx2_p;
        sincos(trackOpts.prevAverages.angle, &sinx2_a, &cosx2_a);
        float cotx2_a = cosx2_a / sinx2_a;
        whiskerDB newWhiskerDB = {lineGatherer_tracker[i].position, lineGatherer_tracker[i].angle, cotx2_p, trackOpts.prevAverages.position, cotx2_a,
                                  lineGatherer_tracker[i].length, lineGatherer_tracker[i].shape, trackOpts.couldNotFit[i]};
        trackOpts.whiskerDb[i].push_back(newWhiskerDB);

        uint whiskerDbSize = trackOpts.whiskerDb[i].size();

        // if the data exceeds the window, delete the oldest data.
        if (whiskerDbSize > trainOpts.window) {
            trackOpts.whiskerDb[i].erase(trackOpts.whiskerDb[i].begin());
        }

        whiskerDbSize = trackOpts.whiskerDb[i].size();
        // compute new mean and variance of the whiskers
        if (!trackOpts.whiskerDb[i][whiskerDbSize-1].cnf) {
            trackOpts.fittedInstances++;
            computeNewMeanSTD(trackOpts.whiskerDb[i][whiskerDbSize-1].x1_p, trackOpts.featuresMean.x1_p, trackOpts.featuresSTD.x1_p, trackOpts.fittedInstances);
            computeNewMeanSTD(trackOpts.whiskerDb[i][whiskerDbSize-1].cotx2_p, trackOpts.featuresMean.cotx2_p, trackOpts.featuresSTD.cotx2_p, trackOpts.fittedInstances);
            computeNewMeanSTD(trackOpts.whiskerDb[i][whiskerDbSize-1].x1_a, trackOpts.featuresMean.x1_a, trackOpts.featuresSTD.x1_a, trackOpts.fittedInstances);
            computeNewMeanSTD(trackOpts.whiskerDb[i][whiskerDbSize-1].cotx2_a, trackOpts.featuresMean.cotx2_a, trackOpts.featuresSTD.cotx2_a, trackOpts.fittedInstances);
            computeNewMeanSTD(trackOpts.whiskerDb[i][whiskerDbSize-1].L, trackOpts.featuresMean.L, trackOpts.featuresSTD.L, trackOpts.fittedInstances);
            computeNewMeanSTD(trackOpts.whiskerDb[i][whiskerDbSize-1].x3, trackOpts.featuresMean.x3, trackOpts.featuresSTD.x3, trackOpts.fittedInstances);
            // initialize STD
            if (trackOpts.fittedInstances == 2) {
                int k = i;
                while (trackOpts.whiskerDb[k][0].cnf) {
                    k--;
                }
                xassert(k >= 0, "Error! No more than 1 whisker was detected in the first frame. Cannot compute standard deviation! Exiting.");
                trackOpts.featuresSTD.x1_p = sqrtf(powf(trackOpts.whiskerDb[i][0].x1_p-trackOpts.featuresMean.x1_p, 2)
                        + powf(trackOpts.whiskerDb[i][whiskerDbSize-1].x1_p-trackOpts.featuresMean.x1_p, 2));
                trackOpts.featuresSTD.cotx2_p = sqrtf(powf(trackOpts.whiskerDb[i][0].cotx2_p-trackOpts.featuresMean.cotx2_p, 2)
                                                      + powf(trackOpts.whiskerDb[i][whiskerDbSize-1].cotx2_p-trackOpts.featuresMean.cotx2_p, 2));
                trackOpts.featuresSTD.x1_a = sqrtf(powf(trackOpts.whiskerDb[i][0].x1_a-trackOpts.featuresMean.x1_a, 2)
                                                      + powf(trackOpts.whiskerDb[i][whiskerDbSize-1].x1_a-trackOpts.featuresMean.x1_a, 2));
                trackOpts.featuresSTD.cotx2_a = sqrtf(powf(trackOpts.whiskerDb[i][0].cotx2_a-trackOpts.featuresMean.cotx2_a, 2)
                                                      + powf(trackOpts.whiskerDb[i][whiskerDbSize-1].cotx2_a-trackOpts.featuresMean.cotx2_a, 2));
                trackOpts.featuresSTD.L = sqrtf(powf(trackOpts.whiskerDb[i][0].L-trackOpts.featuresMean.L, 2)
                                                      + powf(trackOpts.whiskerDb[i][whiskerDbSize-1].L-trackOpts.featuresMean.L, 2));
                trackOpts.featuresSTD.x3 = sqrtf(powf(trackOpts.whiskerDb[i][0].x3-trackOpts.featuresMean.x3, 2)
                                                      + powf(trackOpts.whiskerDb[i][whiskerDbSize-1].x3-trackOpts.featuresMean.x3, 2));
            }
        }
    }
}

void computeNewMeanSTD(float new_value, float &mean, float &STD, int numValues) {
    float oldMean = mean;
    computeNewMean(mean, new_value, numValues);
    if (numValues > 2) {
        computeNewSTD(STD, new_value, oldMean, mean, numValues);
    }
}

void computeNewMean(float &mean, float new_value, int &numValues) {
    mean *= (numValues-1);
    mean += new_value;
    mean = mean / numValues;
}

void computeNewSTD(float &STD, float new_value, float oldMean, float newMean, int numValues) {
    float oldVar = STD*STD; // old variance
    float newVar = ((numValues - 2) * oldVar + (new_value - newMean) * (new_value - oldMean)) / (numValues - 1);
    STD = sqrtf(newVar); // STD is the root of the variance
}

void prepareTrainingData(std::vector<classSVMData> &svmFeatures, const std::vector<std::vector<whiskerDB>> &whiskerDb,
                         train_opts &trainOpts, const track_opts &trackOpts, bool isTrain) {
    size_t total_data = whiskerDb[0].size();
    // if the amount of new data exceeds the window, only add the latest <window> instances
    if (isTrain && trainOpts.newDataSize > trainOpts.maxSizeTrainingData) trainOpts.newDataSize = trainOpts.maxSizeTrainingData;
    // loop through all whiskers
    for (size_t i = 0; i < whiskerDb.size(); i++) {
        size_t k = svmFeatures[i].size;
        // loop through all whiskerDB data
        for (int j = total_data - 1; j >= 0 && (!isTrain || j >= (int) (total_data - trainOpts.newDataSize)); j--) {
            if (isTrain && k >= trainOpts.maxSizeTrainingData) k = 0; // reset k to ovewrite old data when max data size is exceeded.
            insertDataInstance(k, svmFeatures[i], trainOpts.maxSizeTrainingData, whiskerDb[i][j], trackOpts.featuresMean, trackOpts.featuresSTD);
        }
    }
}

void insertDataInstance(size_t &k, classSVMData &svmFeatures, size_t window, const whiskerDB &whiskerDb,
                        const featuresVec featuresMean, const featuresVec featuresSTD) {
    // if the whisker could fit
    if (!whiskerDb.cnf) {
        // WITH STANDARDIZATION
        svmFeatures.features[k].x1_p = (whiskerDb.x1_p - featuresMean.x1_p) / featuresSTD.x1_p;
        svmFeatures.features[k].x1_a = (whiskerDb.x1_a - featuresMean.x1_a) / featuresSTD.x1_a;
        svmFeatures.features[k].cotx2_p = (whiskerDb.cotx2_p - featuresMean.cotx2_p) / featuresSTD.cotx2_p;
        svmFeatures.features[k].cotx2_a = (whiskerDb.cotx2_a - featuresMean.cotx2_a) / featuresSTD.cotx2_a;
        svmFeatures.features[k].x3 = (whiskerDb.x3 - featuresMean.x3) / featuresSTD.x3;
        svmFeatures.features[k].L = (whiskerDb.L - featuresMean.L) / featuresSTD.L;
        if (svmFeatures.size < window) svmFeatures.size++;
        k++;
    }
}

void allocateSVMFeatures(std::vector<classSVMData> &svmFeatures, int window) {
    for (size_t i = 0; i < svmFeatures.size(); i++) {
        // allocate enough space for window number of instances
        svmFeatures[i].features.resize(window);
    }
}

void defineOneVsOneLinear(recog_opts &recogOpts, feature_node *feature_space, int *numExamples, classSVMData svmFeatures1,
                          classSVMData svmFeatures2, int problemIdx, int chanceToTrain, bool copy1, bool copy2, int numFeatures) {
    int class1 = recogOpts.ks[problemIdx];
    int class2 = recogOpts.js[problemIdx];
    int j = 0;
    int k = 0;
    // store training data from first class
    if (!copy1) {
        storeLinearFeatures(recogOpts.linearProblem[problemIdx], feature_space, svmFeatures1, class1, j, k, chanceToTrain);
        numExamples[class1] = k;
    }
    else {
        copyLinearFeatures(recogOpts.linearProblem[problemIdx], recogOpts.linearProblem[class1 - 1], numExamples[class1], numFeatures, 0, 0);
    }
    // store training data from second class
    if (!copy2) {
        storeLinearFeatures(recogOpts.linearProblem[problemIdx], feature_space, svmFeatures2, class2, j, k, chanceToTrain);
        numExamples[class2] = k - numExamples[class1];
    }
    else {
        int problemToCopy = (class2 > 0) ? class2 - 1 : 0;
        int src_offset = (class2 > 0) ? 0 : numExamples[1]; // for class 0, the data is stored after class 1's data
        copyLinearFeatures(recogOpts.linearProblem[problemIdx], recogOpts.linearProblem[problemToCopy], numExamples[class2], numFeatures, numExamples[class1], src_offset);
    }
    recogOpts.linearProblem[problemIdx].l = numExamples[class1] + numExamples[class2]; // total number of training examples
}


void storeLinearFeatures(problem &linearProblem, feature_node *feature_space, classSVMData svmFeatures, int cluster,
                   int &j, int &k, int chanceToTrain) {
    // store training data from first class
    for (size_t i = 0; i < svmFeatures.size; i++) {
        if (rand() % 100 >= chanceToTrain) continue; // take about <chanceToTrain> % of inputs
        linearProblem.y[k] = cluster;
        linearProblem.x[k] = &feature_space[j];

        // store features
        feature_space[j].index = 1;
        feature_space[j++].value = svmFeatures.features[i].x1_p;
        feature_space[j].index = 2;
        feature_space[j++].value = svmFeatures.features[i].cotx2_p;
        feature_space[j].index = 3;
        feature_space[j++].value = svmFeatures.features[i].x1_a;
        feature_space[j].index = 4;
        feature_space[j++].value = svmFeatures.features[i].cotx2_a;
        feature_space[j].index = 5;
        feature_space[j++].value = svmFeatures.features[i].L;
        feature_space[j].index = 6;
        feature_space[j++].value = svmFeatures.features[i].x3;
        feature_space[j++].index = -1;
        k++;
    }
}

void copyLinearFeatures(problem &linearProblem, /*feature_node *feature_space,*/ problem &linearProblemToCopy, int numInstances, int numFeatures,
                        int dst_offset, int src_offset) {
    memcpy(&linearProblem.y[dst_offset], &linearProblemToCopy.y[src_offset], numInstances*sizeof(double));
    memcpy(&linearProblem.x[dst_offset], &linearProblemToCopy.x[src_offset], numInstances*sizeof(struct feature_node*));
}

void trainOnevsOneLinear(const std::vector<classSVMData> svmFeatures, const std::vector<whisker> &lineGatherer_tracker,
                           track_opts &trackOpts, const train_opts &trainOpts, recog_opts &recogOpts) {
    Timer defTimer, trainTimer;
    defTimer.start();
    for (size_t i = 0; i < recogOpts.ks.size(); i++) {
        // first pair (1-vs-0) is sequentially loaded into the linear problem
        if (i == 0) {
            defineOneVsOneLinear(recogOpts, recogOpts.feature_space[i], recogOpts.numExamples, svmFeatures[recogOpts.ks[i]],
                                 svmFeatures[recogOpts.js[i]], i, trainOpts.chance_train, false, false, trainOpts.numFeatures);
        }
        // the rest (X-vs-0) pairs load the X class's data and memcopy the 0 class's data into the linear problem
        else if (i < lineGatherer_tracker.size() - 1) {
            defineOneVsOneLinear(recogOpts, recogOpts.feature_space[i], recogOpts.numExamples, svmFeatures[recogOpts.ks[i]],
                                 svmFeatures[recogOpts.js[i]], i, trainOpts.chance_train, false, true, trainOpts.numFeatures);
        }
        // all other pairs are directly memcopied from the already stored ones
        else {
            defineOneVsOneLinear(recogOpts, recogOpts.feature_space[i], recogOpts.numExamples, svmFeatures[recogOpts.ks[i]],
                                 svmFeatures[recogOpts.js[i]], i, trainOpts.chance_train, true, true, trainOpts.numFeatures);
        }
    }
    defTimer.stop();
    if (trainOpts.verbose > 1) std::cout << "Defining took " << defTimer.seconds() << " seconds." << std::endl;
    trainTimer.start();
#ifdef USE_OPENMP
#pragma omp parallel for // schedule(dynamic)
#endif
    for (size_t i = 0; i < recogOpts.ks.size(); i++) {
        recogOpts.linearModel[i] = *train(&recogOpts.linearProblem[i], &trainOpts.linear_param);
    }
    trainTimer.stop();
    if (trainOpts.verbose) std::cout << "Training took " << trainTimer.seconds() << " seconds." << std::endl;
//    train_accuracy_linearOneVsOne(svmFeatures, recogOpts);
}

void allocateLinearProblemOnevsOne(int window, int numOfWhiskers, int numOfFeatures, recog_opts &recogOpts) {
    recogOpts.linearProblem = (problem*) malloc(recogOpts.ks.size()*sizeof(problem));
    // 6 features + (-1) index signaling the end of an instance
    recogOpts.feature_space = (feature_node**) malloc(recogOpts.ks.size()*sizeof(feature_node*));
    for (size_t i = 0; i < recogOpts.ks.size(); i++) {
        recogOpts.linearProblem[i].bias = recogOpts.linearBias;
        // allocate space to store training data from two whiskers (one-vs-one), each with <window> number of instances
        recogOpts.linearProblem[i].y = (double *) malloc(2 * window * sizeof(double));
        recogOpts.linearProblem[i].x = (feature_node **) malloc(2 * window * sizeof(feature_node *));
        recogOpts.linearProblem[i].n = numOfFeatures;
        recogOpts.feature_space[i] = (feature_node*) malloc(2*window*(numOfFeatures+1)*sizeof(feature_node));
    }
    recogOpts.linearModel = (model*) malloc(recogOpts.ks.size()*sizeof(model));
    recogOpts.numExamples = (int*) malloc(numOfWhiskers*sizeof(int));
}

void freeLinearProblem(std::vector<classSVMData> &svmFeatures, track_opts &trackOpts, recog_opts &recogOpts) {
    for (size_t i = 0; i < recogOpts.ks.size(); i++) {
        free(recogOpts.linearProblem[i].y);
        free(recogOpts.linearProblem[i].x);
        free(recogOpts.feature_space[i]);
    }
    free(recogOpts.linearProblem);
    free(recogOpts.feature_space);
    free(recogOpts.linearModel);
    freeData(svmFeatures, trackOpts);
    free(recogOpts.numExamples);
}

void freeData(std::vector<classSVMData> &svmFeatures, track_opts &trackOpts) {
    for (size_t i = 0; i < svmFeatures.size(); i++) {
        // allocate enough space for window number of instances
        svmFeatures[i].features.clear();
    }
    svmFeatures.clear();
    for (size_t i = 0; i < trackOpts.whiskerDb.size(); i++) {
        trackOpts.whiskerDb[i].clear();
    }
    trackOpts.whiskerDb.clear();
    trackOpts.couldNotFit.clear();
    trackOpts.couldNotTrack.clear();
}

void configLinearEps(parameter &param) {
    if(param.eps == HUGE_VAL)
    {
        switch(param.solver_type)
        {
            case L2R_LR:
            case L2R_L2LOSS_SVC:
                param.eps = 0.01;
                break;
            case L2R_L2LOSS_SVR:
                param.eps = 0.0001;
                break;
            case L2R_L2LOSS_SVC_DUAL:
            case L2R_L1LOSS_SVC_DUAL:
            case MCSVM_CS:
            case L2R_LR_DUAL:
                param.eps = 0.1;
                break;
            case L1R_L2LOSS_SVC:
            case L1R_LR:
                param.eps = 0.01;
                break;
            case L2R_L1LOSS_SVR_DUAL:
            case L2R_L2LOSS_SVR_DUAL:
                param.eps = 0.1;
                break;
            case ONECLASS_SVM:
                param.eps = 0.01;
                break;
        }
    }
}

void print_array(const std::vector<float> whisker_value) {
    for (size_t i = 0; i < whisker_value.size(); i++) {
        std::cout << whisker_value[i] << "\t";
    }
    std::cout << std::endl;
}

void print_whiskerDB(track_opts trackOpts) {
    // for all whiskers
    for (size_t i = 0; i < trackOpts.whiskerDb.size(); i++) {
        std::cout << "whisker " << i << std::endl;
        // for each whisker instance
        for (size_t j = 0; j < trackOpts.whiskerDb[i].size(); j++) {
            std::cout << "instance " << j << std::endl;
            std::cout << "x1_p = " << trackOpts.whiskerDb[i][j].x1_p << std::endl;
            std::cout << "cotx2_p = " << trackOpts.whiskerDb[i][j].cotx2_p << std::endl;
            std::cout << "x1_a = " << trackOpts.whiskerDb[i][j].x1_a << std::endl;
            std::cout << "cotx2_a = " << trackOpts.whiskerDb[i][j].cotx2_a << std::endl;
            std::cout << "L = " << trackOpts.whiskerDb[i][j].L << std::endl;
            std::cout << "x3 = " << trackOpts.whiskerDb[i][j].x3 << std::endl;
            std::cout << "cnf = " << trackOpts.whiskerDb[i][j].cnf << std::endl;
        }
    }
}

void print_null(const char *s) {}

void train_accuracy_linear(std::vector<classSVMData> &svmFeatures, recog_opts recogOpts) {
    float accuracy = 0;
    float examples = 0;
    feature_node *x = (feature_node *) malloc(7 * sizeof(struct feature_node)); // allocate memory for the prediction
    for (size_t i = 0; i < svmFeatures.size(); i++) {
        for (size_t j = 0; j < svmFeatures[i].size; j++) {
            storeLinearTest(svmFeatures[i], x, j);
            int prediction = predict(recogOpts.linearModel, x);
//                    std::cout << "prediction = " << prediction << std::endl;
            accuracy += ((size_t) prediction == i) ? 1 : 0;
            examples += 1;
        }
    }
    std::cout << "accuracy = " << (accuracy / examples) * 100 << "%"<< std::endl;
}

void train_accuracy_linearOneVsOne(const std::vector<classSVMData> &svmFeatures, recog_opts recogOpts) {
    float accuracy = 0;
    float examples = 0;
    feature_node *x = (feature_node *) malloc(7 * sizeof(struct feature_node)); // allocate memory for the prediction
    for (size_t i = 0; i < svmFeatures.size(); i++) {
        for (size_t j = 0; j < svmFeatures[i].size; j++) {
            storeLinearTest(svmFeatures[i], x, j);
            int prediction;
            predictOneVsOneLinear(x, prediction, recogOpts, svmFeatures.size());
//            int prediction = predict(recogOpts.linearModel, x);
//            std::cout << "prediction = " << prediction << std::endl;
            accuracy += ((size_t) prediction == i) ? 1 : 0;
            examples += 1;
        }
    }
    std::cout << "accuracy = " << (accuracy / examples) * 100 << "%"<< std::endl;
}
