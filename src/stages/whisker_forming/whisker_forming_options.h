/*
    This file is part of WhiskEras 2.0.

    WhiskEras 2.0 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WhiskEras 2.0 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WhiskEras 2.0.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef FWTS_C_WHISKER_FORMING_OPTIONS_H
#define FWTS_C_WHISKER_FORMING_OPTIONS_H

#include <opencv2/opencv.hpp>

struct clustering_opts {
    // image dimensions
    int width;
    int height;
    // these values are set according to the position of the snout
    float origin[2];
    bool upside_whiskers = false;
    float a_value;
    float b_value;
    float sbase[2][2];
    int cutoff = 150;   // distance from the snout that whiskers are detected (prevents detection of fur)
    float steger_c = 1; // weight of the angle when detecting neighbours.
    float maxAngle = 20; // [deg] max angle difference for two neighbouring points
    // Improved curve tracing
    // Steger Linking
    int L = 3; // radius (in pixels) of looking for neighbours
    // Curve Following
    int M = 15; // number of points to take average angle from
    int K = 15; // number of pixels to look further ahead for re-emergence of curve
    int minClusterSizeToExtend = 10; // minimum number of points to look for reemergence

    bool plot = false;
    bool plotPointsOnly = false;
    int verbose = 1;
};

struct stitch_I_opts {
    // As there is more distance between two clusters, the chance of an offset is greater (due to bending).
    // Therefore, the maximum offset is modelled as a cone that becomes thinner with greater distance.
    float coneMin = 7 * CV_PI / 180; // [rads] the cone value is modelled as a linear function
    float coneMax = 20 * CV_PI / 180; // [rads] of distance d between clusters with a max and a min value
    float coneMinDist = 25; // distance at which cone gets its minimum value.
    float c = 1000; // Similar to <steger_c>, there's a weight for angle [rad] difference,
    // for each potential stitching, a linear Radon Transform between the two whiskers is done to rule out erroneous stitchings
    int radonLength = 20; // length of intermediate parts whose intensity is checked
    float radonThreshold = 30; // threshold for intensity per point to accept stitching
    float maxAD = 20 * CV_PI / 180; // [rads] max angle difference between tip and bottom.
    int lengthOfTipAndBottom = 15; // Length of tip and bottom in px.
    // Minimum cluster length (in pixels) after local clustering. Clusters shorter than this value will be regarded as noise.
    int minClusterSize = 10;
    bool plot = false;
    bool separate_plot = false;
    int verbose = 1;
};

struct stitch_II_opts {
    // If a whisker's lowest point is higher than <highestRoot>, try to extend it towards the snout.
    float highestRoot = 60; // highest acceptable Y-distance from snout
    // The procedure works by 'inoculating' a whisker onto the whisker it has been hiding behind. These two variables
    // indicate what is an acceptable distance between the lowest point of the whisker and the whisker it was hiding behind.
    int acceptableXdistance = 50;
    int lengthOfTipAndBottom = 20;
    int minClusterSize = 40;
    float cone = 20 * CV_PI / 180; // [rads] only accept inoculation points in a cone
    float maxAD = 20 * CV_PI / 180; // max angle difference between tip and bottom, where tip ends at inoculation point
    int verbose = 0;
    bool plot = false;
    bool separate_plot = false;
};

// abstract description of whiskers
struct whisker {
    // draw a line along the snout and measure
    float position; // position on the line
    float angle; // angle relative to the line
    float shape; // shape of the line
    float length; // length of the whisker
    float x_max;
    float shiftDown;
};

#endif //FWTS_C_WHISKER_FORMING_OPTIONS_H
