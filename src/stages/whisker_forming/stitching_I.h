/*
    This file is part of WhiskEras 2.0.

    WhiskEras 2.0 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WhiskEras 2.0 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WhiskEras 2.0.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef FWTS_C_STITCHING_I_H
#define FWTS_C_STITCHING_I_H

#include "../../../libs/eigen/Eigen/Dense"
#include <opencv2/opencv.hpp>
#include "../../utils/plotting.h"

// stitch clusters together

struct cluster_tips {
    Eigen::Vector2f endPoint;
    Eigen::Vector2f beginPoint;
    float length;
    Eigen::MatrixXf R_general;
    Eigen::MatrixXf R_tip;
    Eigen::MatrixXf R_bottom;
    float angle_general;
    float angle_tip;
    float angle_bottom;
    bool isEmpty_tip = false;
    bool isEmpty_bottom = false;
};

enum pointsSelection {
    general = 0, tip = 1, bottom = 2, edges = 3
};

template <typename T>
int stitching_I(std::vector<int> &clusters, std::vector<float> &rotatedPositions, std::vector<int> &clusterSize,
                Eigen::MatrixXf *dots, std::vector<int> &clusterPointsID, T *preprocessedImage,
                stitch_I_opts stitchIOpts, plotting_opts plottingOpts);

int filterNoisyClusters(std::vector<int> &clusters, std::vector<float> &rotatedPositions, std::vector<int> clusterSize,
                        int *maxPointsOfCluster, int minClusterSize);

void getRotationData(cluster_tips *whisker_clusters, Eigen::MatrixXf *dots, const std::vector<bool> increasedClusters,
                     const std::vector<int> clusterSize, const std::vector<bool> removedClusters, int numOfClusters,
                     stitch_I_opts stitchIOpts);

template <typename T>
void getStitchingScores(Eigen::MatrixXf &scores, const cluster_tips *whisker_clusters, const Eigen::MatrixXf *dots,
                        const std::vector<bool> removedClusters, int numOfClusters, T *preprocessedImage,
                        stitch_I_opts stitchIOpts, plotting_opts plottingOpts);
/**
 *
 * @brief returns rotation data in order to linearize the tip of a cluster
 */
cluster_tips rotationData(const Eigen::MatrixXf dots, int cluster_size, int lengthOfTipAndBottom, pointsSelection whiskerPart);

template <typename T>
T min_array(T *arr, int size);

template <typename T>
void matmult(T *out, T *in1, T *in2, int m, int n, int l);

float mean(int *arr, int n);

float covariance(int *arr1, int *arr2, int n);

Eigen::MatrixXf Covariance(Eigen::MatrixXf input, int size);

Eigen::MatrixXf RotationMatrix(Eigen::MatrixXf covMat);

void AlignToTipAndBottom(cluster_tips &tips, const Eigen::MatrixXf R_init, const Eigen::MatrixXf dots,
                         Eigen::MatrixXf covMat, int cluster_size, int lengthOfTipAndBottom, pointsSelection whiskerPart);

void mergeClusters(std::vector<int> &clusters, Eigen::MatrixXf scores, std::vector<int> &clusterSize,
                   Eigen::MatrixXf *clusterPoints, std::vector<int> &clusterPointsID, int *numOfClusters,
                   int *maxPointsOfCluster, std::vector<bool> &removedClusters, std::vector<bool> &increasedClusters);

void removeClusters(std::vector<int> &clusters, std::vector<float> &rotatedPositions, std::vector<int> &clusterSize,
                    std::vector<bool> clustersToRemove, int *numOfClusters);

template <typename T> bool radonTransformCheck(const cluster_tips *whisker_clusters, const Eigen::MatrixXf *dots, int q, int r,
        T *preprocessedImage, stitch_I_opts stitchIOpts, plotting_opts plottingOpts);

inline bool sortinrev_first(const std::pair<int, int> &a, const std::pair<int, int> &b);

inline bool sortinrev_second(const std::pair<int,int> &a, const std::pair<int,int> &b);

#endif //FWTS_C_STITCHING_I_H
