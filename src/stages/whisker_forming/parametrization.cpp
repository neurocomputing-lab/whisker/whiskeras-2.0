/*
    This file is part of WhiskEras 2.0.

    WhiskEras 2.0 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WhiskEras 2.0 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WhiskEras 2.0.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "parametrization.h"
#include "../../utils/error.h"
#include <math.h>

#ifdef DOUBLE_PRECISION
#define sqrteps 1.4901e-08
#define eps 2.2204e-16
#else
#define sqrteps 3.4527e-04
#define eps 1.1921e-07
#endif

int clusters2whiskers(std::vector<struct whisker> &parametrizations, std::vector<int> &clusters, std::vector<float> &dots,
                          param_opts &paramOpts, plotting_opts plottingOpts) {

    int maxPointsOfCluster, numOfPoints;
    std::vector<int> clusterSize(clusters.size(), 0);
    // If there are too few points, parametrization will be unreliable, therefore discard clusters with few points as noise
    int numOfClusters = filterNoisyClusters(clusters, dots, clusterSize,
                                            &maxPointsOfCluster, paramOpts.minNrOfDots);

    numOfPoints = clusters.size();

    // each entry of the clusterPoints array contains the points of each cluster
    Eigen::MatrixXf clusterPoints[numOfClusters];
    for (int i = 0; i < numOfClusters; i++) {
        clusterPoints[i].resize(2, maxPointsOfCluster);
    }
    clusterSize.resize(numOfClusters);
    std::fill(clusterSize.begin(), clusterSize.end(), 0);
    for (int i = 0; i < numOfPoints; i++) {
        clusterPoints[clusters[i]](0, clusterSize[clusters[i]]) = dots[2 * i];
        clusterPoints[clusters[i]](1, clusterSize[clusters[i]]) = dots[2 * i + 1];
        clusterSize[clusters[i]]++;
    }

    parametrizations.resize(numOfClusters);
    std::vector<bool> valid_clusters(numOfClusters, true);


    // Find parametrization for every cluster
    fitWhiskers(clusterPoints, clusterSize, paramOpts, parametrizations, valid_clusters, numOfClusters);

    // skip removed parametrizations
    int skipped_clusters = 0;
    for (int i = 1; i < numOfClusters; i++) {
//        xassert(valid_clusters[i-1], "woo");
        if (!valid_clusters[i-1]) skipped_clusters++;
        parametrizations[i-skipped_clusters] = parametrizations[i];
    }
    // It is possible that some clusters couldn't be parametrized. Those clusters are discarded
    numOfClusters = filterNoisyClusters(clusters, dots, clusterSize, &maxPointsOfCluster, 0);
    parametrizations.resize(numOfClusters);

    if (paramOpts.verbose > 0) std::cout << "\tNumber of whiskers is " << numOfClusters << std::endl;
    paramOpts.numDetectedWhiskers = numOfClusters;

    // uncomment this to compare plotting to respective clusters
//    if (paramOpts.plot) rotateAndPlotClusters(clusters, dots, numOfClusters, -1, plottingOpts, "Clusters to parametrize");
    if (paramOpts.plot) {
        std::vector<int> order(numOfClusters, 0); // placeholder array (needed for plotting during tracking)
        plotLines(parametrizations, numOfClusters, -1, order, plottingOpts, "lines");
    }

    return numOfClusters;
}

void fitWhiskers(Eigen::MatrixXf *clusterPoints, std::vector<int> &clusterSize, param_opts paramOpts,
                std::vector<struct whisker> &parametrizations, std::vector<bool> &valid_clusters, int numOfClusters) {
    Eigen::MatrixXf xd[numOfClusters];
    Eigen::VectorXf yd[numOfClusters];
    Eigen::Vector3f beta[numOfClusters];
    float D[numOfClusters];
    for (int i = 0; i < numOfClusters; i++) {
        // obtain each cluster's points
        xd[i] = clusterPoints[i].block(0, 0, 2, clusterSize[i]);

        // second case
        D[i] = xd[i].row(1).array().abs().minCoeff();
        xd[i].row(1) = xd[i].row(1).array() - D[i];

        // get an estimation of the angle of the whisker, its begin- and endpoints, and rotation matrix R
        cluster_tips reb = rotationData(xd[i], clusterSize[i], 10, general);
        // If the whisker is too short, we are not interested in it (or it might be a hair), therefore, discard the cluster
        if (reb.length < paramOpts.minLength) {
            clusterSize[i] = -1;
            valid_clusters[i] = false;
            continue;
        }

        // define a whisker parametrization and get a first estimation of angle, shape and position
        parametrizations[i].angle = reb.angle_general;
        parametrizations[i].shape = 0;
        // estimate position of the middle of the whisker
        Eigen::Vector2f middle = (reb.beginPoint + reb.endPoint) / 2;
        // and use that, and the estimated rotation matrix R to estimate the position.
        parametrizations[i].position = middle(0) - middle(1) / ((reb.R_general(0, 1)) / (reb.R_general(0, 0)));

        // initialize points to fit to zero
        yd[i] = Eigen::VectorXf::Zero(xd[i].cols());

        beta[i] = Eigen::Vector3f(parametrizations[i].position, parametrizations[i].angle, parametrizations[i].shape);
    }

    float MSE[numOfClusters];
#ifdef USE_OPENMP
    omp_set_num_threads(6);
#pragma omp parallel for schedule(dynamic,1)
#endif
    for (int i = 0; i < numOfClusters; i++) {
        if (!valid_clusters[i]) continue;
        int success = 1;
        // Perform non-linear fitting, based on the cost function (earlier defined)
        MSE[i] = nlinfit(xd[i], yd[i], beta[i], &success, optimization_func, paramOpts);
        (void) MSE;
        // in case of an error during parametrization
        if (success == 0) {
            clusterSize[i] = -1;
            valid_clusters[i] = false;
            continue;
        }
    }

    for (int i = 0; i < numOfClusters; i++) {
        parametrizations[i].position = beta[i](0);
        parametrizations[i].angle = beta[i](1);
        parametrizations[i].shape = beta[i](2);

        // If the parametrization does not comply with the boundary conditions, discard this cluster
        if ((abs(parametrizations[i].position) > paramOpts.absMaxX)
            || (parametrizations[i].angle < paramOpts.minAngleToSnout)
            || (parametrizations[i].angle > CV_PI - paramOpts.minAngleToSnout)
            || (abs(parametrizations[i].shape) > paramOpts.maxBend)
//            || (MSE[i] > paramOpts.maxMSE)
            ) {
            clusterSize[i] = -1;
            valid_clusters[i] = false;
            continue;
        }

        // Determine the tip of the whisker
        xd[i].row(0) = xd[i].row(0).array() - parametrizations[i].position;
        Eigen::MatrixXf rotationAngle(2, 2);
        rotationAngle << cos(parametrizations[i].angle), -sin(parametrizations[i].angle),
                sin(parametrizations[i].angle), cos(parametrizations[i].angle);
        Eigen::MatrixXf dots_r = xd[i].transpose() * rotationAngle;
        float x_max = dots_r.col(0).array().abs().maxCoeff();

        // Calculate the length of the whisker
        float L = integral<float, float>(0, x_max, parametrizations[i].shape, 1e-10,
                                         reinterpret_cast<float (*)(float, float)>(reinterpret_cast<float (*)(
                                                 float)>(whisker_function)));
        // Add whisker length to the whisker description, completing the tuple. We're also retaining the y_min,
        // which comes in handy for fit-whisker-point-to-data tracking
        parametrizations[i].length = L;
        parametrizations[i].x_max = x_max;
        depthTransform(parametrizations[i], D[i], 0);

        // extend the whisker to snout
        parametrizations[i].length += D[i];
    }
}

float nlinfit(Eigen::MatrixXf X, Eigen::VectorXf y, Eigen::Vector3f &beta, int *success,
        Eigen::VectorXf (*model)(Eigen::MatrixXf, Eigen::Vector3f), param_opts paramOpts) {

    nlinfit_options options;
    options.verbose = paramOpts.verbose;
    // default wgtfun: andrews
    options.Tune = 1.339;
    options.wgtfun = andrews;
    if (paramOpts.wgtfun == "bisquare") {
        options.Tune = 4.685;
        options.wgtfun = bisquare;
    }
    else if (paramOpts.wgtfun == "talwar") {
        options.Tune = 2.795;
        options.wgtfun = talwar;
    }

    Eigen::ArrayXf weights = Eigen::ArrayXf::Ones(y.size());
    // define Jacobian
    Eigen::MatrixXf J;
    Eigen::Vector3f beta_ls = beta;
    LMfit(J, X, y, beta_ls, model, weights, options);

    Eigen::VectorXf res = y - (*model)(X, beta_ls);
    float ols_s = res.norm() / sqrt(std::max((long) 1, (long) (res.size() - beta.size())));

    float sig = nlrobustfit(X, y, beta, J, model, ols_s, options);

    // In case of NAN error, signal failure.
    if (sig == -1) *success = 0;

    // Get MSE from Robust fit.
    float mse = sig*sig;
    return mse;
}

/*
 *  Levenberg-Marquardt algorithm for nonlinear regression
 */
int LMfit(Eigen::MatrixXf &J, Eigen::MatrixXf X, Eigen::VectorXf y, Eigen::Vector3f &beta,
        Eigen::VectorXf (*model)(Eigen::MatrixXf, Eigen::Vector3f), Eigen::ArrayXf weights, nlinfit_options options) {

    // set up convergence tolerances from options
    float betatol = options.TolX;
    float rtol = options.TolFun;
    float fdiffstep = options.DerivStep;

    // Set initial weight for LM algorithm
    float lambda = 0.01;
    Eigen::ArrayXf sweights = weights.sqrt();

    // initialize the fitted values according to model
    Eigen::VectorXf yfit = optimization_func(X, beta);
    Eigen::VectorXf r = sweights * (y-yfit).array();
    float sse = r.dot(r);
    bool breakOut = false;

    int iter;
    // starting from 1 to avoid getting stuck at 0 iterations
    for (iter = 1; iter < options.MaxIter + 1; iter++) {
        Eigen::Vector3f betaold = beta;
        float sseold = sse;
        // Compute a finite difference approximation to the Jacobian
//        std::cout << "iter = " << iter << ", beta = " << std::endl << beta << std::endl;
        J = getjacobian(beta, fdiffstep, X, yfit, model, sweights, options.delta_threshold);

        // Levenberg-Marquardt step: inv(J'*J+lambda*D)*J'*r
        Eigen::Vector3f diagJtJ;
        diagJtJ = J.array().pow(2).colwise().sum();

        // define step vector for beta
        Eigen::VectorXf step, rplus(r.size() + 3, 1);
        sse = LM_step(beta, yfit, model, step, r, rplus, J, diagJtJ, lambda, X, y, sweights, false);


        // If the LM step decreased the SSE, decrease lambda to downweight the
        // steepest descent direction.  Prevent underflowing to zero after many
        // successful steps; smaller than eps is effectively zero anyway.
        if (sse < sseold) {
            lambda = 0.1*lambda > eps ? 0.1*lambda : eps;
        }
        else {
            while (sse > sseold) {
                beta = betaold;
                lambda = 10*lambda;
                if (lambda > 1e16) {
                    breakOut = true;
                    break;
                }
                sse = LM_step(beta, yfit, model, step, r, rplus, J, diagJtJ, lambda, X, y, sweights, true);
            }
        }
        // Check step size and change in SSE for convergence.
        if (step.norm() < betatol*(sqrteps+beta.norm())) {
            break;
        }
        else if (abs(sse - sseold) <= rtol*sse) {
            break;
        }
        else if (breakOut) {
            break;
        }
    }
    return iter;
}

Eigen::MatrixXf getjacobian(Eigen::Vector3f theta, float fdiffstep, Eigen::MatrixXf X, Eigen::VectorXf y0,
        Eigen::VectorXf (*model)(Eigen::MatrixXf, Eigen::Vector3f), Eigen::ArrayXf sweights, float delta_threshold) {
    Eigen::MatrixXf J(y0.size(), theta.size());
    for (int i = 0; i < theta.size(); i++) {
        float delta = fdiffstep * theta[i];
        if (i == 1 && delta == 0) {
            float nTheta = sqrt(theta.array().pow(2).sum());
            delta = fdiffstep * (nTheta + (nTheta == 0));
        }
        Eigen::Vector3f thetaNew = theta;
        // theta(2) is multiplicative to the whole term in the optim. function
        // therefore, handle it in a particular manner
        if (i == 2 && delta < delta_threshold) delta = delta_threshold;
        // theta(0) is a bias term, therefore it should be at least 1 to have an effect
        if (i == 0 && delta < 1) delta = 1;

        thetaNew(i) += delta;
        Eigen::VectorXf yplus = (*model)(X, thetaNew);
        Eigen::VectorXf dy = yplus - y0;
        // in the event that delta is too small to have an effect, increase it until it does
        // this should only occur for i == 0 (bias term)
        int k = 0; // for a maximum of 10 times
        while (!dy.any() && k < 10) {
            delta *= 10e+02;
            thetaNew(i) = theta(i) + delta;
            yplus = (*model)(X, thetaNew);
            dy = yplus - y0;
            k++;
        }
        J.col(i) = dy.array() / delta;
    }
    J = J.array().colwise() * sweights;
    return J;
}

float LM_step(Eigen::Vector3f &beta, Eigen::VectorXf &yfit, Eigen::VectorXf (*model)(Eigen::MatrixXf, Eigen::Vector3f),
        Eigen::VectorXf &step, Eigen::VectorXf &r, Eigen::VectorXf &rplus, Eigen::MatrixXf J, Eigen::Vector3f diagJtJ,
        float lambda, Eigen::MatrixXf X, Eigen::VectorXf y, Eigen::ArrayXf sweights, bool lambda_increased) {

    Eigen::MatrixXf Jdiag = Eigen::MatrixXf::Zero(beta.size(), beta.size());
    Jdiag(0, 0) = sqrt(lambda*diagJtJ(0));
    Jdiag(1, 1) = sqrt(lambda*diagJtJ(1));
    Jdiag(2, 2) = sqrt(lambda*diagJtJ(2));
    Eigen::MatrixXf Jplus(J.rows()+Jdiag.rows(), J.cols());
    Jplus << J,
             Jdiag;
    if (!lambda_increased) rplus << r, 0, 0, 0;
    // this is the cheapest QR solver, appears to give the same errors on first frame
    step = Jplus.householderQr().solve(rplus);
    beta = beta + step;

    // Evaluate the fitted values at the new coefficients and compute the residuals and the SSE.
    yfit = (*model)(X, beta);
    r = sweights * (y-yfit).array();
    float sse = r.dot(r);
    // return error
    return sse;
}

float nlrobustfit(Eigen::MatrixXf X, Eigen::VectorXf y, Eigen::Vector3f &beta, Eigen::MatrixXf &J,
        Eigen::VectorXf (*model)(Eigen::MatrixXf, Eigen::Vector3f), float ols_s, nlinfit_options options) {
    float tune = options.Tune;

    Eigen::MatrixXf yfit = model(X, beta);
    Eigen::VectorXf r = -yfit;
    float Delta = sqrteps;

    // Adjust residuals using leverage, as advised by DuMouchel & O'Brien
    // Compute leverage based on X, the Jacobian
    // economy size QR decomposition
    Eigen::MatrixXf Q = J.householderQr().householderQ() * Eigen::MatrixXf::Identity(J.rows(), J.cols());
    Eigen::ArrayXf h = Q.array().pow(2).rowwise().sum();
    // threshold h values at 1
    Eigen::ArrayXf ones(h.rows(), h.cols());
    ones.fill(0.9999);
    h = h.min(ones).eval();

    // Compute adjustment factor
    Eigen::ArrayXf adjfactor = 1 / (1-h.array()).sqrt();
    Eigen::ArrayXf radj = r.array() * adjfactor;

    // If we get a perfect or near perfect fit, the whole idea of finding
    // outliers by comparing them to the residual standard deviation becomes
    // difficult.  We'll deal with that by never allowing our estimate of the
    // standard deviation of the error term to get below a value that is a small
    // fraction of the standard deviation of the raw response values.
    float tiny_s = 1; // this should be 1e-6*std(y) but y is a vector of zeros

    // Main loop of repeated nonlinear fits, adjust weights each time
    int totiter = 0;
    int maxiter = options.MaxIter;
    Eigen::ArrayXf weights(y.size());
    while (maxiter > 0) {
        Eigen::Vector3f beta0 = beta;
        // robust estimate of sigma for residual
        float s = madsigma(radj, beta.size());
        weights = (*options.wgtfun)(radj / ((s > tiny_s ? s : tiny_s) * tune));

        // this is the weighted nlinfit
        int lsiter = LMfit(J, X, y, beta, model, weights, options);
        totiter += lsiter;
        maxiter -= lsiter;
        yfit = (*model)(X, beta);
        r = y-yfit;
        radj = r.array() * adjfactor;

        // if there is no change in any coefficient, the iterations stop.
        if (((beta-beta0).array().abs() < Delta * beta.array().abs().max(beta0.array().abs())).all()) {
            break;
        }
        if (options.verbose > 2) std::cout << "\t\tIterations: " << totiter << ", error = " << r.dot(r) << std::endl;
    }
    if (options.verbose > 1) std::cout << "\tTotal iterations to convergence: " << totiter << ", error = " << r.dot(r) << std::endl;
//    xassert(!isnan(r.dot(r)), "Parametrization: error is NAN. Exiting."); // assert error != nan
    if (isnan(r.dot(r))) return -1;

    // Compute MAD of adjusted residuals after dropping p-1 closest to 0
    int p = beta.size();
    int n = radj.size();
    float mad_s = madsigma(radj, p);

    // Compute a robust scale estimate for the covariance matrix
    float sig = statrobustsigma(radj, mad_s, h, p, options);

    // Be conservative by not allowing this to be much bigger than the ols value
    // if the sample size is not large compared to p^2
    sig = std::max(sig, sqrtf((ols_s*ols_s * p*p + sig*sig * n) / (p*p + n)));
    return sig;
}

Eigen::VectorXf optimization_func(Eigen::MatrixXf X, Eigen::Vector3f beta) {
    Eigen::VectorXf yfit = (beta(2) * (cos(beta(1)) * (X.row(0).array() - beta(0)) + sin(beta(1))
            * X.row(1).array()).pow(2) - (-sin(beta(1)) * (X.row(0).array() - beta(0))
                    + cos(beta(1)) * X.row(1).array())).pow(2);
    return yfit;
}

float madsigma(Eigen::ArrayXf r, int size) {
    // determine the starting element to look for the median value
    int start = r.size() > size ? size : r.size();
    start = start > 1 ? start : 1;
    start -= 1; // to compensate for 0-indexing in C
    Eigen::ArrayXf rs = r.abs();
    // compute the median
    size_t n = (rs.size() - start) / 2;
    std::nth_element(rs.data() + start, rs.data() + n, rs.data() + rs.size());

    return rs(n) / 0.6745;
}

Eigen::ArrayXf andrews(Eigen::ArrayXf r) {
    Eigen::ArrayXf reps = r.abs().max(sqrteps);
    Eigen::ArrayXf w = (reps < CV_PI).cast<float>() * reps.sin() / reps;
    return w;
}

Eigen::ArrayXf bisquare(Eigen::ArrayXf r) {
    Eigen::ArrayXf w = (r.abs() < 1).cast<float>() * (1 - r.pow(2)).pow(2);
    return w;
}

Eigen::ArrayXf talwar(Eigen::ArrayXf r) {
    Eigen::ArrayXf w = (r.abs() < 1).cast<float>() * 1;
    return w;
}

float whisker_function(float x, float shape) {
    return sqrt(1 + pow((2*shape*x),2));
}

float statrobustsigma(Eigen::ArrayXf r, float s, Eigen::ArrayXf h, int p, nlinfit_options options) {
    // Include tuning constant in sigma value
    float st = s*options.Tune;
    // Get standardized residuals
    int n = r.size();
    Eigen::ArrayXf u = r / st;

    // Compute derivative of phi function
    Eigen::ArrayXf phi = u * (*options.wgtfun)(u);
    float delta = 0.0001;
    Eigen::ArrayXf u1 = u - delta;
    Eigen::ArrayXf phi0 = u1 * (*options.wgtfun)(u1);
    u1 = u + delta;
    Eigen::ArrayXf phi1 = u1 * (*options.wgtfun)(u1);
    Eigen::ArrayXf dphi = (phi1 - phi0) / (2*delta);

    // Compute means of dphi and phi^2; called a and b by Street.  Note that we
    // are including the leverage value here as recommended by O'Brien.
    float m1 = dphi.mean();
    float m2 = ((1-h) * phi.square()).sum() / (n-p);

    // Compute factor that is called K by Huber and O'Brien, and lambda by
    // Street.  Note that O'Brien uses a different expression, but we are using
    // the expression that both other sources use.
    float K = 1 + (p/n) * (1-m1) / m1;

    // Compute final sigma estimate.  Note that Street uses sqrt(K) in place of
    // K, and that some Huber expressions do not show the st term here.
    float sig = K * sqrt(m2) * st /(m1);

    return sig;
}

void depthTransform(whisker &whisker, float y_min, float D) {
    float shiftDown = y_min - D;
    whisker.shiftDown = shiftDown;
    whisker.position -= shiftDown / tan(whisker.angle);
}

// remove whiskers that are only parts of other whiskers
void removeRedundantWhiskers(std::vector<struct whisker> &parametrizations, std::vector<int> &clusterSize,
                             std::vector<bool> &valid_clusters, int numOfClusters) {
    for (int i = 0; i < numOfClusters; i++) {
        whisker whisker1 = parametrizations[i];
        for (int j = 0; j < numOfClusters; j++) {
            if (i == j) continue;
            whisker whisker2 = parametrizations[j];
            if (abs(whisker1.angle - whisker2.angle) < 0.001 &&
                abs(whisker1.position - whisker2.position) < 1 &&
                abs(whisker1.shape - whisker2.shape) < 0.001) {
                if (whisker1.length >= whisker2.length) {
                    clusterSize[j] = -1;
                    valid_clusters[j] = false;
                }
                else {
                    clusterSize[i] = -1;
                    valid_clusters[i] = false;
                }
            }
        }
    }
}
