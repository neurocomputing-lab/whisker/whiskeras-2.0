/*
    This file is part of WhiskEras 2.0.

    WhiskEras 2.0 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WhiskEras 2.0 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WhiskEras 2.0.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "stitching_II.h"
#include "../../utils/Timer.hpp"

int stitching_II(std::vector<int> &clusters, std::vector<float> &rotatedPositions, std::vector<int> &clusterSize,
                stitch_II_opts stitchIIOpts, plotting_opts plottingOpts) {
    int maxPointsOfCluster;
    int numOfClusters = filterNoisyClusters(clusters, rotatedPositions, clusterSize, &maxPointsOfCluster,
            stitchIIOpts.minClusterSize); // filter out clusters with few points as noise
    if (numOfClusters == 0) return 0;
    int numOfPoints = clusters.size();

    // keep the cluster points IDs for the merge, each cluster has a size of the total points to save time later

    maxPointsOfCluster = 0;
    // count the number of points in each cluster
    for (int cluster : clusters) {
        clusterSize[cluster]++;
        if ((cluster + 1) > numOfClusters) numOfClusters = cluster + 1;
        if (maxPointsOfCluster < clusterSize[cluster])
            maxPointsOfCluster = clusterSize[cluster]; // store the max number of points in clusters
    }

    clusterSize.resize(numOfClusters);
    std::fill(clusterSize.begin(), clusterSize.end(), 0);
    Eigen::MatrixXf clusterPoints[numOfClusters];
    Eigen::MatrixXf dots[numOfClusters];
    for (int i = 0; i < numOfClusters; i++) {
        clusterPoints[i].resize(2, maxPointsOfCluster);
    }
    for (int i = 0; i < numOfPoints; i++) {
        clusterPoints[clusters[i]](0, clusterSize[clusters[i]]) = rotatedPositions[2 * i];
        clusterPoints[clusters[i]](1, clusterSize[clusters[i]]) = rotatedPositions[2 * i + 1];
        clusterSize[clusters[i]]++;
    }

    // resize clusterPoints matrix to fit each cluster's size
    for (int i = 0; i < numOfClusters; i++) {
        dots[i] = clusterPoints[i].block(0, 0, 2, clusterSize[i]);
    }

    std::vector<bool> clustersToRemove(numOfClusters, false);
    for (int i = 0; i < numOfClusters; i++) {
        Eigen::MatrixXf::Index minRow, minCol;
        float minY = dots[i].row(1).minCoeff(&minRow, &minCol);
        std::vector<bool> alreadyStitched(numOfClusters, false);
        // if the cluster is floating away from the snout
        while (minY > stitchIIOpts.highestRoot && !clustersToRemove[i]) {
            int inocPoint, inocCluster;
            Eigen::MatrixXf addendum;

            findInoculationPoint(addendum, inocPoint, inocCluster, dots, alreadyStitched, clusterSize[i], i,
                                 numOfClusters, stitchIIOpts, plottingOpts);

            int numOfPointsToAdd;
            // if the point is close enough, add all the whisker points of this cluster below the inoculation point
            if (inocCluster != -1) {
                numOfPointsToAdd = addendum.cols();
                // if (almost) entire cluster is stitched, remove that cluster
                if (dots[inocCluster].cols() <= numOfPointsToAdd + 3) clustersToRemove[inocCluster] = true;
                alreadyStitched[inocCluster] = true;
            } else {
                break;
            }

            // add points of the chosen cluster to this cluster
            clusters.reserve(clusters.size() + numOfPointsToAdd);
            rotatedPositions.reserve(rotatedPositions.size() + 2 * numOfPointsToAdd);
            Eigen::MatrixXf newDots(2, dots[i].cols() + numOfPointsToAdd);
            // add points below the inoculation point
            for (int k = 0; k < addendum.cols(); k++) {
                rotatedPositions.push_back(addendum(0, k));
                rotatedPositions.push_back(addendum(1, k));
                clusters.push_back(i);
                clusterSize[i]++;
            }
            newDots << dots[i], addendum;
            dots[i].resize(2, dots[i].cols() + numOfPointsToAdd);
            dots[i] << newDots;
            // update in case it is still floating
            minY = addendum.row(1).minCoeff(&minRow, &minCol);
        }
    }
    // remove clusters that are still floating away from the snout
    removeFloatingClusters(clusters, rotatedPositions, clustersToRemove, &numOfClusters);
    return numOfClusters;
}

void findInoculationPoint(Eigen::MatrixXf &addendum, int &inocPoint, int &inocCluster, Eigen::MatrixXf *dots,
        std::vector<bool> alreadyStitched, int clusterSize, int floatingClusterIdx,
        int numOfClusters, stitch_II_opts stitchIIOpts, plotting_opts plottingOpts) {
    // determine rotation matrix for the tail, and begin and end points
    cluster_tips r_bottom = rotationData(dots[floatingClusterIdx], clusterSize, stitchIIOpts.lengthOfTipAndBottom, bottom);

    Eigen::MatrixXf otherCluster_rotated[numOfClusters];
    Eigen::Array<bool, Eigen::Dynamic, 1> inds[numOfClusters];
#ifdef USE_OPENMP
    omp_set_num_threads(8);
#pragma omp parallel for
#endif
    for (int j = 0; j < numOfClusters; j++) {
        // avoid stitching the same cluster again to prevent rarely occuring infinite loop
        if (floatingClusterIdx == j || alreadyStitched[j]) continue;
        // align other whiskers with the tail of the current whisker
        otherCluster_rotated[j] = r_bottom.R_bottom * (dots[j].colwise() - r_bottom.beginPoint);
        Eigen::VectorXf d = otherCluster_rotated[j].colwise().norm();
        // find all the dots that are acceptable as inoculation points
        // based on X offset, Y offset (modelled as a cone) and if the points are closer to the snout
        Eigen::Array<bool, Eigen::Dynamic, 1> accX = (otherCluster_rotated[j].row(0).array().abs() <
                                                      stitchIIOpts.acceptableXdistance);
        Eigen::Array<bool, Eigen::Dynamic, 1> accY = ((otherCluster_rotated[j].row(1).array().abs() - d.transpose().array()*sin(stitchIIOpts.cone)) < 0);
        Eigen::Array<bool, Eigen::Dynamic, 1> closerToSnout = (otherCluster_rotated[j].row(0).array() < 0);
        inds[j] = accX * accY * closerToSnout;
    }

    inocPoint = -1;
    inocCluster = -1;
    float inoc_value = FLT_MAX;
    Eigen::MatrixXf addendum_local;
    for (int j = 0; j < numOfClusters; j++) {
        // avoid stitching the same cluster again to prevent rarely occuring infinite loop
        if (floatingClusterIdx == j || alreadyStitched[j]) continue;
        // if no point in cluster is satisfying continue searching for other clusters
        if (!inds[j].any()) continue;

        bool newInoc = false;
        float thisInocValue = inoc_value;
        int thisInocPoint = -1;

//        // plot clusters to stitch for debugging
//        Eigen::MatrixXf plottingDots[2];
//        plottingDots[0] = dots[floatingClusterIdx];
//        plottingDots[1] = dots[j];
//        plotMultipleClusters(plottingDots, 2, plottingOpts, "To stitch II");

        // Find the best inoculation point (closest to the tail)
        for (int i = 0; i < otherCluster_rotated[j].cols(); i++) {
            if (inds[j](i) && abs(otherCluster_rotated[j](0, i)) < thisInocValue) {
                thisInocValue = abs(otherCluster_rotated[j](0, i));
                thisInocPoint = i;
                newInoc = true;
            }
        }
        if (!newInoc) continue;

        // points to be stitched
        addendum_local.resize(2, dots[j].cols());
        int k = 0;
        for (int i = 0; i < dots[j].cols(); i++) {
            if (dots[j](1,i) <= dots[j](1,thisInocPoint)) {
                addendum_local.col(k) = dots[j].col(i);
                k++;
            }
        }
        // if there are too few points to stitch, skip
        if (k < 5) continue;
        addendum_local = addendum_local.leftCols(k).eval();

        // compute inoc_cluster's angle (of last points to be stitched).
        // if their angles are very different then don't stitch the two
        // clusters together.
        cluster_tips r_inoc = rotationData(addendum_local, addendum_local.cols(), stitchIIOpts.lengthOfTipAndBottom, tip);
        float beta = abs(r_inoc.angle_tip - r_bottom.angle_bottom);
        if (beta < stitchIIOpts.maxAD) {
            inocPoint = thisInocPoint;
            inocCluster = j;
            inoc_value = thisInocValue;
            addendum = addendum_local;
        }
    }
}

void removeFloatingClusters(std::vector<int> &clusters, std::vector<float> &rotatedPositions,
                            std::vector<bool> clustersToRemove, int *numOfClusters) {
    // count number of removed clusters before each cluster
    std::vector<int> removedClusters(*numOfClusters+1, 0);
    for (int i = 1; i < *numOfClusters+1; i++) {
        removedClusters[i] = removedClusters[i-1];
        if (clustersToRemove[i-1]) removedClusters[i]++;
    }
    std::vector<int> clustersFromSnout;
    std::vector<float> rotatedPositionsFromSnout;
    // remove floating clusters
    for (size_t i = 0; i < clusters.size(); i++) {
        if (!clustersToRemove[clusters[i]]) {
            clustersFromSnout.push_back(clusters[i]-removedClusters[clusters[i]]);
            rotatedPositionsFromSnout.push_back(rotatedPositions[2*i]);
            rotatedPositionsFromSnout.push_back(rotatedPositions[2*i+1]);
        }
    }
    clusters = clustersFromSnout;
    rotatedPositions = rotatedPositionsFromSnout;
    *numOfClusters -= removedClusters[*numOfClusters];
}

void rotateBackDots(Eigen::MatrixXf rotatedMat, std::vector<float> &plotPositions, plotting_opts plottingOpts) {
    Eigen::MatrixXf plotMat(2, rotatedMat.cols());
    Eigen::MatrixXf sbaseMat = Eigen::Map<Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>>(plottingOpts.sbase[0], 2, 2);
    rotatedMat.row(1) = rotatedMat.row(1).array().eval() + plottingOpts.cutoff;
    Eigen::Vector2f originVec = Eigen::Map<Eigen::Vector2f>(plottingOpts.origin, 2);
    plotMat = (sbaseMat.inverse() * rotatedMat).colwise() + originVec;
    std::vector<float> plotVec(plotMat.data(), plotMat.data() + plotMat.rows() * plotMat.cols());
    plotPositions = plotVec;
}

void plotMultipleClusters(Eigen::MatrixXf *dots, int numOfClusters, plotting_opts plottingOpts, std::string label) {
    cv::Mat image(plottingOpts.height, plottingOpts.width, CV_8UC3, cv::Scalar(0, 0, 0));
    cv::RNG rng(numOfClusters);
    for (int i = 0; i < numOfClusters; i++) {
        cv::Vec3b color(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));
        plotDots(dots[i], plottingOpts, image, color, label);
    }
    showImage(image, label, 0, false, plottingOpts);
}

void plotDots(Eigen::MatrixXf dots, plotting_opts plottingOpts, cv::Mat& image, cv::Vec3b color, std::string label) {
    std::vector<float> plotPositions;
    rotateBackDots(dots, plotPositions, plottingOpts);

    for (int i = 0; i < dots.cols(); i++) {
        int plotX = plotPositions[2 * i];
        int plotY = plotPositions[2 * i + 1];
        image.at<cv::Vec3b>(plotY, plotX) = color;
    }
}
