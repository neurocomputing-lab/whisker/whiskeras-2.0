/*
    This file is part of WhiskEras 2.0.

    WhiskEras 2.0 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WhiskEras 2.0 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WhiskEras 2.0.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef FWTS_C_STITCHING_II_H
#define FWTS_C_STITCHING_II_H

#include "../../../libs/eigen/Eigen/Dense"
#include <opencv2/opencv.hpp>
#include "../../utils/plotting.h"
#include "whisker_forming_options.h"
#include "stitching_I.h"

// detect overlapping centerlines

void findInoculationPoint(Eigen::MatrixXf &addendum, int &inocPoint, int &inocCluster, Eigen::MatrixXf *dots,
                          std::vector<bool> alreadyStitched, int clusterSize, int floatingClusterIdx,
                          int numOfClusters, stitch_II_opts stitchIIOpts, plotting_opts plottingOpts);

void removeFloatingClusters(std::vector<int> &clusters, std::vector<float> &rotatedPositions,
                            std::vector<bool> clustersToRemove, int *numOfClusters);

int stitching_II(std::vector<int> &clusters, std::vector<float> &rotatedPositions, std::vector<int> &clusterSize,
                 stitch_II_opts stitchIIOpts, plotting_opts plottingOpts);

void rotateBackDots(Eigen::MatrixXf rotatedMat, std::vector<float> &plotPositions, plotting_opts plottingOpts);

void plotMultipleClusters(Eigen::MatrixXf *dots, int numOfClusters, plotting_opts plottingOpts, std::string label);

void plotDots(Eigen::MatrixXf dots, plotting_opts plottingOpts, cv::Mat& image, cv::Vec3b color, std::string label);

#endif //FWTS_C_STITCHING_II_H
