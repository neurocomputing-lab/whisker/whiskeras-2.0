/*
    this file is part of whiskeras 2.0.

    whiskeras 2.0 is free software: you can redistribute it and/or modify
    it under the terms of the gnu general public license as published by
    the free software foundation, either version 3 of the license, or
    (at your option) any later version.

    whiskeras 2.0 is distributed in the hope that it will be useful,
    but without any warranty; without even the implied warranty of
    merchantability or fitness for a particular purpose.  see the
    gnu general public license for more details.

    you should have received a copy of the gnu general public license
    along with whiskeras 2.0.  if not, see <https://www.gnu.org/licenses/>.
*/

#ifndef FWTS_C_PREPROCESSING_H
#define FWTS_C_PREPROCESSING_H

#include <opencv2/opencv.hpp>
#include "../../utils/plotting.h"

struct preprocessing_opts {
    // background extraction parameters
    // whiskers shave and morphology extraction parameters
    uchar bwThreshold = 1;
    int dilate_value = 40;
    // noise-filtering parameters
    bool deinterlacingFlag = false;
    float initialGaussianSigma = 0;
    // contrast enhancement parameters
    int histEqMinT = 5;
    int histEqMaxT = 80;
    float histEqGamma = 1;
    double adjustRangeIn[2];
    double adjustRangeOut[2] = {0, 1};
    // invert colors if background is black and whiskers are white
    bool invertColors = false;
    // edge removal parameters
    int remove_lowlevels = 0;
    int framing = 5;
    float upscaling = 2;
    int verbose = 1;
    bool plot = false;
};

cv::Mat preprocessing(cv::Mat src, cv::Mat bgImage, cv::Mat bwShape, const preprocessing_opts pre_opts, plotting_opts plottingOpts);

cv::Mat getBackGroundImageAndShape(cv::VideoCapture video, cv::Mat &bwShape, preprocessing_opts *pre_opts,
                                   int startFrame, int endFrame);

cv::Mat extractWhiskersGrayValuesWithMorphologyAndShave(cv::Mat frame, float bwThreshold, int razorSize);

#endif //FWTS_C_PREPROCESSING_H
