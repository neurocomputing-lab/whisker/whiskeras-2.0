/*
    This file is part of WhiskEras 2.0.

    WhiskEras 2.0 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WhiskEras 2.0 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WhiskEras 2.0.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef FWTS_C_OPTIONS_H
#define FWTS_C_OPTIONS_H

#include "../stages/whisker_point_detection/preprocessing.h"
#include "../stages/whisker_point_detection/c_extraction.h"
#include "../stages/whisker_forming/stitching_I.h"
#include "../stages/whisker_forming/parametrization.h"
#include "../stages/whisker_forming/clustering.h"
#include "../cuda/c_extraction_cuda.h"
#include "../stages/tracking_over_time/tracking.h"
#include "../stages/tracking_over_time/training.h"

struct general_opts {
    cv::String videopath;
    // position of the snout
    int tip[2] = {765, 69};
    int middle[2] = {29, 296};
    // start and end frame
    int startFrame = 0;
    int endFrame = -1;
    bool use_cuda = true; // use GPU
    bool loadTracks = false; // load whisker tracks from file
    std::string loadTracksLocation;
    bool preview = false;
    int verbose = 1;
};

struct dump_opts {
    bool saveTracks = false;
    std::string saveTracksLocation;
    bool getTimingProfile = false;
    std::string timeProfileLocation;
    bool getQualityResults = false;
    std::string qualityOutputDir;
    std::string outputFilename;
    std::string videoOutputPath;
};

void get_opts(general_opts &generalOpts, dump_opts &dumpOpts, preprocessing_opts &pre_opts, c_extraction_opts &whiskerDetectionOpts,
              c_extraction_cuda &detectionCuda, clustering_opts &clusteringOpts, stitch_I_opts &stitchIOpts,
              stitch_II_opts &stitchIIOpts, param_opts &paramOpts, track_opts &trackOpts, train_opts &trainOpts,
              fwptd_opts &fwptdOpts, recog_opts &recogOpts, N_expert_opts &NExpertOpts);

void get_general_opts(general_opts &generalOpts, float upscaling);

void get_plotting_opts(plotting_opts &plottingOpts, clustering_opts clusteringOpts, float upscaling, cv::VideoCapture video,
                       bool &preprocessing, bool &centerlineExtraction, bool &clustering, bool &stitching_I,
                       bool &stitching_II, bool &parametrization, bool &tracking);

void determinePlottingSnoutCutoff(plotting_opts &plottingOpts, clustering_opts clusteringOpts);

void get_dump_opts(dump_opts &dumpOpts);

void get_preprocessing_opts(preprocessing_opts &pre_opts);

void computeAdjustRange(preprocessing_opts &pre_opts);

void get_c_extraction_opts(c_extraction_opts &cExtractionOpts, bool upside_whiskers, float a, float b, bool verbose);

void get_clustering_opts(clustering_opts &clusteringOpts, general_opts generalOpts);

void determineSnoutCutoff(clustering_opts &clusteringOpts, general_opts generalOpts);

void get_stitching_I_opts(stitch_I_opts &stitchIOpts, float upscaling);

void get_stitching_II_opts(stitch_II_opts &stitchIIOpts, float upscaling);

void get_parametrization_opts(param_opts &paramOpts, preprocessing_opts pre_opts, clustering_opts clusteringOpts, general_opts generalOpts);

void get_tracking_opts(track_opts &trackOpts, float *origin, preprocessing_opts pre_opts);

void get_training_opts(train_opts &trainOpts, float minAngleToSnout);

void get_fwptd_opts(fwptd_opts &fwptdOpts);

void get_recognition_opts(recog_opts &recogOpts);

void get_N_expert_opts(N_expert_opts &NExpertOpts);

#endif //FWTS_C_OPTIONS_H
