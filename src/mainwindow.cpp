/*
    This file is part of WhiskEras 2.0.

    WhiskEras 2.0 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WhiskEras 2.0 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WhiskEras 2.0.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QPixmap>
#include <QImage>
#include "imagepreview.h"
#include <QPainter>
#include "QDialog"
#include <QDebug>
#include <QFileDialog>
#include <QSettings>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    /// load options
    get_opts(generalOpts, dumpOpts, preprocessingOpts, cExtractionOpts, cExtractionCuda, clusteringOpts, stitchIOpts,
             stitchIIOpts, paramOpts, trackOpts, trainOpts, fwptdOpts, recogOpts, NExpertOpts);
    // set minimum, maximum of histEqMinT, histEqMaxT and slider values based on spinBox values
    initLimitsAndSliders();

    // set output dir
    const QString DEFAULT_OUTPUT_DIR_KEY("default_results_dir");
    QSettings settings("Erasmus MC Neuroscience Lab", "WhiskEras 2.0");
    dumpOpts.qualityOutputDir = settings.value(DEFAULT_OUTPUT_DIR_KEY, "").toString().toLocal8Bit().constData();
    dumpOpts.getQualityResults = true;

    // restore geometry and state to last session's
    this->restoreGeometry(settings.value("MainWindow/geometry").toByteArray());
    this->restoreState(settings.value("MainWindow/state").toByteArray());
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::resizeEvent(QResizeEvent *event)
{
    QSettings settings("Erasmus MC Neuroscience Lab", "WhiskEras 2.0");
    settings.setValue("MainWindow/geometry", saveGeometry());
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    QSettings settings("Erasmus MC Neuroscience Lab", "WhiskEras 2.0");
    settings.setValue("MainWindow/geometry", saveGeometry());
    settings.setValue("MainWindow/state", saveState());
    QMainWindow::closeEvent(event);
}

void MainWindow::initLimitsAndSliders()
{
    ui->histEqMinT_spinBox->setMaximum(ui->histEqMaxT_spinBox->value()-1);
    ui->histEqMaxT_spinBox->setMinimum(ui->histEqMinT_spinBox->value()+1);
    ui->preview_histEqGamma_slider->setValue(ui->preview_histEqGamma_spinBox->value()*100);
    ui->preview_dilateValue_slider->setValue(ui->preview_dilateValue_spinBox->value());
    ui->preview_minClusterSize_I_slider->setValue(ui->preview_minClusterSize_I_spinBox->value());
    ui->preview_minClusterSize_II_slider->setValue(ui->preview_minClusterSize_II_spinBox->value());
    ui->preview_minLength_slider->setValue(ui->preview_minLength_spinBox->value());
}

void MainWindow::videoProps2Options()
{
    // open whisker video and get some properties
    video = openVideo(generalOpts.videopath);
    videoWidth = video.get(cv::CAP_PROP_FRAME_WIDTH);
    videoHeight = video.get(cv::CAP_PROP_FRAME_HEIGHT);
    numFrames = video.get(cv::CAP_PROP_FRAME_COUNT);
    generalOpts.startFrame = 0;
    generalOpts.endFrame = numFrames - 1;
    cExtractionCuda.width = videoWidth * preprocessingOpts.upscaling;
    cExtractionCuda.height = videoHeight * preprocessingOpts.upscaling;
    clusteringOpts.width = videoWidth*preprocessingOpts.upscaling;
    clusteringOpts.height = videoHeight*preprocessingOpts.upscaling;

    get_plotting_opts(plottingOpts, clusteringOpts, preprocessingOpts.upscaling, video, preprocessingOpts.plot,
                      clusteringOpts.plotPointsOnly, clusteringOpts.plot, stitchIOpts.plot,
                      stitchIIOpts.plot, paramOpts.plot, trackOpts.plot);
}

void MainWindow::getBackgroundForPreview()
{
    if (generalOpts.use_cuda) {
#ifdef USE_CUDA
        getBackground_cuda(bgImage_d, bwShape_d, preprocessedImage_d, video, dumpOpts, generalOpts, preprocessingOpts, cExtractionCuda);
#else
        std::cout << "CUDA is not supported. If it is not recognized, try fixing the problem."
                     " Otherwise, try running without using CUDA instead." << std::endl;
        exit(-1);
#endif
    }
    else {
        getBackground(bgImage, bwShape, video, dumpOpts, generalOpts, preprocessingOpts);
    }
}

void MainWindow::previewMode()
{
    timers timers;
    // set preview options
    dumpOpts.getTimingProfile = false;
    generalOpts.startFrame = ui->previewFrame_slider->value();
    generalOpts.endFrame = generalOpts.startFrame + 1;
    trackOpts.dontTrack = true;
    generalOpts.preview = true;
    plottingOpts.dontShow = true;

    cv::Mat overlaid_whiskers;
    if (generalOpts.use_cuda) {
#ifdef USE_CUDA
        allocateCudaStructs(cExtractionCuda, cExtractionOpts);
        run_processing_cuda(video, bgImage_d, bwShape_d, preprocessedImage_d, timers, preprocessingOpts, cExtractionOpts,
                            cExtractionCuda, clusteringOpts, stitchIOpts, stitchIIOpts, paramOpts, trackOpts, trainOpts,
                            fwptdOpts, recogOpts, NExpertOpts, plottingOpts, generalOpts, dumpOpts, overlaid_whiskers);
        freeCudaStructs(cExtractionCuda, cExtractionOpts);
#else
        std::cout << "CUDA is not supported. If it is not recognized, try fixing the problem."
                     " Otherwise, try running without using CUDA instead." << std::endl;
        exit(-1);
#endif
    }
    else {
        // Run without using GPU
        run_sequential(video, bgImage, bwShape, timers, preprocessingOpts, cExtractionOpts, clusteringOpts, stitchIOpts,
                       stitchIIOpts, paramOpts, trackOpts, trainOpts, fwptdOpts, recogOpts, NExpertOpts, plottingOpts,
                       generalOpts, dumpOpts, overlaid_whiskers);
    }
    // preview detected overlaid whiskers
    cv::Mat dest;
    cv::cvtColor(overlaid_whiskers, dest, cv::COLOR_BGR2RGB);
    QImage image1 = QImage((uchar*) dest.data, dest.cols, dest.rows, dest.step, QImage::Format_RGB888);
    QPixmap pixmap = QPixmap::fromImage(image1);
    ui->previewLabel->setPixmap(pixmap.scaled(1.5*videoWidth, 1.5*videoHeight, Qt::KeepAspectRatio));
    // display number of whiskers
    ui->numDetectedWhiskers_label->setText(QString::number(paramOpts.numDetectedWhiskers));
}

void MainWindow::on_beginProcessing_button_clicked()
{
    if (!processing) {
        processing = true;
        ui->beginProcessing_button->setText("Stop");
        worker_startFrame = ui->startFrame_slider->value();
        worker_endFrame = ui->endFrame_slider->value();
        Worker *worker = new Worker;
        // define options and structs required
        worker->video = this->video;
        worker->generalOpts = this->generalOpts;
        worker->generalOpts.startFrame = worker_startFrame;
        worker->generalOpts.endFrame = worker_endFrame;
        worker->dumpOpts = this->dumpOpts;
        worker->plottingOpts = this->plottingOpts;
        if (plottingOpts.writeVideoOutput)
            worker->plottingOpts.videoOutput = cv::VideoWriter(dumpOpts.videoOutputPath,
                                                               cv::VideoWriter::fourcc('M','J','P','G'),
                                                               video.get(cv::CAP_PROP_FPS),
                                                               cv::Size(plottingOpts.width, plottingOpts.height), true);
        worker->preprocessingOpts = this->preprocessingOpts;
        worker->cExtractionOpts = this->cExtractionOpts;
        worker->clusteringOpts = this->clusteringOpts;
        worker->stitchIOpts = this->stitchIOpts;
        worker->stitchIIOpts = this->stitchIIOpts;
        worker->paramOpts = this->paramOpts;
        worker->trackOpts = this->trackOpts;
        worker->fwptdOpts = this->fwptdOpts;
        worker->trainOpts = this->trainOpts;
        worker->recogOpts = this->recogOpts;
        worker->NExpertOpts = this->NExpertOpts;
        worker->numFrames = this->numFrames;
        worker->bgImage = this->bgImage;
        worker->bwShape = this->bwShape;
#ifdef USE_CUDA
        worker->cExtractionCuda = this->cExtractionCuda;
        worker->bgImage_d = this->bgImage_d;
        worker->bwShape_d = this->bwShape_d;
        worker->preprocessedImage_d = this->preprocessedImage_d;
#endif
        connect(&workerThread, SIGNAL(finished()), worker, SLOT(deleteLater()));
        connect(&workerThread, SIGNAL(finished()), this, SLOT(enableProcessingButton()));
        connect(worker, SIGNAL(newFrame(int)), this, SLOT(on_frameProcessed(int)));
        worker->setupWorkerProcessing(workerThread);
        worker->moveToThread(&workerThread);
        workerThread.start();
        workerThread.quit();
    }
    else {
        workerThread.requestInterruption();
        ui->beginProcessing_button->setEnabled(false);
    }
}

void MainWindow::enableProcessingButton()
{
    ui->beginProcessing_button->setEnabled(true);
    ui->beginProcessing_button->setText("Start");
    ui->processing_progressBar->setValue(0);
    processing = false;
}

void MainWindow::setupProcessingThread(QThread &thread)
{
    connect(&thread, SIGNAL(started()), this, SLOT(beginProcessingWithThread()));
}

void MainWindow::on_SnoutLineButton_clicked()
{
    generalOpts.startFrame = 0;
    generalOpts.endFrame = numFrames - 1;
    snoutLineWindow = new SnoutLineWindow(video, generalOpts.startFrame, this);
    snoutLineWindow->setModal(true);
    snoutLineWindow->show();
    connect(snoutLineWindow, SIGNAL(accepted()), this, SLOT(snoutLineDialog_accepted()));
    connect(snoutLineWindow, SIGNAL(bgFramesChanged(int,int)), this, SLOT(onBgFramesChanged(int,int)));
}

void MainWindow::on_drawSnoutLine_advanced_button_clicked()
{
    on_SnoutLineButton_clicked();
}

void MainWindow::onBgFramesChanged(int first, int last)
{
    generalOpts.startFrame = first;
    generalOpts.endFrame = last;
}

void MainWindow::snoutLineDialog_accepted()
{
    generalOpts.tip[0] = preprocessingOpts.upscaling * snoutLineWindow->tip[0];
    generalOpts.tip[1] = preprocessingOpts.upscaling * snoutLineWindow->tip[1];
    generalOpts.middle[0] = preprocessingOpts.upscaling * snoutLineWindow->middle[0];
    generalOpts.middle[1] = preprocessingOpts.upscaling * snoutLineWindow->middle[1];
    clusteringOpts.upside_whiskers = snoutLineWindow->upside_whiskers;
    cExtractionOpts.upside_whiskers = clusteringOpts.upside_whiskers;
    preprocessingOpts.invertColors = snoutLineWindow->invertColors;
    determineSnoutCutoff(clusteringOpts, generalOpts);
    cExtractionOpts.a_value = clusteringOpts.a_value;
    cExtractionOpts.b_value = clusteringOpts.b_value;
    determinePlottingSnoutCutoff(plottingOpts, clusteringOpts);
    paramOpts.absMaxX = sqrt(pow(generalOpts.tip[0]-generalOpts.middle[0], 2) + pow(generalOpts.tip[1]-generalOpts.middle[1], 2)) / 1.4;

    ui->beginProcessing_button->setEnabled(true);
    snoutLineDrawn = true;
    // get background
    getBackgroundForPreview();
    // begin Preview
    previewMode();
}

void MainWindow::on_openWhiskerVideo_triggered()
{
    // set default directory to the video file's opened in the last session
    const QString DEFAULT_VIDEO_DIR_KEY("default_video_dir");
    QSettings settings("Erasmus MC Neuroscience Lab", "WhiskEras 2.0");

    QString fileName = QFileDialog::getOpenFileName(this, tr("Open whisker-video file"), settings.value(DEFAULT_VIDEO_DIR_KEY).toString(), tr("Image Files (*.avi *.mp4 *.mkv)"));
    if (!fileName.isEmpty()) {
        // save video path for next sessions
        QDir CurrentDir;
        settings.setValue(DEFAULT_VIDEO_DIR_KEY, CurrentDir.absoluteFilePath(fileName));

        // open video
        generalOpts.videopath = fileName.toLocal8Bit().constData();
        if (video.isOpened()) {
            video.release();
            cv::destroyAllWindows(); // close all the frames
#ifdef USE_CUDA
            // release bgImage from device
//            freeBgData(cExtractionCuda);
            resetGPU();
#endif
        }
        videoOpened = true;
        // get background and set options
        getOptionsAndConfigUI();

        // get the filename (without the full path)
        dumpOpts.outputFilename = getFilenameFromPath(generalOpts.videopath, "whiskervideo", 1);
        // remove the filename extension (e.g. '.avi')
        dumpOpts.outputFilename = removeFilenameExtension(dumpOpts.outputFilename);
    }
}

void MainWindow::getOptionsAndConfigUI()
{
    // set some options which depend on the video
    videoProps2Options();
    // set GUI options to match the default options
    setHistEqLimits();
    // set options to match GUI default options
    initializeOptions();
    // configure UI (enable sliders, spinboxes, set frame limits etc)
    configUI();
    // set processing start and end frames
    worker_startFrame = 0;
    worker_endFrame = numFrames - 1;
}

void MainWindow::configUI()
{
    // ensure that no preview is possible until snout line is drawn
    snoutLineDrawn = false;

    // configure some ui values/limits
    ui->previewFrame_slider->setMaximum(numFrames-1);
    ui->previewFrame_spinBox->setMaximum(numFrames-1);
    ui->previewFrame_slider->setValue(0);
    ui->beginProcessing_button->setEnabled(false);
    ui->SnoutLineButton->setEnabled(true);
    ui->drawSnoutLine_advanced_button->setEnabled(true);
    ui->startFrame_slider->setEnabled(true);
    ui->startFrame_spinBox->setEnabled(true);
    ui->endFrame_slider->setEnabled(true);
    ui->endFrame_spinBox->setEnabled(true);
    ui->videoOutputDir_button->setEnabled(true);
    ui->startFrame_slider->setMaximum(numFrames-2);
    ui->startFrame_spinBox->setMaximum(numFrames-2);
    ui->endFrame_slider->setMaximum(numFrames-1);
    ui->endFrame_spinBox->setMaximum(numFrames-1);
    ui->startFrame_slider->setValue(0);
    ui->startFrame_spinBox->setValue(0);
    ui->endFrame_slider->setValue(numFrames-1);
    ui->endFrame_spinBox->setValue(numFrames-1);
    ui->videoOutput_checkBox->setEnabled(true);
}

std::string MainWindow::getFilenameFromPath(std::string path, std::string default_str, size_t bytes_per_char)
{
    std::string filename;
    const size_t last_slash_idx = path.rfind('/');
    const size_t last_dot_idx = path.rfind('.');
    // if there is a match with character '/' i.e. if it contains a directory
    if (std::string::npos != last_slash_idx) {
        size_t last_idx = path.length() / bytes_per_char - 1;
        filename = path.substr(last_slash_idx + 1, last_idx);
    }
    else {
        filename = default_str;
    }
    return filename;
}

std::string MainWindow::removeFilenameExtension(std::string filename)
{
    std::string filenameNoExt;
    const size_t last_dot_idx = filename.rfind('.');
    // if there is an extension, remove it
    if (std::string::npos != last_dot_idx) {
            filenameNoExt = filename.substr(0, last_dot_idx);
    }
    else {
        filenameNoExt = filename;
    }
    return filenameNoExt;
}

void MainWindow::on_previewFrame_slider_valueChanged(int value)
{
    generalOpts.startFrame = value;
    if (snoutLineDrawn) previewMode();
}


void MainWindow::initializeOptions()
{
    // general options
    generalOpts.use_cuda = ui->useGPU_checkBox->isChecked();
    // preprocessing options
    preprocessingOpts.bwThreshold = ui->bwThreshold_spinBox->value();
    preprocessingOpts.dilate_value = ui->dilateValue_spinBox->value();
    preprocessingOpts.deinterlacingFlag = ui->deinterlacingFlag_checkBox->isChecked();
    preprocessingOpts.initialGaussianSigma = ui->gaussianSigma_spinBox->value();
    preprocessingOpts.framing = ui->framing_spinBox->value();
    clusteringOpts.cutoff = ui->cutoff_spinBox->value();
    plottingOpts.cutoff = ui->cutoff_spinBox->value();
    preprocessingOpts.histEqMinT = ui->histEqMinT_spinBox->value();
    preprocessingOpts.histEqMaxT = ui->histEqMaxT_spinBox->value();
    computeAdjustRange(preprocessingOpts);
    preprocessingOpts.histEqGamma = ui->histEqGamma_spinBox->value();
    preprocessingOpts.remove_lowlevels = ui->removeLowLevels_spinBox->value();
    preprocessingOpts.upscaling = ui->upscaling_spinBox->value();
    // centerline extraction options
    cExtractionOpts.tr2 = ui->tr2_spinBox->value();
    cExtractionOpts.sigma = ui->sigma_spinBox->value();
    cExtractionOpts.gaussianKernelSize = ui->gaussianKernelSize_spinBox->value();
    // local clustering options
    clusteringOpts.steger_c = ui->steger_c_spinBox->value();
    clusteringOpts.maxAngle = ui->maxAngle_spinBox->value();
    clusteringOpts.K = ui->K_spinBox->value();
    clusteringOpts.M = ui->M_spinBox->value();
    clusteringOpts.L = ui->L_spinBox->value();
    clusteringOpts.minClusterSizeToExtend = ui->beamThreshold_spinBox->value();
    // stitching I options
    stitchIOpts.coneMin = ui->coneMin_spinBox->value() * CV_PI / 180;
    stitchIOpts.coneMax = ui->coneMax_spinBox->value() * CV_PI / 180;
    stitchIOpts.coneMinDist = ui->coneMinDist_spinBox->value();
    stitchIOpts.c = ui->c_spinBox->value();
    stitchIOpts.maxAD = ui->maxAD_spinBox->value() * CV_PI / 180;
    stitchIOpts.lengthOfTipAndBottom = ui->LengthOfTipAndBottom_spinBox->value();
    stitchIOpts.radonLength = ui->radonLength_spinBox->value();
    stitchIOpts.radonThreshold = ui->radonThreshold_spinBox->value();
    stitchIOpts.minClusterSize = ui->minClusterSize_I_spinBox->value();
    // stitching II options
    stitchIIOpts.highestRoot = ui->highestRoot_spinBox->value();
    stitchIIOpts.acceptableXdistance = ui->acceptableXDistance_spinBox->value();
    stitchIIOpts.lengthOfTipAndBottom = ui->LengthOfTipAndBottom_II_spinBox->value();
    stitchIIOpts.cone = ui->cone_spinBox->value() * CV_PI / 180;
    stitchIIOpts.maxAD = ui->maxAD_II_spinBox->value() * CV_PI / 180;
    stitchIIOpts.minClusterSize = ui->minClusterSize_II_spinBox->value();
    // parameter fitting options
    paramOpts.minNrOfDots = ui->minNrOfDots_spinBox->value();
    paramOpts.minLength = ui->minLength_spinBox->value();
    paramOpts.minAngleToSnout = ui->minAngleToSnout_spinBox->value() * CV_PI / 180;
    paramOpts.maxBend = ui->maxBend_spinBox->value();
    // tracking options
    trackOpts.useKalman = ui->useKalman_checkBox->isChecked();
    trackOpts.kalmanDistance = ui->kalmanDistance_spinBox->value();
    trackOpts.weightB = ui->weightB_spinBox->value();
    trackOpts.weightL = ui->weightL_spinBox->value();
    trackOpts.weightX = ui->weightXf_spinBox->value();
    trackOpts.weightAlpha = ui->weightAlpha_spinBox->value();
    trackOpts.dontTrack = ui->dontTrack_checkBox->isChecked();
    trackOpts.maxChangeInAngle = ui->maxChangeInAngle_spinBox->value() * CV_PI / 180;
    trackOpts.maxChangeInXf = ui->maxChangeInXf_spinBox->value();
    trackOpts.maxChangeInBend = ui->maxChangeInBend_spinBox->value();
    trackOpts.maxChangeInLength = ui->maxChangeInLength_spinBox->value();
    // training options
    trainOpts.retrainFreqInit = ui->retrainFreqInit_spinBox->value();
    trainOpts.retrainAtLeast = ui->retrainAtLeast_spinBox->value();
    trainOpts.increaseWith = ui->increaseWith_spinBox->value();
    trainOpts.increaseEvery = ui->increaseEvery_spinBox->value();
    trainOpts.bootstrap = ui->bootstrap_spinBox->value();
    trainOpts.window = ui->window_spinBox->value();
    trainOpts.maxSizeTrainingData = ui->maxSizeTrainingData_spinBox->value();
    trainOpts.chance_train = ui->chance_train_spinBox->value() * 100;
    // recognition options
    recogOpts.weightB = ui->weightB_spinBox_2->value();
    recogOpts.weightX = ui->weightXf_spinBox_2->value();
    recogOpts.weightL = ui->weightL_spinBox_2->value();
    recogOpts.weightA = ui->weightAlpha_spinBox_2->value();
    // N-Expert options
    NExpertOpts.maxChangeInBend = ui->maxChangeInBend_spinBox_2->value();
    NExpertOpts.maxChangeInLength = ui->maxChangeInLength_spinBox_2->value();
    NExpertOpts.maxChangeInAngle = ui->maxChangeInAngle_spinBox_2->value() * CV_PI / 180;
    NExpertOpts.maxChangeInXf = ui->maxChangeInXf_spinBox_2->value();
}

void MainWindow::setHistEqLimits()
{
    ui->histEqMinT_spinBox->setMaximum(preprocessingOpts.histEqMaxT-1);
    ui->histEqMaxT_spinBox->setMinimum(preprocessingOpts.histEqMinT+1);
}

// general options
void MainWindow::on_useGPU_checkBox_clicked(bool checked)
{
    if (checked) {
        generalOpts.use_cuda = true;
        resetGPU();
//        freeBgData(cExtractionCuda);
        ui->gaussianKernelSize_spinBox->setValue(20);
        ui->gaussianKernelSize_spinBox->setEnabled(false);
    }
    else {
        generalOpts.use_cuda = false;
        ui->gaussianKernelSize_spinBox->setEnabled(true);
    }
    if (video.isOpened()) getBackgroundForPreview();
    if (snoutLineDrawn) previewMode();
}

// preprocessing options

void MainWindow::on_bwThreshold_spinBox_valueChanged(int arg1)
{
    preprocessingOpts.bwThreshold = arg1;
    if (videoOpened) {
        if (generalOpts.use_cuda) freeBgData(cExtractionCuda);
        getBackgroundForPreview();
    }
    if (snoutLineDrawn) previewMode();
}

void MainWindow::on_dilateValue_spinBox_valueChanged(int arg1)
{
    preprocessingOpts.dilate_value = arg1;
    ui->preview_dilateValue_slider->setValue(arg1);
    if (videoOpened) {
        if (generalOpts.use_cuda) freeBgData(cExtractionCuda);
        getBackgroundForPreview();
    }
    if (snoutLineDrawn) previewMode();
}

void MainWindow::on_deinterlacingFlag_checkBox_clicked(bool checked)
{
    if (checked) preprocessingOpts.deinterlacingFlag = true;
    else preprocessingOpts.deinterlacingFlag = false;
    if (videoOpened) {
        if (generalOpts.use_cuda) freeBgData(cExtractionCuda);
        getBackgroundForPreview();
    }
    if (snoutLineDrawn) previewMode();
}

void MainWindow::on_gaussianSigma_spinBox_valueChanged(double arg1)
{
    preprocessingOpts.initialGaussianSigma = arg1;
    if (videoOpened) {
        if (generalOpts.use_cuda) freeBgData(cExtractionCuda);
        getBackgroundForPreview();
    }
    if (snoutLineDrawn) previewMode();
}

void MainWindow::on_histEqMinT_spinBox_valueChanged(int arg1)
{
    preprocessingOpts.histEqMinT = arg1;
    preprocessingOpts.adjustRangeIn[0] = double(preprocessingOpts.histEqMinT)/255;
    ui->histEqMaxT_spinBox->setMinimum(preprocessingOpts.histEqMinT+1);
    if (snoutLineDrawn) previewMode();
}

void MainWindow::on_histEqMaxT_spinBox_valueChanged(int arg1)
{
    preprocessingOpts.histEqMaxT = arg1;
    preprocessingOpts.adjustRangeIn[1] = double(preprocessingOpts.histEqMaxT)/255;
    ui->histEqMinT_spinBox->setMaximum(preprocessingOpts.histEqMaxT-1);
    if (snoutLineDrawn) previewMode();
}

void MainWindow::on_histEqGamma_spinBox_valueChanged(double arg1)
{
    preprocessingOpts.histEqGamma = arg1;
    ui->preview_histEqGamma_slider->setValue(arg1*100);
    if (snoutLineDrawn) previewMode();
}

void MainWindow::on_removeLowLevels_spinBox_valueChanged(int arg1)
{
    preprocessingOpts.remove_lowlevels = arg1;
    if (snoutLineDrawn) previewMode();
}

void MainWindow::on_framing_spinBox_valueChanged(int arg1)
{
    preprocessingOpts.framing = arg1;
    if (snoutLineDrawn) previewMode();
}

void MainWindow::on_upscaling_spinBox_valueChanged(double arg1)
{
    preprocessingOpts.upscaling = arg1;
    if (snoutLineDrawn) previewMode();
}

void MainWindow::on_cutoff_spinBox_valueChanged(int arg1)
{
    clusteringOpts.cutoff = arg1;
    plottingOpts.cutoff = arg1;
    if (snoutLineDrawn) previewMode();
}

// centerline extraction options

void MainWindow::on_tr2_spinBox_valueChanged(double arg1)
{
    cExtractionOpts.tr2 = arg1;
    if (snoutLineDrawn) previewMode();
}

void MainWindow::on_sigma_spinBox_valueChanged(double arg1)
{
    cExtractionOpts.sigma = arg1;
    computeGaussianKernels(cExtractionOpts, 0);
    if (snoutLineDrawn) previewMode();
}

void MainWindow::on_gaussianKernelSize_spinBox_valueChanged(int arg1)
{
    cExtractionOpts.gaussianKernelSize = arg1;
    if (snoutLineDrawn) previewMode();
}

// local clustering options

void MainWindow::on_steger_c_spinBox_valueChanged(double arg1)
{
    clusteringOpts.steger_c = arg1;
    if (snoutLineDrawn) previewMode();
}

void MainWindow::on_maxAngle_spinBox_valueChanged(double arg1)
{
    clusteringOpts.maxAngle = arg1;
    if (snoutLineDrawn) previewMode();
}

void MainWindow::on_L_spinBox_valueChanged(int arg1)
{
    clusteringOpts.L = arg1;
    if (snoutLineDrawn) previewMode();
}

void MainWindow::on_M_spinBox_valueChanged(int arg1)
{
    clusteringOpts.M = arg1;
    if (snoutLineDrawn) previewMode();
}

void MainWindow::on_K_spinBox_valueChanged(int arg1)
{
    clusteringOpts.K = arg1;
    if (snoutLineDrawn) previewMode();
}

void MainWindow::on_beamThreshold_spinBox_valueChanged(int arg1)
{
    clusteringOpts.minClusterSizeToExtend = arg1;
    if (snoutLineDrawn) previewMode();
}

// stitching I options

void MainWindow::on_coneMin_spinBox_valueChanged(double arg1)
{
    stitchIOpts.coneMin = arg1 * CV_PI / 180;
    if (snoutLineDrawn) previewMode();
}

void MainWindow::on_coneMax_spinBox_valueChanged(double arg1)
{
    stitchIOpts.coneMax = arg1 * CV_PI / 180;
    if (snoutLineDrawn) previewMode();
}

void MainWindow::on_coneMinDist_spinBox_valueChanged(double arg1)
{
    stitchIOpts.coneMinDist = arg1;
    if (snoutLineDrawn) previewMode();
}

void MainWindow::on_c_spinBox_valueChanged(double arg1)
{
    stitchIOpts.c = arg1;
    if (snoutLineDrawn) previewMode();
}

void MainWindow::on_maxAD_spinBox_valueChanged(double arg1)
{
    stitchIOpts.maxAD = arg1 * CV_PI / 180;
    if (snoutLineDrawn) previewMode();
}

void MainWindow::on_LengthOfTipAndBottom_spinBox_valueChanged(double arg1)
{
    stitchIOpts.lengthOfTipAndBottom = arg1;
    if (snoutLineDrawn) previewMode();
}

void MainWindow::on_radonLength_spinBox_valueChanged(int arg1)
{
    stitchIOpts.radonLength = arg1;
    if (snoutLineDrawn) previewMode();
}

void MainWindow::on_radonThreshold_spinBox_valueChanged(double arg1)
{
    stitchIOpts.radonThreshold = arg1;
    if (snoutLineDrawn) previewMode();
}

void MainWindow::on_minClusterSize_I_spinBox_valueChanged(int arg1)
{
    stitchIOpts.minClusterSize = arg1;
    ui->preview_minClusterSize_I_slider->setValue(arg1);
    if (snoutLineDrawn) previewMode();
}

// stitching II options

void MainWindow::on_highestRoot_spinBox_valueChanged(double arg1)
{
    stitchIIOpts.highestRoot = arg1;
    if (snoutLineDrawn) previewMode();
}

void MainWindow::on_acceptableXDistance_spinBox_valueChanged(int arg1)
{
    stitchIIOpts.acceptableXdistance = arg1;
    if (snoutLineDrawn) previewMode();
}

void MainWindow::on_LengthOfTipAndBottom_II_spinBox_valueChanged(double arg1)
{
    stitchIIOpts.lengthOfTipAndBottom = arg1;
    if (snoutLineDrawn) previewMode();
}

void MainWindow::on_cone_spinBox_valueChanged(double arg1)
{
    stitchIIOpts.cone = arg1 * CV_PI / 180;
    if (snoutLineDrawn) previewMode();
}

void MainWindow::on_maxAD_II_spinBox_valueChanged(double arg1)
{
    stitchIIOpts.maxAD = arg1 * CV_PI / 180;
    if (snoutLineDrawn) previewMode();
}

void MainWindow::on_minClusterSize_II_spinBox_valueChanged(int arg1)
{
    stitchIIOpts.minClusterSize = arg1;
    ui->preview_minClusterSize_II_slider->setValue(arg1);
    if (snoutLineDrawn) previewMode();
}

// parameter fitting options

void MainWindow::on_minNrOfDots_spinBox_valueChanged(int arg1)
{
    paramOpts.minNrOfDots = arg1;
    if (snoutLineDrawn) previewMode();
}

void MainWindow::on_minLength_spinBox_valueChanged(int arg1)
{
    paramOpts.minLength = arg1;
    ui->preview_minLength_slider->setValue(arg1);
    if (snoutLineDrawn) previewMode();
}

void MainWindow::on_minAngleToSnout_spinBox_valueChanged(double arg1)
{
    paramOpts.minAngleToSnout = arg1 * CV_PI / 180;
    if (snoutLineDrawn) previewMode();
}

void MainWindow::on_maxBend_spinBox_valueChanged(double arg1)
{
    paramOpts.maxBend = arg1;
    if (snoutLineDrawn) previewMode();
}

// tracking options

void MainWindow::on_useKalman_checkBox_clicked(bool checked)
{
    trackOpts.useKalman = checked;
    if (!checked) {
        ui->kalmanDistance_spinBox->setEnabled(false);
        ui->maxChangeInAngle_spinBox->setEnabled(false);
        ui->maxChangeInXf_spinBox->setEnabled(false);
        ui->maxChangeInBend_spinBox->setEnabled(false);
        ui->maxChangeInLength_spinBox->setEnabled(false);
        ui->maxScoreForMatch_spinBox->setEnabled(false);
        ui->weightXf_spinBox->setEnabled(false);
        ui->weightAlpha_spinBox->setEnabled(false);
        ui->weightB_spinBox->setEnabled(false);
        ui->weightL_spinBox->setEnabled(false);
    }
    else {
        ui->kalmanDistance_spinBox->setEnabled(true);
        ui->maxChangeInAngle_spinBox->setEnabled(true);
        ui->maxChangeInXf_spinBox->setEnabled(true);
        ui->maxChangeInBend_spinBox->setEnabled(true);
        ui->maxChangeInLength_spinBox->setEnabled(true);
        ui->maxScoreForMatch_spinBox->setEnabled(true);
        ui->weightXf_spinBox->setEnabled(true);
        ui->weightAlpha_spinBox->setEnabled(true);
        ui->weightB_spinBox->setEnabled(true);
        ui->weightL_spinBox->setEnabled(true);
    }
}

void MainWindow::on_kalmanDistance_spinBox_valueChanged(int arg1)
{
    trackOpts.kalmanDistance = arg1;
}

void MainWindow::on_weightB_spinBox_valueChanged(double arg1)
{
    trackOpts.weightB = arg1;
}

void MainWindow::on_weightL_spinBox_valueChanged(double arg1)
{
    trackOpts.weightL = arg1;
}

void MainWindow::on_weightXf_spinBox_valueChanged(double arg1)
{
    trackOpts.weightX = arg1;
}

void MainWindow::on_weightAlpha_spinBox_valueChanged(double arg1)
{
    trackOpts.weightAlpha = arg1;
}

void MainWindow::on_dontTrack_checkBox_clicked(bool checked)
{
    trackOpts.dontTrack = checked;
}

void MainWindow::on_maxChangeInAngle_spinBox_valueChanged(double arg1)
{
    trackOpts.maxChangeInAngle = arg1 * CV_PI / 180;
}

void MainWindow::on_maxChangeInXf_spinBox_valueChanged(double arg1)
{
   trackOpts.maxChangeInXf = arg1;
}

void MainWindow::on_maxChangeInBend_spinBox_valueChanged(double arg1)
{
    trackOpts.maxChangeInBend = arg1;
}

void MainWindow::on_maxChangeInLength_spinBox_valueChanged(double arg1)
{
    trackOpts.maxChangeInLength = arg1;
}

void MainWindow::on_maxScoreForMatch_spinBox_valueChanged(double arg1)
{
    trackOpts.maxScoreForMatch = arg1;
}

// training options

void MainWindow::on_retrainFreqInit_spinBox_valueChanged(int arg1)
{
    trainOpts.retrainFreqInit = arg1;
}

void MainWindow::on_retrainAtLeast_spinBox_valueChanged(int arg1)
{
    trainOpts.retrainAtLeast = arg1;
}

void MainWindow::on_increaseWith_spinBox_valueChanged(int arg1)
{
    trainOpts.increaseWith = arg1;
}

void MainWindow::on_increaseEvery_spinBox_valueChanged(int arg1)
{
    trainOpts.increaseEvery = arg1;
}

void MainWindow::on_bootstrap_spinBox_valueChanged(int arg1)
{
    trainOpts.bootstrap = arg1;
}

void MainWindow::on_window_spinBox_valueChanged(int arg1)
{
    trainOpts.window = arg1;
}

void MainWindow::on_maxSizeTrainingData_spinBox_valueChanged(int arg1)
{
    trainOpts.maxSizeTrainingData = arg1;
}

void MainWindow::on_chance_train_spinBox_valueChanged(double arg1)
{
    trainOpts.chance_train = arg1 * 100;
}

// recognition options

void MainWindow::on_weightB_spinBox_2_valueChanged(double arg1)
{
    recogOpts.weightB = arg1;
}

void MainWindow::on_weightL_spinBox_2_valueChanged(double arg1)
{
    recogOpts.weightL = arg1;
}

void MainWindow::on_weightXf_spinBox_2_valueChanged(double arg1)
{
    recogOpts.weightX = arg1;
}

void MainWindow::on_weightAlpha_spinBox_2_valueChanged(double arg1)
{
    recogOpts.weightA = arg1;
}

// N-Expert options

void MainWindow::on_maxChangeInBend_spinBox_2_valueChanged(double arg1)
{
    NExpertOpts.maxChangeInBend = arg1;
}

void MainWindow::on_maxChangeInLength_spinBox_2_valueChanged(double arg1)
{
    NExpertOpts.maxChangeInLength = arg1;
}

void MainWindow::on_maxChangeInAngle_spinBox_2_valueChanged(double arg1)
{
    NExpertOpts.maxChangeInAngle = arg1 * CV_PI / 180;
}

void MainWindow::on_maxChangeInXf_spinBox_2_valueChanged(double arg1)
{
    NExpertOpts.maxChangeInXf = arg1;
}


void MainWindow::on_startFrame_slider_valueChanged(int value)
{
    if (value >= worker_endFrame) {
        ui->startFrame_slider->setValue(worker_endFrame - 1);
    }
    else {
        worker_startFrame = value;
        ui->startFrame_spinBox->setValue(value);
    }
}

void MainWindow::on_startFrame_spinBox_valueChanged(int arg1)
{
    ui->startFrame_slider->setValue(arg1);
}

void MainWindow::on_endFrame_slider_valueChanged(int value)
{
    if (value <= worker_startFrame) {
        ui->endFrame_slider->setValue(worker_startFrame + 1);
    }
    else {
        worker_endFrame = value;
        ui->endFrame_spinBox->setValue(value);
    }
}

void MainWindow::on_endFrame_spinBox_valueChanged(int arg1)
{
    ui->endFrame_slider->setValue(arg1);
}

void MainWindow::on_results_dir_button_clicked()
{
    // set default directory to the video file's opened in the last session
    const QString DEFAULT_PRE_OUTPUT_DIR_KEY("defaulti_pre_results_dir");
    QSettings settings("Erasmus MC Neuroscience Lab", "WhiskEras 2.0");

    QString dir = QFileDialog::getExistingDirectory(this, tr("Select Output Directory"),
                                                    settings.value(DEFAULT_PRE_OUTPUT_DIR_KEY).toString(),
                                                    QFileDialog::ShowDirsOnly
                                                    | QFileDialog::DontResolveSymlinks);

    if (!dir.isEmpty()) {
        // save video path for next sessions
        QDir CurrentDir;
        settings.setValue(DEFAULT_PRE_OUTPUT_DIR_KEY, CurrentDir.absoluteFilePath(dir) + "/..");
        dir = dir + "/";
        dumpOpts.qualityOutputDir = dir.toLocal8Bit().constData();
    }
}

void MainWindow::saveSettings()
{
    QSettings settings("Erasmus MC Neuroscience Lab", "WhiskEras 2.0");
    // save settings with the prefix below
    settings.beginGroup(ui->settingsName_lineEdit->text());

    settings.beginGroup("generalOpts");
    settings.setValue("tip(0)", generalOpts.tip[0]);
    settings.setValue("tip(1)", generalOpts.tip[1]);
    settings.setValue("middle(0)", generalOpts.middle[0]);
    settings.setValue("middle(1)", generalOpts.middle[1]);
    settings.setValue("startFrame", ui->startFrame_slider->value());
    settings.setValue("endFrame", ui->endFrame_slider->value());
    settings.setValue("use_cuda", ui->useGPU_checkBox->isChecked());
    settings.endGroup();
    settings.beginGroup("preprocessingOpts");
    settings.setValue("bwThreshold", ui->bwThreshold_spinBox->value());
    settings.setValue("dilate_value", ui->dilateValue_spinBox->value());
    settings.setValue("deinterlacingFlag", ui->deinterlacingFlag_checkBox->isChecked());
    settings.setValue("gaussian_sigma", ui->gaussianSigma_spinBox->value());
    settings.setValue("histEqMinT", ui->histEqMinT_spinBox->value());
    settings.setValue("histEqMaxT", ui->histEqMaxT_spinBox->value());
    settings.setValue("histEqGamma", ui->histEqGamma_spinBox->value());
    settings.setValue("remove_low_levels", ui->removeLowLevels_spinBox->value());
    settings.setValue("framing", ui->framing_spinBox->value());
    settings.setValue("upscaling", ui->upscaling_spinBox->value());
    settings.setValue("cutoff", ui->cutoff_spinBox->value());
    settings.endGroup();
    settings.beginGroup("cExtractionOpts");
    settings.setValue("tr2", ui->tr2_spinBox->value());
    settings.setValue("sigma", ui->sigma_spinBox->value());
    settings.setValue("gaussian_kernel_size", ui->gaussianKernelSize_spinBox->value());
    settings.endGroup();
    settings.beginGroup("clusteringOpts");
    settings.setValue("steger_c", ui->steger_c_spinBox->value());
    settings.setValue("maxAngle", ui->maxAngle_spinBox->value());
    settings.setValue("L", ui->L_spinBox->value());
    settings.setValue("M", ui->M_spinBox->value());
    settings.setValue("K", ui->K_spinBox->value());
    settings.setValue("beamThreshold", ui->beamThreshold_spinBox->value());
    settings.endGroup();
    settings.beginGroup("stitchIOpts");
    settings.setValue("coneMin", ui->coneMin_spinBox->value());
    settings.setValue("coneMax", ui->coneMax_spinBox->value());
    settings.setValue("coneMinDist", ui->coneMinDist_spinBox->value());
    settings.setValue("c", ui->c_spinBox->value());
    settings.setValue("radonLength", ui->radonLength_spinBox->value());
    settings.setValue("radonThreshold", ui->radonThreshold_spinBox->value());
    settings.setValue("maxAD_I", ui->maxAD_spinBox->value());
    settings.setValue("lengthOfTipAndBottom_I", ui->LengthOfTipAndBottom_spinBox->value());
    settings.setValue("minClusterSize_I", ui->minClusterSize_I_spinBox->value());
    settings.endGroup();
    settings.beginGroup("stitchIIOpts");
    settings.setValue("highestRoot", ui->highestRoot_spinBox->value());
    settings.setValue("acceptableXDistance", ui->acceptableXDistance_spinBox->value());
    settings.setValue("lengthOfTipAndBottom_II", ui->LengthOfTipAndBottom_II_spinBox->value());
    settings.setValue("minClusterSize_II", ui->minClusterSize_II_spinBox->value());
    settings.setValue("maxAD_II", ui->maxAD_II_spinBox->value());
    settings.setValue("cone", ui->cone_spinBox->value());
    settings.endGroup();
    settings.beginGroup("paramOpts");
    settings.setValue("minNrOfDots", ui->minNrOfDots_spinBox->value());
    settings.setValue("minLength", ui->minLength_spinBox->value());
    settings.setValue("maxBend", ui->maxBend_spinBox->value());
    settings.setValue("minAngleToSnout", ui->minAngleToSnout_spinBox->value());
    settings.endGroup();
    settings.beginGroup("trackOpts");
    settings.setValue("useKalman", ui->useKalman_checkBox->isChecked());
    settings.setValue("dontTrack", ui->dontTrack_checkBox->isChecked());
    settings.setValue("kalmanDistance", ui->kalmanDistance_spinBox->value());
    settings.setValue("weightB", ui->weightB_spinBox->value());
    settings.setValue("weightL", ui->weightL_spinBox->value());
    settings.setValue("weightX", ui->weightXf_spinBox->value());
    settings.setValue("weightAlpha", ui->weightAlpha_spinBox->value());
    settings.setValue("maxChangeInBend", ui->maxChangeInBend_spinBox->value());
    settings.setValue("maxChangeInXf", ui->maxChangeInXf_spinBox->value());
    settings.setValue("maxChangeInLength", ui->maxChangeInLength_spinBox->value());
    settings.setValue("maxChangeInAngle", ui->maxChangeInAngle_spinBox->value());
    settings.setValue("maxScoreForMatch", ui->maxScoreForMatch_spinBox->value());
    settings.endGroup();
    settings.beginGroup("trainOpts");
    settings.setValue("retrainFreqInit", ui->retrainFreqInit_spinBox->value());
    settings.setValue("retrainAtLeast", ui->retrainAtLeast_spinBox->value());
    settings.setValue("increaseWith", ui->increaseWith_spinBox->value());
    settings.setValue("increaseEvery", ui->increaseEvery_spinBox->value());
    settings.setValue("bootstrap", ui->bootstrap_spinBox->value());
    settings.setValue("window", ui->window_spinBox->value());
    settings.setValue("maxSizeTrainingData", ui->maxSizeTrainingData_spinBox->value());
    settings.setValue("chance_train", ui->chance_train_spinBox->value());
    settings.endGroup();
    settings.beginGroup("recogOpts");
    settings.setValue("weightB_2", ui->weightB_spinBox_2->value());
    settings.setValue("weightL_2", ui->weightL_spinBox_2->value());
    settings.setValue("weightX_2", ui->weightXf_spinBox_2->value());
    settings.setValue("weightAlpha_2", ui->weightAlpha_spinBox_2->value());
    settings.endGroup();
    settings.beginGroup("NExpertOpts");
    settings.setValue("maxChangeInBend_2", ui->maxChangeInBend_spinBox_2->value());
    settings.setValue("maxChangeInXf_2", ui->maxChangeInXf_spinBox_2->value());
    settings.setValue("maxChangeInLength_2", ui->maxChangeInLength_spinBox_2->value());
    settings.setValue("maxChangeInAngle_2", ui->maxChangeInAngle_spinBox_2->value());
    settings.endGroup();


    settings.endGroup();
}

void MainWindow::loadSettings()
{
    QSettings settings("Erasmus MC Neuroscience Lab", "WhiskEras 2.0");
    // load settings with the prefix below
    settings.beginGroup(ui->settingsName_lineEdit->text());

    settings.beginGroup("generalOpts");
    generalOpts.tip[0] = settings.value("tip(0)", generalOpts.tip[0]).toInt();
    generalOpts.tip[1] = settings.value("tip(1)", generalOpts.tip[1]).toInt();
    generalOpts.middle[0] = settings.value("middle(0)", generalOpts.middle[0]).toInt();
    generalOpts.middle[1] = settings.value("middle(1)", generalOpts.middle[1]).toInt();
    ui->startFrame_slider->setValue(settings.value("startFrame", generalOpts.startFrame).toInt());
    ui->endFrame_slider->setValue(settings.value("endFrame", generalOpts.endFrame).toInt());
    ui->useGPU_checkBox->setChecked(settings.value("use_cuda", generalOpts.use_cuda).toBool());
    settings.endGroup();
    settings.beginGroup("preprocessingOpts");
    ui->bwThreshold_spinBox->setValue(settings.value("bwThreshold", preprocessingOpts.bwThreshold).toInt());
    ui->dilateValue_spinBox->setValue(settings.value("dilate_value", preprocessingOpts.dilate_value).toFloat());
    ui->deinterlacingFlag_checkBox->setChecked(settings.value("deinterlacingFlag", preprocessingOpts.deinterlacingFlag).toBool());
    ui->gaussianSigma_spinBox->setValue(settings.value("gaussian_sigma", preprocessingOpts.initialGaussianSigma).toFloat());
    ui->histEqMinT_spinBox->setValue(settings.value("histEqMinT", preprocessingOpts.histEqMinT).toInt());
    ui->histEqMaxT_spinBox->setValue(settings.value("histEqMaxT", preprocessingOpts.histEqMaxT).toInt());
    ui->histEqGamma_spinBox->setValue(settings.value("histEqGamma", preprocessingOpts.histEqGamma).toFloat());
    ui->removeLowLevels_spinBox->setValue(settings.value("remove_low_levels", preprocessingOpts.remove_lowlevels).toInt());
    ui->framing_spinBox->setValue(settings.value("framing", preprocessingOpts.framing).toInt());
    ui->upscaling_spinBox->setValue(settings.value("upscaling", preprocessingOpts.upscaling).toFloat());
    ui->cutoff_spinBox->setValue(settings.value("cutoff", clusteringOpts.cutoff).toInt());
    settings.endGroup();
    settings.beginGroup("cExtractionOpts");
    ui->tr2_spinBox->setValue(settings.value("tr2", cExtractionOpts.tr2).toFloat());
    ui->sigma_spinBox->setValue(settings.value("sigma", cExtractionOpts.sigma).toFloat());
    ui->gaussianKernelSize_spinBox->setValue(settings.value("gaussian_kernel_size", cExtractionOpts.gaussianKernelSize).toInt());
    settings.endGroup();
    settings.beginGroup("clusteringOpts");
    ui->steger_c_spinBox->setValue(settings.value("steger_c", clusteringOpts.steger_c).toFloat());
    ui->maxAngle_spinBox->setValue(settings.value("maxAngle", clusteringOpts.maxAngle).toFloat());
    ui->L_spinBox->setValue(settings.value("L", clusteringOpts.L).toInt());
    ui->M_spinBox->setValue(settings.value("M", clusteringOpts.M).toInt());
    ui->K_spinBox->setValue(settings.value("K", clusteringOpts.K).toInt());
    ui->beamThreshold_spinBox->setValue(settings.value("beamThreshold", clusteringOpts.minClusterSizeToExtend).toInt());
    settings.endGroup();
    settings.beginGroup("stitchIOpts");
    ui->coneMin_spinBox->setValue(settings.value("coneMin", stitchIOpts.coneMin).toFloat());
    ui->coneMax_spinBox->setValue(settings.value("coneMax", stitchIOpts.coneMax).toFloat());
    ui->coneMinDist_spinBox->setValue(settings.value("coneMinDist", stitchIOpts.coneMinDist).toFloat());
    ui->c_spinBox->setValue(settings.value("c", stitchIOpts.c).toFloat());
    ui->radonLength_spinBox->setValue(settings.value("radonLength", stitchIOpts.radonLength).toInt());
    ui->radonThreshold_spinBox->setValue(settings.value("radonThreshold", stitchIOpts.radonThreshold).toFloat());
    ui->maxAD_spinBox->setValue(settings.value("maxAD_I", stitchIOpts.maxAD).toFloat());
    ui->LengthOfTipAndBottom_spinBox->setValue(settings.value("lengthOfTipAndBottom_I", stitchIOpts.lengthOfTipAndBottom).toInt());
    ui->minClusterSize_I_spinBox->setValue(settings.value("minClusterSize_I", stitchIOpts.minClusterSize).toInt());
    settings.endGroup();
    settings.beginGroup("stitchIIOpts");
    ui->highestRoot_spinBox->setValue(settings.value("highestRoot", stitchIIOpts.highestRoot).toFloat());
    ui->acceptableXDistance_spinBox->setValue(settings.value("acceptableXDistance", stitchIIOpts.acceptableXdistance).toInt());
    ui->LengthOfTipAndBottom_II_spinBox->setValue(settings.value("lengthOfTipAndBottom_II", stitchIIOpts.lengthOfTipAndBottom).toInt());
    ui->minClusterSize_II_spinBox->setValue(settings.value("minClusterSize_II", stitchIIOpts.minClusterSize).toInt());
    ui->maxAD_II_spinBox->setValue(settings.value("maxAD", stitchIIOpts.maxAD).toFloat());
    ui->cone_spinBox->setValue(settings.value("cone", stitchIIOpts.cone).toFloat());
    settings.endGroup();
    settings.beginGroup("paramOpts");
    ui->minNrOfDots_spinBox->setValue(settings.value("minNrOfDots", paramOpts.minNrOfDots).toInt());
    ui->minLength_spinBox->setValue(settings.value("minLength", paramOpts.minLength).toInt());
    ui->maxBend_spinBox->setValue(settings.value("maxBend", paramOpts.maxBend).toFloat());
    ui->minAngleToSnout_spinBox->setValue(settings.value("minAngleToSnout", paramOpts.minAngleToSnout).toFloat());
    settings.endGroup();
    settings.beginGroup("trackOpts");
    ui->useKalman_checkBox->setChecked(settings.value("useKalman", trackOpts.useKalman).toBool());
    ui->dontTrack_checkBox->setChecked(settings.value("dontTrack", trackOpts.dontTrack).toBool());
    ui->kalmanDistance_spinBox->setValue(settings.value("kalmanDistance", trackOpts.kalmanDistance).toInt());
    ui->weightB_spinBox->setValue(settings.value("weightB", trackOpts.weightB).toFloat());
    ui->weightL_spinBox->setValue(settings.value("weightL", trackOpts.weightL).toFloat());
    ui->weightXf_spinBox->setValue(settings.value("weightX", trackOpts.weightX).toFloat());
    ui->weightAlpha_spinBox->setValue(settings.value("weightAlpha", trackOpts.weightAlpha).toFloat());
    ui->maxChangeInBend_spinBox->setValue(settings.value("maxChangeInBend", trackOpts.maxChangeInBend).toFloat());
    ui->maxChangeInXf_spinBox->setValue(settings.value("maxChangeInXf", trackOpts.maxChangeInXf).toFloat());
    ui->maxChangeInLength_spinBox->setValue(settings.value("maxChangeInLength", trackOpts.maxChangeInLength).toFloat());
    ui->maxChangeInAngle_spinBox->setValue(settings.value("maxChangeInAngle", trackOpts.maxChangeInAngle).toFloat());
    ui->maxScoreForMatch_spinBox->setValue(settings.value("maxScoreForMatch", trackOpts.maxScoreForMatch).toFloat());
    settings.endGroup();
    settings.beginGroup("trainOpts");
    ui->retrainFreqInit_spinBox->setValue(settings.value("retrainFreqInit", trainOpts.retrainFreqInit).toInt());
    ui->retrainAtLeast_spinBox->setValue(settings.value("retrainAtLeast", trainOpts.retrainAtLeast).toInt());
    ui->increaseWith_spinBox->setValue(settings.value("increaseWith", trainOpts.increaseWith).toInt());
    ui->increaseEvery_spinBox->setValue(settings.value("increaseEvery", trainOpts.increaseEvery).toInt());
    ui->bootstrap_spinBox->setValue(settings.value("bootstrap", trainOpts.bootstrap).toInt());
    ui->window_spinBox->setValue(settings.value("window", trainOpts.window).toUInt());
    ui->maxSizeTrainingData_spinBox->setValue(settings.value("maxSizeTrainingData", (quint64) trainOpts.maxSizeTrainingData).toUInt());
    ui->chance_train_spinBox->setValue(settings.value("chance_train", trainOpts.chance_train).toFloat());
    settings.endGroup();
    settings.beginGroup("recogOpts");
    ui->weightB_spinBox_2->setValue(settings.value("weightB_2", recogOpts.weightB).toFloat());
    ui->weightL_spinBox_2->setValue(settings.value("weightL_2", recogOpts.weightL).toFloat());
    ui->weightXf_spinBox_2->setValue(settings.value("weightX_2", recogOpts.weightX).toFloat());
    ui->weightAlpha_spinBox_2->setValue(settings.value("weightAlpha_2", recogOpts.weightA).toFloat());
    settings.endGroup();
    settings.beginGroup("NExpertOpts");
    ui->maxChangeInBend_spinBox_2->setValue(settings.value("maxChangeInBend_2", NExpertOpts.maxChangeInBend).toFloat());
    ui->maxChangeInXf_spinBox_2->setValue(settings.value("maxChangeInXf_2", trackOpts.maxChangeInXf).toFloat());
    ui->maxChangeInLength_spinBox_2->setValue(settings.value("maxChangeInLength_2", trackOpts.maxChangeInLength).toFloat());
    ui->maxChangeInAngle_spinBox_2->setValue(settings.value("maxChangeInAngle_2", trackOpts.maxChangeInAngle).toFloat());
    settings.endGroup();

    settings.endGroup();
}

void MainWindow::on_saveOptions_button_clicked()
{

    saveSettings();
}

void MainWindow::on_loadOptions_button_clicked()
{
    loadSettings();
}

void MainWindow::on_preview_histEqGamma_slider_valueChanged(int value)
{
    preprocessingOpts.histEqGamma = (double) value / 100;
    ui->preview_histEqGamma_spinBox->setValue((double) value / 100);
    ui->histEqGamma_spinBox->setValue((double) value / 100);
}

void MainWindow::on_preview_dilateValue_slider_valueChanged(int value)
{
    preprocessingOpts.dilate_value = value;
    ui->preview_dilateValue_spinBox->setValue(value);
    ui->dilateValue_spinBox->setValue(value);
}

void MainWindow::on_preview_minClusterSize_I_slider_valueChanged(int value)
{
    stitchIOpts.minClusterSize = value;
    ui->preview_minClusterSize_I_spinBox->setValue(value);
    ui->minClusterSize_I_spinBox->setValue(value);
}

void MainWindow::on_preview_minClusterSize_II_slider_valueChanged(int value)
{
    stitchIIOpts.minClusterSize = value;
    ui->preview_minClusterSize_II_spinBox->setValue(value);
    ui->minClusterSize_II_spinBox->setValue(value);
}

void MainWindow::on_preview_minLength_slider_valueChanged(int value)
{
    paramOpts.minLength = value;
    ui->preview_minLength_spinBox->setValue(value);
    ui->minLength_spinBox->setValue(value);
}

void MainWindow::on_preview_histEqGamma_spinBox_valueChanged(double arg1)
{
    ui->preview_histEqGamma_slider->setValue(arg1*100);
}

void MainWindow::on_preview_dilateValue_spinBox_valueChanged(int arg1)
{
    ui->preview_dilateValue_slider->setValue(arg1);
}

void MainWindow::on_preview_minClusterSize_I_spinBox_valueChanged(int arg1)
{
    ui->preview_minClusterSize_I_slider->setValue(arg1);
}

void MainWindow::on_preview_minClusterSize_II_spinBox_valueChanged(int arg1)
{
    ui->preview_minClusterSize_II_slider->setValue(arg1);
}

void MainWindow::on_preview_minLength_spinBox_valueChanged(int arg1)
{
    ui->preview_minLength_slider->setValue(arg1);
}

void MainWindow::on_frameProcessed(int val)
{
    int percent_progress = (double) (val-worker_startFrame) / (worker_endFrame-worker_startFrame-1) * 100;
    ui->processing_progressBar->setValue(percent_progress);
}

void MainWindow::on_videoOutput_checkBox_clicked(bool checked)
{
    plottingOpts.writeVideoOutput = checked;
    // if checked, prompt video output directory dialog
    if (checked && dumpOpts.videoOutputPath.empty())
        on_videoOutputDir_button_clicked();
}

void MainWindow::on_videoOutputDir_button_clicked()
{
    // set default directory to the video file's opened in the last session
    const QString DEFAULT_VIDEO_OUTPUT_DIR_KEY("default_video_output_dir");
    QSettings settings("Erasmus MC Neuroscience Lab", "WhiskEras 2.0");

    QString dir = QFileDialog::getExistingDirectory(this, tr("Select Video Output Directory"),
                                                    settings.value(DEFAULT_VIDEO_OUTPUT_DIR_KEY).toString(),
                                                    QFileDialog::ShowDirsOnly
                                                    | QFileDialog::DontResolveSymlinks);

    if (!dir.isEmpty()) {
        // save video path for next sessions
        QDir CurrentDir;
        settings.setValue(DEFAULT_VIDEO_OUTPUT_DIR_KEY, CurrentDir.absoluteFilePath(dir) + "/..");
        if (!dumpOpts.outputFilename.empty()) {
            dumpOpts.videoOutputPath = std::string(dir.toLocal8Bit().constData()) + '/' + dumpOpts.outputFilename + ".avi";
        }
        else {
            dumpOpts.videoOutputPath = std::string(dir.toLocal8Bit().constData()) + "/whisker_tracking.avi";
        }
    }
    else if (dumpOpts.videoOutputPath.empty()) {
        ui->videoOutput_checkBox->setChecked(false);
    }
}
